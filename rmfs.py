import os

tag = 'btagmedium'

os.system('xrdfs root://cmseos.fnal.gov/ ls /store/user/acrobert/condor/pkl/ > lstmp.txt 2>&1')
lsout = open('lstmp.txt','r')
lslines = lsout.readlines()

for line in lslines:
    if tag in line:
        os.system('xrdfs root://cmseos.fnal.gov/ rm '+line.rstrip())

os.system('rm lstmp.txt')
