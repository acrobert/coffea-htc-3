import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
import matplotlib.pyplot as plt
import mplhep
from argparse import ArgumentParser
import os, psutil
import tracemalloc
import gc
from copy import deepcopy
import matplotlib.ticker as ticker

plt.rcParams['text.usetex'] = True
tmr = timer()

btagthresh = 0.2783
pucut = 4

def selection(tagnano, hltkey):

    # basic selection with ZJets HLT                                                                                                                                                                              
    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]
    selbjets = tagnano.Jet[(tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4) & (tagnano.Jet.btagDeepFlavB > btagthresh) & (tagnano.Jet.puId >= 4)]
    tagnano = tagnano[ak.num(selbjets) >= 2]

    # bjet - bbjet bin selection                                                                                                                                                                                  
    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    tagnano = tagnano[(ak.num(bbjets[bbjets.pt > 30.]) >= 1) & (ak.num(bjets[bjets.pt > 30.]) >= 1)]

    # cut on the dR between the two jets                                                                                                                                                                          
    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    passes = (ak.num(bjet4V) > 0) & (ak.num(bbjet4V) > 0)
    bbtobdR = bbjet4V[passes][:, 0].delta_r(bjet4V[passes])
    selbjet = bjet4V[passes][bbtobdR == ak.min(bbtobdR, axis=1)]

    tagnano = tagnano[passes][ak.min(bbtobdR, axis=1) <= 1.5]

    return tagnano


def fillhist(tagnano, hist):

    # dijet mass                                                                                                                                                                                                  
    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    passes = (ak.num(bjet4V) > 0) & (ak.num(bbjet4V) > 0)
    bbtobdR = bbjet4V[passes][:, 0].delta_r(bjet4V[passes])
    selbjet = bjet4V[passes][bbtobdR == ak.min(bbtobdR, axis=1)]

    dijetmass = (bbjet4V[passes][:, 0] + selbjet[:, 0]).mass
    hist.fill(dijetmass)
    return len(tagnano)

def loadfile(loc, path='', metadata={}):

    if path=='':
        try:
            nano = NanoEventsFactory.from_root(
                loc,
                schemaclass=NanoAODSchema.v6,
                metadata=metadata,
            ).events()
        except uproot.exceptions.KeyInFileError:
            return None
        except OSError:
            return None
    else:
        try:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                nano = NanoEventsFactory.from_root(
                    loc,
                    treepath=path,
                    schemaclass=NanoAODSchema.v6,
            metadata=metadata,
                ).events()
        except uproot.exceptions.KeyInFileError:
            return None
        except OSError:
            return None

    return nano


def loadpair(nanofile, friendfile, mode='1Fr'):

    tagnano = loadfile(nanofile, metadata={"dataset file": nanofile})
    friends = [loadfile(file, path='fevt/EvtTree', metadata={"dataset file": file}) for file in friendfile]

    if tagnano is None:
        print(f'Skipping null file: {nanofile.split("/")[-1]}')
        return None
    if friends[0] is None:
        locpath = friendfile[0].replace('root://cmsdata.phys.cmu.edu/', '')
        os.system(f'xrdfs root://cmsdata.phys.cmu.edu/ rm {locpath}')
        print(f'Skipping null file: {friendfile[0].split("/")[-1]}')
        return None
        #return tagnano                                                                                                                                                                                        

    if mode != '1Fr':
        if friends[1] is None:
            print(f'Skipping null file: {friendfile[1].split("/")[-1]}')
            return None

    if mode != '1Fr':
        if len(tagnano) != len(friends[0]) or len(tagnano) != len(friends[1]):
            print(f'Skipping unmatched files: {len(friends[0])} {len(friends[1])} {len(tagnano)}')
            return None
    else:
        if len(tagnano) != len(friends[0]):
            print(f'Skipping unmatched files: {len(friends[0])} {len(tagnano)}')
            return None

    assert len(tagnano) == len(friends[0])
    #assert len(tagnano) == len(friends[1])                                                                                                                                                                    

    if mode != '1Fr':
        keys = ['Zs', 'map']
        for i in range(len(friends)):
            #print(i, friends[i].fields)                                                                                                                                                                       
            if keys[i] not in friends[i].fields:
                print(f'Skipping missing field: {keys[i]}')
                return None
            tagnano[keys[i]] = friends[i][keys[i]]
        #print(f'Running file pair {nanofile}; elapsed time {round(tmr.time()/60., 3)}')                                                                                                                       
    else:
        keys = ['Zs', 'map', 'AllTruthBs','NonZTruthBs']
        for i in range(len(keys)):
            #print(friends[0].fields)                                                                                                                                                                          
            if keys[i] not in friends[0].fields:
                print(f'Skipping missing field: {keys[i]}')
                return None
            try:
                tagnano[keys[i]] = friends[0][keys[i]]
            except:
                print('Skipping missing field:', keys[i])

    tagnano['friendjet_noptcut'] = friends[0]['friendjet']
    ptthresh = friends[0].friendjet.pt > 15.00001
    friends[0]['friendjet'] = friends[0]['friendjet'][ptthresh]
    tagnano['friendjet'] = friends[0]['friendjet']

    diffs = ak.num(tagnano.Jet) != ak.num(friends[0]['friendjet'])
    diffs = ak.num(tagnano.Jet) != ak.num(tagnano.friendjet.pfDeepFlavourJetTags_probbb)
    if ak.sum(diffs) != 0:
        print(ak.num(tagnano.Jet)[diffs], ak.num(tagnano.friendjet.pfDeepFlavourJetTags_probbb[diffs]))

        ptthresh = friends[0].friendjet.pt > 15.001
        friends[0]['friendjet'] = friends[0]['friendjet'][ptthresh]
        tagnano['friendjet'] = friends[0]['friendjet']

    return tagnano

def checkcq():
    statuses = {}
    os.system('condor_q > cqtmp.txt 2>&1')
    try:
        with open('cqtmp.txt','r') as cqout:
            cqlines = cqout.readlines()
            for line in cqlines:
                if 'makeHist.sh' in line:
                    linelist = line.split(' ')
                    lineinfo = list(filter(lambda a: a != '', linelist[:-1]))

                    key = lineinfo[-1]
                    if key not in statuses.keys():
                        statuses[key] = [0, 0, 0]

                    if lineinfo[5] == 'I':
                        statuses[key][0] += 1
                    elif lineinfo[5] == 'R':
                        statuses[key][1] += 1
                    else:
                        statuses[key][2] += 1
        os.system('rm cqtmp.txt')
    except IOError:
        pass

    return statuses
