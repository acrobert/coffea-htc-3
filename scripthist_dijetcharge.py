import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
import matplotlib.pyplot as plt
import mplhep
from argparse import ArgumentParser
from classRunCoffea import RunCoffea
import os, psutil
import tracemalloc
import gc
from copy import deepcopy
import matplotlib.ticker as ticker
from time import sleep
from htcutils import *
import pickle as pkl
import sys

plt.rcParams['text.usetex'] = True
tmr = timer()

run = sys.argv[1] == 'run' 

if run:
    print(' >> running hists')

fillhist = 'dijetcharge'
selection = 'zjhlt_b_bb_dR1p5'
btagthresh = 'medium'

addtag = ''

xmin = -1.5
xmax = 1.5
nbins = 30
xlab = 'Dijet Charge'
ylab = 'Normalized Events'
logscale = False
title = r'BBJet-BJet Charge Sum'
#fname = f'plots/combinedhist_{tag}.pdf'
normalized = True
run2rescale = False
stack = False
combinetypes = True
showSB = False

from funcrunhist import histsrunner

runner = histsrunner(fillhist, selection, btagthresh, addtag=addtag)

if run:
    runner.runhists(xmin, xmax, nbins)

print(' >> reading hists')
runner.checkhists()
runner.readhists(xlab, ylab, title, logscale, normalized, run2rescale, stack, combinetypes, showSB)

