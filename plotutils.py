import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
import matplotlib.pyplot as plt
import mplhep
from argparse import ArgumentParser
import os, psutil
from copy import deepcopy
import matplotlib.ticker as ticker
from time import sleep
from htcutils import *
import pickle as pkl
import sys


def addcms(self, ax, data=False, mc=False, run2=False):

    if data and mc:
        cmsstr = 'data/simulation'
    elif data:
        cmsstr = 'data'
    elif mc:
        cmsstr = 'simulation'

    lumi = r"138 $\mathrm{fb^{-1}}$ (13 TeV)" if run2 else r"(13 TeV)"

    ax.text(x=0, y=1.005, s=f"Private work (CMS {cmsstr})", transform=ax.transAxes, ha="left", va="bottom",
            fontsize=10, fontweight="normal", fontname="TeX Gyre Heros")
    ax.text(x=1, y=1.005, s=lumi, transform=ax.transAxes, ha="right", va="bottom",
            fontsize=10, fontweight="normal", fontname="TeX Gyre Heros")


def addmeanwidth(self, ax, counts, bins, color, density=False):

    binwidth = bins[1] - bins[0]
    mean = np.sum((bins[:-1] + binwidth/2) * (counts)) / np.sum(counts)

    if density:
        densityvals = counts/np.sum(counts * (binwidth))
        maxval = np.max(densityvals)
    else:
        maxval = np.max(counts)

    stdev = np.sqrt(np.sum(counts * (bins[:-1] + binwidth/2 - mean)**2 ) / (np.sum(counts)-1) )
    fwhm = 2*np.sqrt(2*(math.log(2)))*stdev

    x1 = [mean, mean]
    y1 = [0., maxval]
    ax.plot(x1, y1, linestyle='--', color=color)
    x2 = [mean - fwhm/2, mean + fwhm/2]
    y2 = [0.1*maxval + 0.03*maxval*i, 0.1*maxval + 0.03*maxval*i]
    ax.plot(x2, y2, linestyle='--', color=color)

def checkhists(self):

    while True:

        print(round(self.tmr.time()/60., 3))
        print(checkcq(self.tag))
        if checkcq(self.tag) == {}:
            break
        sleep(30)

def getpkls(self, key, tag, site = 'cmseos.fnal.gov'):
            
    os.system(f'xrdfs root://{site}/ ls /store/user/acrobert/condor/pkl/ > lstmp.txt 2>&1')
    lsout = open('lstmp.txt','r')
    lslines = lsout.readlines()

    nhists = -1
    nbins = -1

    thisnall = 0
    thisnevts = None #[0 for j in range(self.nhists)]
    thiscountslist = None

    for line in lslines:
        if key in line and tag in line:

            rfname = f'root://{site}/'+line.rstrip()
            locname = line.rstrip().split('/')[-1]
            os.system(f'xrdcp {rfname} pkl/.')

            with open('pkl/'+locname, "rb") as output_file:
                outdict = pkl.load(output_file)
                
                if nhists == -1:
                    nhists = len(outdict['hist'])
                    thisnevts = [0 for j in range(nhists)]

                for ih in range(nhists):
                    c, b = outdict['hist'][ih].to_numpy()
                    
                    if nbins == -1:
                        nbins = len(c)
                        thiscountslist = [ np.zeros((nbins)) for k in range(nhists) ]

                    thiscountslist[ih] += c
                    thisnevts[ih] += outdict['nevts'][ih]
                    thisnall += outdict['nall']
                            
            os.system('rm pkl/'+locname)
            
    os.system('rm lstmp.txt')
    return thisnall, thisnevts, thiscountslist
