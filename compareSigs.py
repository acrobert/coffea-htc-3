import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
import matplotlib.pyplot as plt
import mplhep
from argparse import ArgumentParser
from classRunCoffea import RunCoffea
import os, psutil
import tracemalloc
import gc
from copy import deepcopy
import matplotlib.ticker as ticker

#plt.rcParams['text.usetex'] = True
tmr = timer()

#keys = ['ZJetsTo2B-HT800-4j-g0', 'TTJets_up20UL18', 'ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18', 'QCD_HT2000_up20UL18']
#keys = ['TTJets_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18', 'ZJetsTo2B-HT800-4j-g0']
#types = ['TTJets', 'VJets', 'QCD', 'ZJets to 4B']
#keys = [['TTJets_up20UL18'], ['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['ZJetsTo2B-HT6to8-4j-g0', 'ZJetsTo2B-HT800-4j-g0']]

types = ['VJets', 'WJets', 'ZJets', 'ZJets to 4B']
keys = [['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['WJets_HT800_up20UL18'], ['ZJets_HT800_up20UL18'], ['ZJetsTo2B-HT800-4j-g0']]


xmin = 0
xmax = 300
nbins = 30
xlab = 'Dijet Mass'
ylab = 'Run 2 Events'
logscale = False
title = r'BJet + BBJet mass rescaled to Run 2 Luminosity'
fname = 'plots/combinedhist_dijetmass_b-bb-bin-dR1p5_typetest.pdf'
normalized = False
run2rescale = True
stack = False
combinetypes = True

maxevents = 10000
maxfiles = 10

thishist = [[ Hist(hist.axis.Regular(nbins, xmin, xmax, name='bin')) for i in range(len(keys[j])) ] for j in range(len(types)) ]
btagthresh = 0.2783
pucut = 4

def selection(tagnano):

    # basic selection with ZJets HLT
    tagnano = tagnano[passHLT(tagnano, anaHLT[keys[-1][-1]])]
    selbjets = tagnano.Jet[(tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4) & (tagnano.Jet.btagDeepFlavB > btagthresh) & (tagnano.Jet.puId >= 4)]
    tagnano = tagnano[ak.num(selbjets) >= 2]

    # bjet - bbjet bin selection
    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]
    
    tagnano = tagnano[(ak.num(bbjets[bbjets.pt > 30.]) >= 1) & (ak.num(bjets[bjets.pt > 30.]) >= 1)]

    # cut on the dR between the two jets
    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    passes = (ak.num(bjet4V) > 0) & (ak.num(bbjet4V) > 0)
    bbtobdR = bbjet4V[passes][:, 0].delta_r(bjet4V[passes])
    selbjet = bjet4V[passes][bbtobdR == ak.min(bbtobdR, axis=1)]
    
    tagnano = tagnano[passes][ak.min(bbtobdR, axis=1) <= 1.5]

    return tagnano

def fillhist(tagnano, hist):

    # number of leptons
    #electrons = tagnano.Electron[(tagnano.Electron.pt >= 5)]# & (abs(tagnano.Electron.eta) < 2.4)]# & (tagnano.Electron.jetIdx == -1)]
    #muons = tagnano.Muon[(tagnano.Muon.pt >= 5)]# & (abs(tagnano.Muon.eta) < 2.4)]# & (tagnano.Muon.jetIdx == -1)]
    ##nlep = ak.num(electrons) + ak.num(muons)
    #hist.fill(ak.flatten(electrons.pfRelIso03_all))
    #hist.fill(ak.flatten(muons.pfRelIso03_all))
    #return len(electrons)

    # dijet mass
    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    #bjets = bjets[ak.argsort(bjets.pfDeepFlavourJetTags_probb + bjets.pfDeepFlavourJetTags_problepb, ascending=False)]
    #bbjets = bbjets[ak.argsort(bbjets.pfDeepFlavourJetTags_probbb, ascending=False)]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    passes = (ak.num(bjet4V) > 0) & (ak.num(bbjet4V) > 0)
    bbtobdR = bbjet4V[passes][:, 0].delta_r(bjet4V[passes])
    selbjet = bjet4V[passes][bbtobdR == ak.min(bbtobdR, axis=1)]

    dijetmass = (bbjet4V[passes][:, 0] + selbjet[:, 0]).mass
    hist.fill(dijetmass)
    #counts, bins = hist.to_numpy()
    #print(counts)
    #hist.fill(bjets.pt)
    #hist.fill(bbjets.pt)
    return len(tagnano)

nall = [[0 for i in keys[k]] for k in range(len(types))]
nevts = [[0 for i in keys[k]] for k in range(len(types))]

for k in range(len(types)):
    for i in range(len(keys[k])):

        key = keys[k][i]
        
        print(f'Running key {key}; elapsed time {round(tmr.time()/60., 3)}')
        if key == 'TTJets_up20UL18':
            filepairs = RunCoffea.initnanos(key, mode='1Fr')
        else:
            filepairs = RunCoffea.initnanos(key)
        
        nfiles = 0
        nevents = 0
        for j in range(len(filepairs)):
        
            nanofile, friendfile = filepairs[j]
            if key == 'TTJets_up20UL18':
                tagnano = RunCoffea.loadpair(nanofile, friendfile, mode='1Fr')
            else:
                tagnano = RunCoffea.loadpair(nanofile, friendfile) 
        
            if tagnano is None:
                continue
            if 'Zs' not in tagnano.fields:
                continue
           
            print(f'Running file pair {j} filled {nevents}; elapsed time {round(tmr.time()/60., 3)}')
            nfiles += 1
        
            nall[k][i] += len(tagnano)
        
            tagnano = selection(tagnano)
            nevents += fillhist(tagnano, thishist[k][i])
        
            nevts[k][i] += len(tagnano)
        
            del tagnano
            print('Current memory:', psutil.Process(os.getpid()).memory_info().rss / 1024 ** 2)
            gc.collect()
        
            if nevents >= maxevents:
                break
            if nfiles >= maxfiles:
                break
    
fig, ax = plt.subplots(figsize=(8, 5))
finalcounts = []
finalerrs = []
finalkeys = []
finalnevts = []

for i in range(len(types)):

    if not combinetypes: 
        for j in range(len(keys[i])):
            count, bins = thishist[i][j].to_numpy()
            finalcounts.append(count*signalevents[keys[i][j]]/nall[i][j])
            finalerrs.append(np.sqrt(count)*signalevents[keys[i][j]]/nall[i][j])
            finalkeys.append(keys[i][j])
            finalnevts.append(round(nevts[i][j]*signalevents[keys[i][j]]/nall[i][j], 2))
            continue

    
    counts = []
    errs = []
    tnevts = []

    for j in range(len(keys[i])):

        count, bins = thishist[i][j].to_numpy()
        newcount = count*signalevents[keys[i][j]]/nall[i][j]
        counts.append(newcount)
        errs.append(np.sqrt(count)*signalevents[keys[i][j]]/nall[i][j])
        tnevts.append(round(nevts[i][j]*signalevents[keys[i][j]]/nall[i][j], 2))

    counts = np.array(counts)
    errs = np.array(errs)

    typecount = np.sum(counts, axis=0)
    typeerrs = np.sqrt(np.sum(errs**2, axis=0))

    finalcounts.append(typecount)
    finalerrs.append(typeerrs)
    finalkeys.append(types[i])
    finalnevts.append(sum(tnevts))

for i in range(len(finalcounts)):
    mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=finalcounts[i]), yerr=finalerrs[i], label=finalkeys[i].replace('_','-')+' (N='+str(finalnevts[i])+')', ax=ax, density=normalized)

if run2rescale and stack:

    mplhep.histplot([finalcounts[-1*i-1] for i in range(len(finalkeys))], bins=bins, histtype='fill', stack=True, label=[finalkeys[-1*i-1].replace('_','-') for i in range(len(finalkeys))])
    
    tcounts = np.array(finalcounts)
    sumcounts = np.sum(tcounts, axis=0)

    mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=sumcounts), yerr=np.sqrt(sumcounts), label='Sum', ax=ax, histtype='errorbar', color='k')

#if run2rescale:
#    counts = []
#    errs = []
#    for k in range(len(keys)):
#        try:
#            N = round(nevts[k]*signalevents[keys[k]]/nall[k], 2)
#    
#            c, bins = thishist[k].to_numpy()
#            newcount = c*signalevents[keys[k]]/nall[k]
#        except ZeroDivisionError:
#            N = 0
#            c, bins = thishist[k].to_numpy()
#            newcount = c
#
#        counts.append(newcount)
#
#        origerrs = np.sqrt(c)
#        newerrs = np.array([origerrs[i] * newcount[i] / c[i] for i in range(len(c))])
#
#        errs.append(newerrs)
#
#        if not stack:
#            mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=newcount), yerr=newerrs, label=keys[k].replace('_','-')+' (N='+str(N)+')', ax=ax, density=normalized)
#    
#    if stack:
#        mplhep.histplot([counts[-1*i-1] for i in range(len(keys))], bins=bins, histtype='fill', stack=True, label=[keys[-1*i-1].replace('_','-') for i in range(len(keys))])
#
#        counts = np.array(counts)
#        sumcounts = np.sum(counts, axis=0)
#
#        mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=sumcounts), yerr=np.sqrt(sumcounts), label='Sum', ax=ax, histtype='errorbar', color='k')
#
#else:
#    for j in range(len(thishist)):
#        mplhep.histplot(thishist[j], label=keys[j].replace('_','-'), ax=ax, density=normalized)



mplhep.cms.label(loc=1)
ax.set_ylabel(ylab)
ax.set_xlabel(xlab)
ax.set_title(title)
ax.legend()
if logscale:
    ax.set_yscale('log')
plt.savefig(fname)
plt.close(fig)
        
        


