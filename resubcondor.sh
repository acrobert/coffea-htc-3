#!/bin/bash                                                                                                                                                                                                        
key=$1
evtsperfile=$2
nfiles=$3
objtype=$4

rm subcondor

for i in 0
do
    echo +MaxRuntime = 86400 >> subcondor
    echo Executable = pqmaker.sh >> subcondor
    echo Arguments = $key $i $evtsperfile $objtype >> subcondor
    echo Should_transfer_files = YES >> subcondor
    echo x509userproxy = /uscms/home/acrobert/x509up_u57074 >> subcondor
    echo Use_x509userproxy = true >> subcondor
    echo Getenv = true >> subcondor
    echo Output          = /uscms/home/acrobert/nobackup/zto4b/htc/logs/${key}_${objtype}_${evtsperfile}evts_${i}.out >> subcondor
    echo Error           = /uscms/home/acrobert/nobackup/zto4b/htc/logs/${key}_${objtype}_${evtsperfile}evts_${i}.err >> subcondor
    echo Log             = /uscms/home/acrobert/nobackup/zto4b/htc/logs/${key}_${objtype}_${evtsperfile}evts_${i}.log >> subcondor
    echo Queue >> subcondor
done

condor_submit subcondor
