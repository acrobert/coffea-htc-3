import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
from argparse import ArgumentParser
#from classRunCoffea import RunCoffea
import os, psutil
import tracemalloc
import gc
from copy import deepcopy
from htcutils import *
import pickle as pkl

tmr = timer()

import sys
print('sys argv', sys.argv)
name = sys.argv[1]
hltkey = sys.argv[2]
selection = sys.argv[3]
selectionlst = selection.split(',')
fillhist = sys.argv[4]
fillhistlst = fillhist.split(',')
btagthresh = btagthreshs[sys.argv[5]]
files = sys.argv[6]
xmin = [float(k) for k in sys.argv[7].split(',')]
xmax = [float(k) for k in sys.argv[8].split(',')]
nbins = int(sys.argv[9])
filepairs = [arg for arg in files.split('=')]
splitpairs = [p.split('+') for p in filepairs]

print(name, splitpairs)
print('args', name, hltkey, selection, fillhist, btagthresh)

for j in range(len(splitpairs)):
    print(splitpairs[j])
    splitpairs[j][1] = splitpairs[j][1].replace('[','').replace(']','').replace("'",'')


thishist = [Hist(hist.axis.Regular(nbins, xmin[f], xmax[f], name=fillhistlst[f])) for f in range(len(fillhistlst))]

nall = 0
nevts = [0 for f in fillhistlst]

for j in range(len(splitpairs)):
    
    nanofile, friendfile = splitpairs[j]
    tagnano = loadpair(nanofile, [friendfile]) 
    
    if tagnano is None:
        continue
    if 'Zs' not in tagnano.fields:
        continue

    print(len(tagnano))
   
    print(f'Running file pair {j} filled {nevts}; elapsed time {round(tmr.time()/60., 3)}')

    nall += len(tagnano)

    print(btagthresh, nanofile, 'g-j' in nanofile)
    for i in range(len(selectionlst)):
        if 'data' in name and 'yAyB' in selectionlst[i]:
            selnano = selections[selectionlst[i]](tagnano, hltkey, btagthresh, blind=True)
        else:
            selnano = selections[selectionlst[i]](tagnano, hltkey, btagthresh)
        #print(j, i, selnano)
        nevts[i] += fillhists[fillhistlst[i]](selnano, thishist[i], btagthresh, signal=('j-g' in nanofile))
        del selnano

    del tagnano

    print('Current memory:', psutil.Process(os.getpid()).memory_info().rss / 1024 ** 2)
    gc.collect()

for i in range(len(selectionlst)):
    counts, bins = thishist[i].to_numpy()
    print(i, selectionlst[i], fillhistlst[i], counts) 
outdict = {'hist': thishist, 'nall': nall, 'nevts': nevts}
print(outdict)
with open(f"{name}.pkl", "wb") as output_file:
    pkl.dump(outdict, output_file)
