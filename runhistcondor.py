import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
import matplotlib.pyplot as plt
import mplhep
from argparse import ArgumentParser
from classRunCoffea import RunCoffea
import os, psutil
import tracemalloc
import gc
from copy import deepcopy
import matplotlib.ticker as ticker
from time import sleep
from htcutils import *
import pickle as pkl
import sys

plt.rcParams['text.usetex'] = True
tmr = timer()

run = sys.argv[1] == 'run' 

types = ['TTJets', 'VJets', 'QCD', 'ZJets to 4B']
#keys = [['TTJets_up20UL18'], ['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['QCD_HT10to15_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['WZToLNu2B-NLO-1j-g0', 'ZZTo2L2B-NLO-1j-g0']]
#keys = [['TTJets_up20UL18'], ['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['QCD_HT10to15_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['WZToLNu2B-NLO-1j-g0', 'ZZTo2Nu2B-NLO-1j-g0']]
keys = [['TTJets_up20UL18'], ['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['QCD_HT10to15_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['ZJetsTo2B-HT4to6-4j-g0', 'ZJetsTo2B-HT6to8-4j-g0', 'ZJetsTo2B-HT800-4j-g0']]

#types = ['VJets', 'VZ to 4B']
#keys = [['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['ZZTo2L2B-NLO-1j-g0', 'WZToLNu2B-NLO-1j-g0']]

if run:
    print(' >> running hists')
else:
    print(' >> reading hists')

fillhist = 'dijetptsum'
selection = 'zjhlt_b_bb_dR1p5'
btagthresh = 'medium'

tag = f'{selection}_btag{btagthresh}_dijetptsum'

xmin = 0
xmax = 3000
nbins = 30
xlab = 'Dijet Pt Sum'
ylab = 'Normalized Events'
logscale = False
title = r'BBJet-BJet Scalar Pt Sum'
fname = f'plots/combinedhist_{tag}.pdf'
normalized = True
run2rescale = False
stack = False
combinetypes = True
showSB = False
pucut = 4

def spawnCondor(outname, hltkey, infiles, submit=False):
    logname = outname
    subcondor_template = """
Executable            = makeHist.sh
Arguments             = {} {} {} {} {} {} {} {} {}
Should_transfer_files = NO
x509userproxy         = /uscms/home/acrobert/x509up_u57074
Use_x509userproxy     = true
Output                = /uscms/home/acrobert/nobackup/zto4b/htc/logs/{}.out
Error                 = /uscms/home/acrobert/nobackup/zto4b/htc/logs/{}.err
Log                   = /uscms/home/acrobert/nobackup/zto4b/htc/logs/{}.log
Queue
""".format(outname, hltkey, selection, fillhist, btagthresh, infiles, xmin, xmax, nbins, logname, logname, logname)

    with open('subcondorHist','a') as sub:
        sub.write(subcondor_template)

    if submit:
        os.system('condor_submit subcondorHist')



nfiles = 0
filesperhist = 10

if run:
    for fl in ['datadicts','utils','htcutils','exechtc']:
        os.system(f'xrdcp -f {fl}.py root://cmseos.fnal.gov//store/user/acrobert/condor/.')
    
    for k in range(len(types)):
        for i in range(len(keys[k])):
    
            key = keys[k][i]
            
            print(f'Running key {key}; elapsed time {round(tmr.time()/60., 3)}')
            if key == 'TTJets_up20UL18':
                filepairs = RunCoffea.initnanos(key, mode='1Fr')
            else:
                filepairs = RunCoffea.initnanos(key)
    
            nthreads = len(filepairs) // filesperhist + 1
            nfiles += nthreads
            #print(filepairs[0:20])
            for j in range(nthreads):
                s = '='.join([str(a)+'+'+str(b) for a, b in filepairs[j*filesperhist : (j+1)*filesperhist]])
    
                spawnCondor(f'{key}_{tag}_hist{j}', keys[-1][-1], s)
    
    
    print('Spawning {} condor jobs...'.format(nfiles))
    os.system('condor_submit subcondorHist')
    print('Submitted!')
    
    os.system('rm subcondorHist')
    
    while True:
    
        print(round(tmr.time()/60., 3))
        print(checkcq(tag))
        if checkcq(tag) == {}:
            break
        sleep(30)

countslist = [[ np.zeros((nbins)) for i in keys[j] ] for j in range(len(types)) ]
pucut = 4

os.system('xrdfs root://cmseos.fnal.gov/ ls /store/user/acrobert/condor/pkl/ > lstmp.txt 2>&1')    
lsout = open('lstmp.txt','r')
lslines = lsout.readlines()

nall = [[0 for i in keys[k]] for k in range(len(types))]
nevts = [[0 for i in keys[k]] for k in range(len(types))]

bins = None
    
for i in range(len(types)):
    for j in range(len(keys[i])):
        for line in lslines:
            if keys[i][j] in line and tag in line:

                rfname = 'root://cmseos.fnal.gov/'+line.rstrip()
                locname = line.rstrip().split('/')[-1]
                os.system(f'xrdcp {rfname} pkl/.')

                with open('pkl/'+locname, "rb") as output_file:
                    outdict = pkl.load(output_file)
                    #print(outdict)
                    c, bins = outdict['hist'].to_numpy()
                    countslist[i][j] += c
                    nall[i][j] += outdict['nall']
                    nevts[i][j] += outdict['nevts']

                os.system('rm pkl/'+locname)

os.system('rm lstmp.txt')

print(nall, nevts)

if showSB:
    fig = plt.figure(figsize=(8, 5))
    gs = fig.add_gridspec(2, 1, height_ratios=(4, 1))

    ax2 = fig.add_subplot(gs[1,0])
    ax1 = fig.add_subplot(gs[0,0], sharex=ax2)

    fig.subplots_adjust(hspace=0.1)

else:
    fig, ax1 = plt.subplots(figsize=(8, 5))


finalcounts = []
finalerrs = []
finalkeys = []
finalnevts = []

for i in range(len(types)):

    if not combinetypes:
        for j in range(len(keys[i])):
            count = countslist[i][j]
            finalcounts.append(count*signalevents[keys[i][j]]/nall[i][j])
            finalerrs.append(np.sqrt(count)*signalevents[keys[i][j]]/nall[i][j])
            finalkeys.append(keys[i][j])
            finalnevts.append(round(nevts[i][j]*signalevents[keys[i][j]]/nall[i][j], 2))
            continue

    counts = []
    errs = []
    tnevts = []

    for j in range(len(keys[i])):

        count = countslist[i][j]
        newcount = count*signalevents[keys[i][j]]/nall[i][j]
        counts.append(newcount)
        errs.append(np.sqrt(count)*signalevents[keys[i][j]]/nall[i][j])
        tnevts.append(round(nevts[i][j]*signalevents[keys[i][j]]/nall[i][j], 2))

    counts = np.array(counts)
    errs = np.array(errs)

    typecount = np.sum(counts, axis=0)
    typeerrs = np.sqrt(np.sum(errs**2, axis=0))

    finalcounts.append(typecount)
    finalerrs.append(typeerrs)
    finalkeys.append(types[i])
    finalnevts.append(sum(tnevts))

for i in range(len(finalcounts)):
    mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=finalcounts[i]), yerr=finalerrs[i], label=finalkeys[i].replace('_','-')+' (N='+str(finalnevts[i])+')', ax=ax1, density=normalized)

if run2rescale and stack:

    mplhep.histplot([finalcounts[-1*i-1] for i in range(len(finalkeys))], bins=bins, histtype='fill', stack=True, label=[finalkeys[-1*i-1].replace('_','-') for i in range(len(finalkeys))], ax=ax1)

    tcounts = np.array(finalcounts)
    sumcounts = np.sum(tcounts, axis=0)

    mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=sumcounts), yerr=np.sqrt(sumcounts), label='Sum', ax=ax1, histtype='errorbar', color='k')

mplhep.cms.label(loc=1, ax=ax1)
ax1.set_ylabel(ylab)
ax1.set_title(title)
ax1.legend()
if logscale:
    ax1.set_yscale('log')

if showSB:
    ax1.tick_params(labelbottom=False)
    ax2.set_xlabel(xlab)
    ax2.set_ylabel(r'$S/\sqrt{B}$')

    sig = np.array(finalcounts[-1])
    sigma_s = np.array(finalerrs[-1])
    bkg = np.sum(np.array(finalcounts[:-1]), axis=0)
    sigma_b = np.sqrt(np.sum(np.array(finalerrs[:1])**2, axis=0))

    ratios = np.nan_to_num(sig / np.sqrt(bkg), nan=0., posinf=0.)
    raterrs = np.nan_to_num(ratios * np.sqrt( sigma_s**2/sig**2 + 1/4 * sigma_b**2/bkg**2 ), nan=0., posinf=0.)
    mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=ratios), yerr=np.sqrt(raterrs), ax=ax2, histtype='errorbar', color='k', markersize=2.)
else:
    ax1.set_xlabel(xlab)

print(fname)
plt.savefig(fname)
plt.close(fig)

rmyn = input('Type "yes" to delete all hists in xrd dir, else enter: ')
if rmyn == 'yes':
    for i in range(len(types)):
        for j in range(len(keys[i])):
            for line in lslines:
                if keys[i][j] in line and tag in line:
                    os.system('xrdfs root://cmseos.fnal.gov/ rm '+line.rstrip())



    
