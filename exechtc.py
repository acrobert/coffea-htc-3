import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
from argparse import ArgumentParser
import os, psutil
import tracemalloc
import gc
from copy import deepcopy
from htcutils import *
import pickle as pkl

tmr = timer()

import sys
name = sys.argv[1]
hltkey = sys.argv[2]
selection = sys.argv[3]
fillhist = sys.argv[4]
btagthresh = btagthreshs[sys.argv[5]]
files = sys.argv[6]
xmin = float(sys.argv[7])
xmax = float(sys.argv[8])
nbins = int(sys.argv[9])
filepairs = [arg for arg in files.split('=')]
splitpairs = [p.split('+') for p in filepairs]

print(name, splitpairs)
print('args', name, hltkey, selection, fillhist, btagthresh)
for j in range(len(splitpairs)):
    splitpairs[j][1] = splitpairs[j][1].replace('[','').replace(']','').replace("'",'')

thishist = Hist(hist.axis.Regular(nbins, xmin, xmax, name='bin'))
pucut = 4

nall = 0
nevts = 0

for j in range(len(splitpairs)):
    
    nanofile, friendfile = splitpairs[j]
    tagnano = loadpair(nanofile, [friendfile]) 
    
    if tagnano is None:
        continue
    if 'Zs' not in tagnano.fields:
        continue
   
    print(f'Running file pair {j} filled {nevts}; elapsed time {round(tmr.time()/60., 3)}')

    nall += len(tagnano)

    tagnano = selections[selection](tagnano, hltkey, btagthresh)
    nevts += fillhists[fillhist](tagnano, thishist, btagthresh)

    del tagnano
    print('Current memory:', psutil.Process(os.getpid()).memory_info().rss / 1024 ** 2)
    gc.collect()

counts, bins = thishist.to_numpy()
print(counts)

outdict = {'hist': thishist, 'nall': nall, 'nevts': nevts}
print(outdict)
with open(f"{name}.pkl", "wb") as output_file:
    pkl.dump(outdict, output_file)

print(f"{name}.pkl", outdict)
