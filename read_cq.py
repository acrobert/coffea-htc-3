import os
from time import sleep
from utils import *

tmr = timer()

def checkcq(): 
    statuses = {}
    os.system('condor_q > cqtmp.txt 2>&1')
    try:
        with open('cqtmp.txt','r') as cqout:
            cqlines = cqout.readlines()
            for line in cqlines:
                if 'makeHist.sh' in line:
                    linelist = line.split(' ')
                    lineinfo = list(filter(lambda a: a != '', linelist[:-1]))
                    
                    key = lineinfo[-1]
                    if key not in statuses.keys():
                        statuses[key] = [0, 0, 0]
    
                    if lineinfo[5] == 'I':
                        statuses[key][0] += 1
                    elif lineinfo[5] == 'R':
                        statuses[key][1] += 1
                    else:
                        statuses[key][2] += 1
        os.system('rm cqtmp.txt')
    except IOError:
        pass

    return statuses


while True:
    
    print(round(tmr.time()/60., 3))

    print(checkcq())

    sleep(10)
