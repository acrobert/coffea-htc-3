import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
import matplotlib.pyplot as plt
import mplhep
from argparse import ArgumentParser
from classRunCoffea import RunCoffea
import os, psutil
import tracemalloc
import gc
from copy import deepcopy
import matplotlib.ticker as ticker
from time import sleep
from htcutils import *
import pickle as pkl
import sys
import math

#plt.rcParams['text.usetex'] = True
#site = 'cmsdata.phys.cmu.edu'
site = 'cmseos.fnal.gov'

#keys = [['TTJets_up20UL18'], ['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['QCD_HT10to15_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['WZToLNu2B-NLO-1j-g0', 'ZZTo2L2B-NLO-1j-g0']]
#keys = [['TTJets_up20UL18'], ['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['QCD_HT10to15_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['WZToLNu2B-NLO-1j-g0', 'ZZTo2Nu2B-NLO-1j-g0']]

#types = ['VJets', 'VZ to 4B']
#keys = [['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['ZZTo2L2B-NLO-1j-g0', 'WZToLNu2B-NLO-1j-g0']]

class histsrunner:

    def __init__(self, fillhist, selection, btagthresh, addtag = None, style = None):
        
        #self.types = ['QCD', 'Data']
        #self.keys = [['QCD_HT10to15_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['data_JetHT_up20UL18_A']]
        self.types = ['QCD', 'Data']
        self.keys = [['QCD_HT10to15_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['data_JetHT_up20UL18_A']]
        
        self.hltkey = 'ZJetsTo2B-HT800-4j-g0'

        self.nfiles = 0
        self.filesperhist = 20

        self.tmr = timer()

        self.fillhist = fillhist
        self.selection = selection
        self.btagthresh = btagthresh
        
        self.tag = f'{selection}_btag{btagthresh}_{fillhist}' + (f'_{addtag}' if (addtag is not None and addtag != '') else '')
        
        if style is not None:
            self.style = style

        self.nhists = len(selection.split(','))

    def spawnCondor(self, outname, infiles, xmin, xmax, nbins, submit=False):
        logname = outname
        #execsh = 'makeHist.sh' if not self.multi else 'makeHist_multi.sh'
        #execpy = 'exechtc.py' if not self.multi else 'exechtc_multi.py'
        execsh = 'makeHist_multi.sh'
        execpy = 'exechtc_multi.py'
        subcondor_template = """
Executable            = {}
Arguments             = {} {} {} {} {} {} {} {} {}
Should_transfer_files = YES
transfer_output_files = {}
x509userproxy         = /uscms/home/acrobert/x509up_u57074
Use_x509userproxy     = true
Output                = /uscms/home/acrobert/nobackup/zto4b/htc/logs/{}.out
Error                 = /uscms/home/acrobert/nobackup/zto4b/htc/logs/{}.err
Log                   = /uscms/home/acrobert/nobackup/zto4b/htc/logs/{}.log
Queue
""".format(execsh, outname, self.hltkey, self.selection, self.fillhist, self.btagthresh, infiles, xmin, xmax, nbins, execpy, logname, logname, logname)

        with open('subcondorHist','a') as sub:
            sub.write(subcondor_template)

        if submit:
            os.system('condor_submit subcondorHist')

    def addtocondor(self, key, xmin, xmax, nbins):
        
        print(f'Running key {key}; elapsed time {round(self.tmr.time()/60., 3)}')
        filepairs = RunCoffea.initnanos(key, date='Apr2024')
                
        nthreads = len(filepairs) // self.filesperhist + 1
        self.nfiles += nthreads
        for j in range(nthreads):
            s = '='.join([str(a)+'+'+str(b) for a, b in filepairs[j*self.filesperhist : (j+1)*self.filesperhist]])
            self.spawnCondor(f'{key}_{self.tag}_hist{j}', s, xmin, xmax, nbins)
    


    def runhists(self, xmin, xmax, nbins):
        for fl in ['datadicts','utils','htcutils','exechtc', 'exechtc_multi']:
            os.system(f'xrdcp -f {fl}.py root://{site}//store/user/acrobert/condor/.')
    
        for k in range(len(self.types)):
            for i in range(len(self.keys[k])):    
                key = self.keys[k][i]
                self.addtocondor(key, xmin, xmax, nbins)
    
        print('Spawning {} condor jobs...'.format(self.nfiles))
        os.system('condor_submit subcondorHist')
        print('Submitted!')
    
        os.system('rm subcondorHist')
        self.nbins = nbins
        self.checkhists()

    def checkhists(self):
        while True:
    
            print(round(self.tmr.time()/60., 3))
            print(checkcq(self.tag))
            if checkcq(self.tag) == {}:
                break
            sleep(30)
        
    def getpkls(self, key):

        os.system(f'xrdfs root://{site}/ ls /store/user/acrobert/condor/pkl/ > lstmp.txt 2>&1')
        lsout = open('lstmp.txt','r')
        lslines = lsout.readlines()
        
        thisnall = 0
        thisnevts = [0 for j in range(self.nhists)]
        #thiscountslist = [ np.zeros((self.nbins)) for k in range(self.nhists) ]
        thiscountslist = None
        
        for line in lslines:
            if key in line and self.tag in line:

                rfname = f'root://{site}/'+line.rstrip()
                locname = line.rstrip().split('/')[-1]
                os.system(f'xrdcp {rfname} pkl/.')
                
                with open('pkl/'+locname, "rb") as output_file:
                    outdict = pkl.load(output_file)
                    for ih in range(self.nhists):
                        c, bins = outdict['hist'][ih].to_numpy()
                        self.bins = bins
                        if thiscountslist is None:
                            self.nbins = len(c)
                            thiscountslist = [ np.zeros((self.nbins)) for k in range(self.nhists) ] 
                        thiscountslist[ih] += c
                        thisnevts[ih] += outdict['nevts'][ih]
                    thisnall += outdict['nall']
                                                    
                os.system('rm pkl/'+locname)
                        
        os.system('rm lstmp.txt')

        return thisnall, thisnevts, thiscountslist

    def buildcounts(self):

        nall = [[0 for i in self.keys[k]] for k in range(len(self.types))]
        nevts = [[[0 for j in range(self.nhists)] for i in self.keys[k]] for k in range(len(self.types))]

        countslist = None
        #countslist = [[[ np.zeros((self.nbins)) for k in range(self.nhists) ] for i in self.keys[j] ] for j in range(len(self.types)) ]
        for it in range(len(self.types)):
            for ik in range(len(self.keys[it])):
                thisnall, thisnevts, thiscountslist = self.getpkls(self.keys[it][ik])
                nall[it][ik] += thisnall
                if countslist is None:
                    self.nbins = len(thiscountslist[0])
                    countslist = [[[ np.zeros((self.nbins)) for k in range(self.nhists) ] for i in self.keys[j] ] for j in range(len(self.types)) ]
                
                for ih in range(self.nhists):
                    nevts[it][ik][ih] += thisnevts[ih]
                    countslist[it][ik][ih] += thiscountslist[ih]
                    
        print(nall, nevts)

        return nall, nevts, countslist

    def rescalecounts(self, thesekeys, typenall, typenevts, typecountslist):

        retcounts = [[] for ih in range(self.nhists)]
        reterrs = [[] for ih in range(self.nhists)]
        retnevts = [[] for ih in range(self.nhists)]

        for ik in range(len(thesekeys)):
            thiskey = thesekeys[ik]
            if 'data' in thiskey:
                rescale = dsetlumi['Run2'] / dsetlumi[thiskey]
            else:
                rescale = signalevents[thiskey] * filtereffs[thiskey] / typenall[ik]
            for ih in range(self.nhists):
                count = typecountslist[ik][ih]
                retcounts[ih].append(count * rescale) 
                reterrs[ih].append(np.sqrt(count) * rescale )
                retnevts[ih].append(typenevts[ik][ih] * rescale )

        return retcounts, reterrs, retnevts

    def addcms(self, ax, key):
        
        if ('data' in key or 'Data' in key):
            cmsstr = 'data'
        else:
            cmsstr = 'simulation'
        ax.text(x=0, y=1.005, s=f"Private work (CMS {cmsstr})", transform=ax.transAxes, ha="left", va="bottom",
                fontsize=10, fontweight="normal", fontname="TeX Gyre Heros")
        ax.text(x=1, y=1.005, s=r"138 $\mathrm{fb^{-1}}$ (13 TeV)", transform=ax.transAxes, ha="right", va="bottom",
                fontsize=10, fontweight="normal", fontname="TeX Gyre Heros")

        

    def readhists(self, xlab, ylab, title, logscale, normalized, run2rescale, stack, combinetypes, showSB, mean_spread, separate):
        #countslist = [[ np.zeros((nbins)) for i in self.keys[j] ] for j in range(len(self.types)) ]
        countslist = []

        if separate and showSB:
            raise ValueError

        nall, nevts, countslist = self.buildcounts()

        finalcounts = [[] for ih in range(self.nhists)]
        finalerrs = [[] for ih in range(self.nhists)]
        finalkeys = []
        finalnevts = [[] for ih in range(self.nhists)]

        for it in range(len(self.types)):

            thiscounts, thiserrs, thisnevts = self.rescalecounts(self.keys[it], nall[it], nevts[it], countslist[it])
            if not combinetypes:
                finalkeys += self.keys[it]
                for ih in range(self.nhists):
                    finalcounts[ih] += thiscounts[ih]
                    finalerrs[ih] += thiserrs[ih]
                    finalnevts[ih] += thisnevts[it]
            else:
                finalkeys.append(self.types[it])
                for ih in range(self.nhists):
                    counts = np.array([thiscounts[ih][ik] for ik in range(len(self.keys[it]))])
                    errs = np.array([thiserrs[ih][ik] for ik in range(len(self.keys[it]))])
                    typecount = np.sum(counts, axis=0)
                    typeerrs = np.sqrt(np.sum(errs**2, axis=0))

                    finalcounts[ih].append(typecount)
                    finalerrs[ih].append(typeerrs)
                    
                    finalnevts[ih].append(sum([thisnevts[ih][ik] for ik in range(len(self.keys[it]))]))
                    
        for ic in range(len(finalcounts[0])):
            fig = plt.figure(figsize=(8, 5))
            gs = fig.add_gridspec(2, 1, height_ratios=(4, 1))
            
            ax2 = fig.add_subplot(gs[1,0])
            ax1 = fig.add_subplot(gs[0,0], sharex=ax2)
            
            fig.subplots_adjust(hspace=0.1)

            key = self.keys[ic][0]
            print(key,  ('data' in key or 'Data' in key))
            labs = ['VR' if ('data' in key or 'Data' in key) else 'SR and VR', 'CR1', 'CR2', 'CR3']
        

            
            for ih in range(self.nhists):
  
                mplhep.histplot(np.histogram(self.bins[0:-1], bins=self.bins, weights=finalcounts[ih][ic]), yerr=finalerrs[ih][ic], label=labs[ih]+' (N='+str(round(finalnevts[ih][ic]))+')', ax=ax1, density=normalized)

            estsig = finalcounts[1][ic] * finalcounts[2][ic] / finalcounts[3][ic]
            estsig = np.nan_to_num(estsig)
            estsig[estsig > 100000000.] = 0.
            esterr = estsig * np.sqrt( (finalerrs[1][ic]/finalcounts[1][ic])**2 + (finalerrs[2][ic]/finalcounts[2][ic])**2 + (finalerrs[3][ic]/finalcounts[3][ic])**2 )
            esterr = np.nan_to_num(esterr)
            mplhep.histplot(np.histogram(self.bins[0:-1], bins=self.bins, weights=estsig), yerr=esterr, label='SR Estimate', ax=ax1, density=normalized)

            self.addcms(ax1, self.types[ic])
            ax1.set_ylabel(ylab)
            ax1.set_title(title)
            ax1.legend()
            if logscale:
                ax1.set_yscale('log')
            
            if showSB:
                ax1.tick_params(labelbottom=False)
                ax2.set_xlabel(xlab)
                ax2.set_ylabel(r'Predicted/Actual')

                sig = estsig
                sigma_s = esterr
                bkg = finalcounts[0][ic]
                sigma_b = finalerrs[0][ic]

                ratios = np.nan_to_num(sig / bkg, nan=0., posinf=0.)
                raterrs = np.nan_to_num(ratios * np.sqrt( sigma_s**2/sig**2 + sigma_b**2/bkg**2 ), nan=0., posinf=0.)
                mplhep.histplot(np.histogram(self.bins[0:-1], bins=self.bins, weights=ratios), yerr=np.sqrt(raterrs), ax=ax2, histtype='errorbar', color='k', markersize=2.)
            
                x1 = [self.bins[0], self.bins[-1]]
                y1 = [1., 1.]
                 
                ax2.plot(x1, y1, linestyle='--', color='k')
                ax2.set_yscale('log')

                if finalkeys[ic] == 'Data' or 'data' in finalkeys[ic]:
                    import matplotlib.patches as patches
                    rct = patches.Rectangle((70, 10), 40, -20, facecolor='0.5', alpha=0.5)
                    ax2.add_patch(rct)
                    ax2.text(78, 0.45, 'Blinded')
                    
            
            temptag = 'ABCD_btagmedium_dijetmass'
            #fname = f'plots/bkghist_{temptag}.pdf'
            #if self.style is not None:
            fname = f'plots/bkghist_{temptag}_{finalkeys[ic]}.pdf'
            print(fname)
            plt.savefig(fname)
            plt.close(fig)
            
        rmyn = input('Type "yes" to delete all hists in xrd dir, else enter: ')
        if rmyn == 'yes':
            os.system(f'xrdfs root://{site}/ ls /store/user/acrobert/condor/pkl/ > lstmp.txt 2>&1')
            lsout = open('lstmp.txt','r')
            lslines = lsout.readlines()
            os.system('rm lstmp.txt')
            for i in range(len(self.types)):
                for j in range(len(self.keys[i])):
                    for line in lslines:
                        if self.keys[i][j] in line and self.tag in line:
                            os.system(f'xrdfs root://{site}/ rm '+line.rstrip())



    
        def addmeanwidth(self, ax, counts, color, density=False):

            binwidth = self.bins[1] - self.bins[0]
            mean = np.sum((self.bins[:-1] + binwidth/2) * (counts)) / np.sum(counts)
            
            if density:
                densityvals = counts/np.sum(counts * (binwidth))
                maxval = np.max(densityvals)
            else:
                maxval = np.max(counts)

            stdev = np.sqrt(np.sum(counts * (self.bins[:-1] + binwidth/2 - mean)**2 ) / (np.sum(counts)-1) )
            fwhm = 2*np.sqrt(2*(math.log(2)))*stdev
            
            x1 = [mean, mean]
            y1 = [0., maxval]
            x2 = [mean - fwhm/2, mean + fwhm/2]
            y2 = [0.1*maxval + 0.03*maxval*i, 0.1*maxval + 0.03*maxval*i]
            ax.plot(x2, y2, linestyle='--', color=color)
            ax.plot(x1, y1, linestyle='--', color=color)
