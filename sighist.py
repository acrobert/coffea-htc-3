import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
import matplotlib.pyplot as plt
import mplhep
from argparse import ArgumentParser
from classRunCoffea import RunCoffea
import os, psutil
import tracemalloc
import gc
from copy import deepcopy
import matplotlib.ticker as ticker
from time import sleep
from htcutils import *
import pickle as pkl
import sys
import math

#plt.rcParams['text.usetex'] = True
#site = 'cmsdata.phys.cmu.edu'
site = 'cmseos.fnal.gov'

#types = ['QCD', 'Data']
#keys = [['QCD_HT10to15_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['data_JetHT_up20UL18_A']]
types = ['Data', 'Z to 4b']
keys = [['data_JetHT_up20UL18_A'], ['ZJetsTo2B-HT6to8-4j-g0', 'ZJetsTo2B-HT800-4j-g0']]

hltkey = 'ZJetsTo2B-HT800-4j-g0'

tmr = timer()

fillhistlst = ['dijetmass_b_bb', 'dijetmass_b_bb', 'dijetmass_b_b', 'dijetmass_b_b']
selectionlst = ['yAyB', 'yAnB', 'nAyB', 'nAnB']

fillhist = ','.join(fillhistlst)
selection = ','.join(selectionlst)
print(fillhist, selection)
btagthresh = 'medium'

datatag = f'{selection}_btag{btagthresh}_{fillhist}'
mctag = f'{selectionlst[0]}_btag{btagthresh}_{fillhistlst[0]}'

thisbins = None

def getpkls(key, tag, nhists):

    os.system(f'xrdfs root://{site}/ ls /store/user/acrobert/condor/pkl/ > lstmp.txt 2>&1')
    lsout = open('lstmp.txt','r')
    lslines = lsout.readlines()
    
    thisnall = 0
    thisnevts = [0 for j in range(nhists)]
    #thiscountslist = [ np.zeros((nbins)) for k in range(nhists) ]
    thiscountslist = None

    for line in lslines:
        if key in line and tag in line:

            rfname = f'root://{site}/'+line.rstrip()
            locname = line.rstrip().split('/')[-1]
            os.system(f'xrdcp {rfname} pkl/.')
            
            with open('pkl/'+locname, "rb") as output_file:
                outdict = pkl.load(output_file)
                if nhists > 1:
                    for ih in range(nhists):
                        c, bins = outdict['hist'][ih].to_numpy()
                        if thiscountslist is None:
                            nbins = len(c)
                            thiscountslist = [ np.zeros((nbins)) for k in range(nhists) ] 
                        thiscountslist[ih] += c
                        thisnevts[ih] += outdict['nevts'][ih]
                else:
                    c, bins = outdict['hist'].to_numpy()
                    if thiscountslist is None:
                        nbins = len(c)
                        thiscountslist = [ np.zeros((nbins)) for k in range(nhists) ]
                    thiscountslist[0] += c
                    thisnevts[0] += outdict['nevts']

                thisnall += outdict['nall']
                                                
            os.system('rm pkl/'+locname)
                    
    os.system('rm lstmp.txt')

    return thisnall, thisnevts, thiscountslist

def buildcounts():

    nall = [[0 for i in keys[k]] for k in range(len(types))]
    nevts = [[[0 for j in range(4 if 'data' in i else 1)] for i in keys[k]] for k in range(len(types))]

    countslist = None
    #countslist = [[[ np.zeros((nbins)) for k in range(nhists) ] for i in keys[j] ] for j in range(len(types)) ]
    for it in range(len(types)):
        nhists = 4 if types[it] == 'Data' else 1
        for ik in range(len(keys[it])):
            thistag = datatag if 'data' in keys[it][ik] else mctag
            thisnall, thisnevts, thiscountslist = getpkls(keys[it][ik], thistag, nhists)
            nall[it][ik] += thisnall
            if countslist is None:
                nbins = len(thiscountslist[0])
                countslist = [[[ np.zeros((nbins)) for k in range(nhists) ] for i in keys[j] ] for j in range(len(types)) ]
            
            for ih in range(nhists):
                nevts[it][ik][ih] += thisnevts[ih]
                countslist[it][ik][ih] += thiscountslist[ih]
                
    print(nall, nevts)

    return nall, nevts, countslist

def rescalecounts(thesekeys, typenall, typenevts, typecountslist, nhists):

    retcounts = [[] for ih in range(nhists)]
    reterrs = [[] for ih in range(nhists)]
    retnevts = [[] for ih in range(nhists)]

    for ik in range(len(thesekeys)):
        thiskey = thesekeys[ik]
        if 'data' in thiskey:
            rescale = dsetlumi['Run2'] / dsetlumi[thiskey]
        else:
            rescale = signalevents[thiskey] * filtereffs[thiskey] / typenall[ik]
        for ih in range(nhists):
            count = typecountslist[ik][ih]
            retcounts[ih].append(count * rescale) 
            reterrs[ih].append(np.sqrt(count) * rescale )
            retnevts[ih].append(typenevts[ik][ih] * rescale )

    return retcounts, reterrs, retnevts

def addcms(ax, key):
    
    cmsstr = 'data/simulation'
    ax.text(x=0, y=1.005, s=f"Private work (CMS {cmsstr})", transform=ax.transAxes, ha="left", va="bottom",
            fontsize=10, fontweight="normal", fontname="TeX Gyre Heros")
    ax.text(x=1, y=1.005, s=r"138 $\mathrm{fb^{-1}}$ (13 TeV)", transform=ax.transAxes, ha="right", va="bottom",
            fontsize=10, fontweight="normal", fontname="TeX Gyre Heros")

def readhists(xlab, ylab, title, logscale, normalized, run2rescale, stack, combinetypes, showSB, mean_spread, separate):
    #countslist = [[ np.zeros((nbins)) for i in keys[j] ] for j in range(len(types)) ]
    countslist = []

    if separate and showSB:
        raise ValueError

    dnhists = 4
    nall, nevts, countslist = buildcounts()

    datacounts = [[] for ih in range(dnhists)]
    dataerrs = [[] for ih in range(dnhists)]
    datakeys = []
    datanevts = [[] for ih in range(dnhists)]

    it = 0
    thiscounts, thiserrs, thisnevts = rescalecounts(keys[it], nall[it], nevts[it], countslist[it], dnhists)
        
    for ih in range(dnhists):
        counts = np.array([thiscounts[ih][ik] for ik in range(len(keys[it]))])
        errs = np.array([thiserrs[ih][ik] for ik in range(len(keys[it]))])
        typecount = np.sum(counts, axis=0)
        typeerrs = np.sqrt(np.sum(errs**2, axis=0))
        
        datacounts[ih].append(typecount)
        dataerrs[ih].append(typeerrs)
        
        datanevts[ih].append(sum([thisnevts[ih][ik] for ik in range(len(keys[it]))]))
        
    mnhists = 1
    #mnall, mnevts, mcountslist = buildcounts(mnhists)

    mccounts = [[] for ih in range(mnhists)]
    mcerrs = [[] for ih in range(mnhists)]
    mckeys = []
    mcnevts = [[] for ih in range(mnhists)]

    it = 1
    thiscounts, thiserrs, thisnevts = rescalecounts(keys[it], nall[it], nevts[it], countslist[it], mnhists)

    for ih in range(mnhists):
        counts = np.array([thiscounts[ih][ik] for ik in range(len(keys[it]))])
        errs = np.array([thiserrs[ih][ik] for ik in range(len(keys[it]))])
        typecount = np.sum(counts, axis=0)
        typeerrs = np.sqrt(np.sum(errs**2, axis=0))

        mccounts[ih].append(typecount)
        mcerrs[ih].append(typeerrs)

        mcnevts[ih].append(sum([thisnevts[ih][ik] for ik in range(len(keys[it]))]))

    thisbins = np.arange(0, 310, 10)
    
    fig = plt.figure(figsize=(8, 5))
    gs = fig.add_gridspec(2, 1, height_ratios=(4, 1))
    
    ax2 = fig.add_subplot(gs[1,0])
    ax1 = fig.add_subplot(gs[0,0], sharex=ax2)
    
    fig.subplots_adjust(hspace=0.1)

    # data first
    estbkg = datacounts[1][0] * datacounts[2][0] / datacounts[3][0]
    estbkg = np.nan_to_num(estbkg)
    estbkg[estbkg > 100000000.] = 0.
    esterr = estbkg * np.sqrt( (dataerrs[1][0]/datacounts[1][0])**2 + (dataerrs[2][0]/datacounts[2][0])**2 + (dataerrs[3][0]/datacounts[3][0])**2 )
    esterr = np.nan_to_num(esterr)
    mplhep.histplot(np.histogram(thisbins[0:-1], bins=thisbins, weights=estbkg), yerr=esterr, label='Bkg Estimate', ax=ax1, density=normalized)

    # now mc signal
    mplhep.histplot(np.histogram(thisbins[0:-1], bins=thisbins, weights=mccounts[0][0]), yerr=mcerrs[0][0], label='Signal', ax=ax1, density=normalized, color='C3')

    print('plotted')
    
    addcms(ax1, types[0])
    ax1.set_ylabel(ylab)
    ax1.set_title(title)
    ax1.legend()
    if logscale:
        ax1.set_yscale('log')
    
    if showSB:
        ax1.tick_params(labelbottom=False)
        ax2.set_xlabel(xlab)
        ax2.set_ylabel(r'$S/\sqrt{B}$')

        sig = mccounts[0][0]
        sigma_s = mcerrs[0][0]
        bkg = estbkg
        sigma_b = esterr

        ratios = np.nan_to_num(sig / np.sqrt(bkg), nan=0., posinf=0.)
        raterrs = np.nan_to_num(ratios * np.sqrt( sigma_s**2/sig**2 + 1/4 * sigma_b**2/bkg**2 ), nan=0., posinf=0.)
        mplhep.histplot(np.histogram(thisbins[0:-1], bins=thisbins, weights=ratios), yerr=np.sqrt(raterrs), ax=ax2, histtype='errorbar', color='k', markersize=2.)

    #print(thisbins[7:11], ratios[7:11], np.sum(sig[7:11]), np.sum(bkg[7:11]), np.sum(sig[7:11]) / np.sqrt(np.sum(bkg[7:11])))
    totsens = np.sum(sig[7:11]) / np.sqrt(np.sum(bkg[7:11]))
    sigma_num = np.sqrt(np.sum(sigma_s[7:11]**2))
    sigma_denom = np.sqrt(np.sum(sigma_b[7:11]**2))
    print(np.sqrt( sigma_num**2 / np.sum(sig[7:11])**2 + 1/4 * sigma_denom**2 / np.sum(bkg[7:11])**2 ))
    print(sigma_num**2 / np.sum(sig[7:11])**2)
    print(sigma_denom**2 / np.sum(bkg[7:11])**2)
    totsenserror = totsens * np.sqrt( sigma_num**2 / np.sum(sig[7:11])**2 + 1/4 * sigma_denom**2 / np.sum(bkg[7:11])**2 ) 
    print(totsens, totsenserror)
    text=r"""Z$\rightarrow 4b$ simulated events pass SR selection
    Background estimated with ABCD method
    $S/\sqrt{B}=4.89$ in $m_{jj} \in [70, 110] ~\mathrm{GeV}$"""
    ax1.text(x=0.01, y=0.88, s=text, transform=ax1.transAxes, ha="left", va="top",
             fontsize=10, fontweight="normal", fontname="TeX Gyre Heros")
    ax1.text(x=0.01, y=0.95, s="Projected Results", transform=ax1.transAxes, ha="left", va="top",
             fontsize=14, fontweight="normal", fontname="TeX Gyre Heros")

    
    temptag = 'datamc_btagmedium_dijetmass'
    #fname = f'plots/bkghist_{temptag}.pdf'
    #if style is not None:
    fname = f'plots/sighist_{temptag}.pdf'
    print(fname)
    plt.savefig(fname)
    plt.close(fig)
    
    rmyn = input('Type "yes" to delete all hists in xrd dir, else enter: ')
    if rmyn == 'yes':
        os.system(f'xrdfs root://{site}/ ls /store/user/acrobert/condor/pkl/ > lstmp.txt 2>&1')
        lsout = open('lstmp.txt','r')
        lslines = lsout.readlines()
        os.system('rm lstmp.txt')
        for i in range(len(types)):
            for j in range(len(keys[i])):
                for line in lslines:
                    if keys[i][j] in line and (mctag in line or datatag in line):
                        os.system(f'xrdfs root://{site}/ rm '+line.rstrip())




def addmeanwidth(self, ax, counts, color, density=False):

    binwidth = bins[1] - bins[0]
    mean = np.sum((bins[:-1] + binwidth/2) * (counts)) / np.sum(counts)
    
    if density:
        densityvals = counts/np.sum(counts * (binwidth))
        maxval = np.max(densityvals)
    else:
        maxval = np.max(counts)

    stdev = np.sqrt(np.sum(counts * (bins[:-1] + binwidth/2 - mean)**2 ) / (np.sum(counts)-1) )
    fwhm = 2*np.sqrt(2*(math.log(2)))*stdev
    
    x1 = [mean, mean]
    y1 = [0., maxval]
    x2 = [mean - fwhm/2, mean + fwhm/2]
    y2 = [0.1*maxval + 0.03*maxval*i, 0.1*maxval + 0.03*maxval*i]
    ax.plot(x2, y2, linestyle='--', color=color)
    ax.plot(x1, y1, linestyle='--', color=color)

xmin = 0.
xmax = 300.
nbins = 30
xlab = f'Dijet Mass [GeV]'
ylab = 'Run 2 Events'
logscale = False
title = f''

normalized = False
run2rescale = True
stack = False
combinetypes = True
showSB = True
mean_spread = False
separate = False

readhists(xlab, ylab, title, logscale, normalized, run2rescale, stack, combinetypes, showSB, mean_spread, separate)
