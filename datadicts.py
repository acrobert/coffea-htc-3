import os

datasets = {
      'ZJets_HT_800toInf': '/ZJetsToQQ_HT-800toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'ZGammaToJJ': '/ZGammaToJJGamma_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'WZToLNuQQ': '/WZTo1L1Nu2Q_4f_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'ZZTo2Q2L': '/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'ZZTo2Q2L_mc2017UL': '/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'WZToLNuQQ_mc2017UL': '/WZTo1L1Nu2Q_4f_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'ZGammaToJJ_mc2017UL': '/ZGammaToJJGamma_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM' 
    , 'ZJets_HT_800toInf_mc2017UL': '/ZJetsToQQ_HT-800toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM' 
    , 'ZZTo4B01j_mc2017UL': '/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v1/MINIAODSIM'
    , 'ZJets_HT_600to800_mc2017UL': '/ZJetsToQQ_HT-600to800_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'WZ_mc2017UL': '/WZ_TuneCP5_13TeV-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v1/MINIAODSIM'
    , 'TTJets_mc2017UL': '/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'WJetsToLNu_mc2017UL': '/WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'sWJetsToLNu_mc2017UL': '/WJetsToLNu_012JetsNLO_34JetsLO_EWNLOcorr_13TeV-sherpa/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v4/MINIAODSIM'
    , 'QCD_BGen_HT_700to1000_mc2017UL': '/QCD_HT700to1000_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'QCD_BGen_HT_1000to1500_mc2017UL': '/QCD_HT1000to1500_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'QCD_BGen_HT_1500to2000_mc2017UL': '/QCD_HT1500to2000_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'ZJets_HT_800toInf_F17mc': '/ZJetsToQQ_HT-800toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIFall17MiniAODv2-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/MINIAODSIM'
    , 'ZJets_HT_800toInf_up2018UL': '/ZJetsToQQ_HT-800toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'ZHToAATo4B_mc2017UL_m12': '/SUSY_ZH_ZToAll_HToAATo4B_M-12_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'ZHToAATo4B_mc2017UL_m15': '/SUSY_ZH_ZToAll_HToAATo4B_M-15_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'ZHToAATo4B_mc2017UL_m20': '/SUSY_ZH_ZToAll_HToAATo4B_M-20_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'ZHToAATo4B_mc2017UL_m25': '/SUSY_ZH_ZToAll_HToAATo4B_M-25_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'ZHToAATo4B_mc2017UL_m30': '/SUSY_ZH_ZToAll_HToAATo4B_M-30_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'ZHToAATo4B_mc2017UL_m35': '/SUSY_ZH_ZToAll_HToAATo4B_M-35_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'ZHToAATo4B_mc2017UL_m40': '/SUSY_ZH_ZToAll_HToAATo4B_M-40_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'

    , 'WZToLNu4B-1j-g1': '/privateMCProductionLHEGEN/acrobert-eventMiniAODv2_WZToLNu4B-1j-g1_NFiles-500_2023081312081691921535-dd00e8e5190104a7aafdc4fba9805483/USER'
    , 'ZZTo2L4B-1j-g2': '/privateMCProductionLHEGEN/acrobert-eventMiniAODv2_ZZTo2L4B-1j-g2_NFiles-500_2023081312081691921680-dd00e8e5190104a7aafdc4fba9805483/USER'
    , 'ZGammaTo4B-1j-g1': '/privateMCProductionLHEGEN/acrobert-eventMiniAODv2_ZGammaTo4B-1j-g1_NFiles-500_2023081420081692037191-dd00e8e5190104a7aafdc4fba9805483/USER'

    , 'ZZTo2L4B-1j-g3': '/privateMCProductionLHEGEN/acrobert-eventMiniAODv2_ZZTo2L4B-1j-g3_NFiles-197_2023082422081692910234-dd00e8e5190104a7aafdc4fba9805483/USER'
    , 'ZZTo2L2B-NLO-1j-gX': '/privateMCProductionLHEGEN/acrobert-eventMiniAODv2_ZZTo2L2B-NLO-1j-gX_NFiles-209_2023082703081693099015-dd00e8e5190104a7aafdc4fba9805483/USER'
    , 'ZZTo2L2B-NLO-1j-g0': '/privateMCProductionLHEGEN/acrobert-eventMiniAODv2_ZZTo2L2B-NLO-1j-g0_656779evts_2023110202111698887007-9e9deecbd9d681dc7b5d57ef1a0422d7/USER'
    , 'WZToLNu2B-NLO-1j-g0': '/privateMCProductionLHEGEN/acrobert-eventMiniAODv2_WZToLNu2B-NLO-1j-g0_640328evts_2023110200111698879747-9e9deecbd9d681dc7b5d57ef1a0422d7/USER'
    , 'ZGammaTo2B-NLO-1j-g0': '/privateMCProductionLHEGEN/acrobert-eventMiniAODv2_ZGammaTo2B-NLO-1j-g0_661004evts_2023110200111698879858-9e9deecbd9d681dc7b5d57ef1a0422d7/USER'
    , 'ZJetsTo2B-HT800-4j-g0': '/privateMCProductionLHEGEN/acrobert-eventMiniAODv2_ZJetsTo2B-HT800-4j-g0_863026evts_2023111115111699712516-9e9deecbd9d681dc7b5d57ef1a0422d7/USER'
    , 'ZJetsTo2B-HT6to8-4j-g0': '/privateMCProductionLHEGEN/acrobert-eventMiniAODv2_ZJetsTo2B-HT6to8-4j-g0_861740evts_2024020115021706798148-9e9deecbd9d681dc7b5d57ef1a0422d7/USER'
    , 'ZJetsTo2B-HT4to6-4j-g0': '/privateMCProductionLHEGEN/acrobert-eventMiniAODv2_ZJetsTo2B-HT4to6-4j-g0_863366evts_2024020115021706798266-9e9deecbd9d681dc7b5d57ef1a0422d7/USER'
    , 'ZZTo2Nu2B-NLO-1j-g0': '/privateMCProductionLHEGEN/acrobert-eventMiniAODv2_ZZTo2Nu2B-NLO-1j-g0_860039evts_2023111521111700081870-9e9deecbd9d681dc7b5d57ef1a0422d7/USER'
    
    , 'ZZTo2Nu2Q_up20UL18': '/ZZTo2Nu2Q_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v3/MINIAODSIM'
    , 'ZZTo2Q2L_up20UL18': '/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'WZToLNu2Q_up20UL18': '/WZTo1L1Nu2Q_4f_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'TTJets_up20UL18': '/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'WJetsToLNu_up20UL18': '/WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'ZJets_HT800_up20UL18': '/ZJetsToQQ_HT-800toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'WJets_HT800_up20UL18': '/WJetsToQQ_HT-800toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'QCD_HT2000_up20UL18': '/QCD_HT2000toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'QCD_HT15to20_up20UL18': '/QCD_HT1500to2000_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'QCD_HT10to15_up20UL18': '/QCD_HT1000to1500_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'QCD_HT7to10_up20UL18': '/QCD_HT700to1000_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'QCD_HT5to7_up20UL18': '/QCD_HT500to700_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    
    , 'ZHTo2L2B_up20UL18': '/ZH_HToBB_ZToLL_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM'
    , 'ZHTo2Nu2B_up20UL18': '/ZH_HToBB_ZToNuNu_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM'
    , 'data_JetHT_up20UL18_A': '/JetHT/Run2018A-UL2018_MiniAODv2-v1/MINIAOD'
    , 'data_JetHT_up20UL18_A_19Nov12': '/JetHT/Run2018A-12Nov2019_UL2018-v2/MINIAOD'
    , 'data_JetHT_up20UL18_B_19Nov12': '/JetHT/Run2018B-12Nov2019_UL2018-v2/MINIAOD'
    , 'data_JetHT_up20UL18_C_19Nov12': '/JetHT/Run2018C-12Nov2019_UL2018_rsb-v1/MINIAOD'
    , 'data_JetHT_up20UL18_D_19Nov12': '/JetHT/Run2018D-12Nov2019_UL2018-v5/MINIAOD'

    , 'QCD_HT2000_bFlt_up20UL18': '/QCD_HT2000toInf_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'QCD_HT15to20_bFlt_up20UL18': '/QCD_HT1500to2000_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
    , 'QCD_HT10to15_bFlt_up20UL18': '/QCD_HT1000to1500_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'

    , 'QCD_HT2000_bFlt_mc20UL17': '/QCD_HT2000toInf_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'QCD_HT15to20_bFlt_mc20UL17': '/QCD_HT1500to2000_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
    , 'QCD_HT10to15_bFlt_mc20UL17': '/QCD_HT1000to1500_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'

    , 'QCD_HT2000_mc20UL17': '/QCD_HT2000toInf_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v1/MINIAODSIM'
    , 'QCD_HT15to20_mc20UL17': '/QCD_HT1500to2000_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v1/MINIAODSIM'
    , 'QCD_HT10to15_mc20UL17': '/QCD_HT1000to1500_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v1/MINIAODSIM'


}

t3dirs = {'WZToLNuQQ_mc2017UL': 'WZ', 'ZJets_HT_800toInf_mc2017UL': 'ZJets', 'ZGammaToJJ_mc2017UL': 'ZGamma', 'ZZTo2Q2L_mc2017UL': 'ZZ', 'ZZTo4B01j_mc2017UL': 'ZZ', 'ZJets_HT_600to800_mc2017UL': 'ZJets', 'WZ_mc2017UL': 'WZ', 'TTJets_mc2017UL':'TTJets', 'WJetsToLNu_mc2017UL': 'WJets', 'sWJetsToLNu_mc2017UL': 'WJets', 'QCD_BGen_HT_700to1000_mc2017UL': 'QCD', 'QCD_BGen_HT_1000to1500_mc2017UL': 'QCD', 'QCD_BGen_HT_1500to2000_mc2017UL': 'QCD', 'ZJets_HT_800toInf_F17mc': 'ZJets', 'ZJets_HT_800toInf_up2018UL': 'ZJets', 'ZHToAATo4B_mc2017UL_m12': 'ZHToAA', 'ZHToAATo4B_mc2017UL_m15': 'ZHToAA', 'ZHToAATo4B_mc2017UL_m20': 'ZHToAA', 'ZHToAATo4B_mc2017UL_m25': 'ZHToAA', 'ZHToAATo4B_mc2017UL_m30': 'ZHToAA', 'ZHToAATo4B_mc2017UL_m35': 'ZHToAA', 'ZHToAATo4B_mc2017UL_m40': 'ZHToAA', 'WZToLNu4B-1j-g1':'WZToLNu4B-1j-g1', 'ZZTo2L4B-1j-g2': 'ZZTo2L4B-1j-g2', 'ZGammaTo4B-1j-g1': 'ZGammaTo4B-1j-g1', 'ZZTo2L4B-1j-g3': 'ZZTo2L4B-1j-g3', 'ZZTo2L2B-NLO-1j-gX': 'ZZTo2L2B-NLO-1j-gX', 'ZZTo2L2B-NLO-1j-g0': 'ZZTo2L2B-NLO-1j-g0', 'ZGammaTo2B-NLO-1j-g0': 'ZGammaTo2B-NLO-1j-g0', 'WZToLNu2B-NLO-1j-g0': 'WZToLNu2B-NLO-1j-g0', 'ZGammaTo2B-NLO-1j-g0': 'ZGammaTo2B-NLO-1j-g0', 'ZZTo2Nu2B-NLO-1j-g0': 'ZZTo2Nu2B-NLO-1j-g0', 'ZJetsTo2B-HT800-4j-g0': 'ZJetsTo2B-HT800-4j-g0', 'ZJets_HT800_up20UL18': 'ZJets_HT800_up20UL18'}

signalevents = {'ZZTo2L2B-NLO-1j-g0':           437.3588, 
                'ZGammaTo2B-NLO-1j-g0':        5951.828, 
                'WZToLNu2B-NLO-1j-g0':         1402.3594, 
                'ZZTo2Nu2B-NLO-1j-g0':          661.6004, 
                'ZJetsTo2B-HT800-4j-g0':      16108.46,
                'ZJetsTo2B-HT6to8-4j-g0':     29720.78,
                'ZJetsTo2B-HT4to6-4j-g0':    118877.64,

                'ZZTo2Nu2Q_up20UL18':        623021.2,
                'ZZTo2Q2L_up20UL18':         505721.8,
                'WZToLNu2Q_up20UL18':       1256070.8,
                'TTJets_up20UL18':        103380200,
                'WJetsToLNu_up20UL18':   9193796000,
                'ZJets_HT800_up20UL18':     1764834,
                'WJets_HT800_up20UL18':     3928886,

                'QCD_HT2000_up20UL18':      3005232,
                'QCD_HT15to20_up20UL18':   14823400,
                'QCD_HT10to15_up20UL18':  153522200,
                'QCD_HT7to10_up20UL18':   880293500,
                'QCD_HT5to7_up20UL18':   4136989000,
                
                'QCD_HT2000_bFlt_up20UL18': 426892,
                'QCD_HT15to20_bFlt_up20UL18': 1977937.5,
                'QCD_HT10to15_bFlt_up20UL18': 18953950,
                
                'QCD_HT2000_mc20UL17': 3026741,
                'QCD_HT15to20_mc20UL17': 14756270,
                'QCD_HT10to15_mc20UL17': 152878300,
                
                'QCD_HT2000_bFlt_mc20UL17': 427275.6,
                'QCD_HT15to20_bFlt_mc20UL17': 1994994,
                'QCD_HT10to15_bFlt_mc20UL17': 18963540,
                
                'ZHTo2L2B_up20UL18': 10928.49,
                'ZHTo2Nu2B_up20UL18': 21550.1
}

dsetlumi = {'Run2': 137, 'data_JetHT_up20UL18_A': 14.436175}

filtereffs = {'QCD_HT2000_up20UL18': 0.090911,
              'QCD_HT15to20_up20UL18': 0.077131,
              'QCD_HT10to15_up20UL18': 0.059521,

              'QCD_HT2000_bFlt_up20UL18': 0.323338,
              'QCD_HT15to20_bFlt_up20UL18': 0.29208,
              'QCD_HT10to15_bFlt_up20UL18': 0.240255,

              'QCD_HT2000_mc20UL17': 0.097842,
              'QCD_HT15to20_mc20UL17': 0.082626,
              'QCD_HT10to15_mc20UL17': 0.058877,

              'QCD_HT2000_bFlt_mc20UL17': 0.339314,
              'QCD_HT15to20_bFlt_mc20UL17': 0.305851,
              'QCD_HT10to15_bFlt_mc20UL17': 0.235423,

              'TTJets_up20UL18': 0.082283,
              'WJets_HT800_up20UL18': 0.040556,
              'ZJets_HT800_up20UL18': 0.123309,
              
              'ZJetsTo2B-HT800-4j-g0': 1.,
              'ZJetsTo2B-HT6to8-4j-g0': 1.,
              'ZJetsTo2B-HT4to6-4j-g0': 1.,
}

dsetnevts = {'QCD_HT2000_up20UL18': 394892,
             'QCD_HT15to20_up20UL18': 798399,
             'QCD_HT10to15_up20UL18': 771507,

             'QCD_HT2000_bFlt_up20UL18': 381996,
             'QCD_HT15to20_bFlt_up20UL18': 339511,
             'QCD_HT10to15_bFlt_up20UL18': 369380,

             'QCD_HT2000_mc20UL17': 402383,
             'QCD_HT15to20_mc20UL17': 605916,
             'QCD_HT10to15_mc20UL17': 532051,

             'QCD_HT2000_bFlt_mc20UL17': 259816,
             'QCD_HT15to20_bFlt_mc20UL17': 349556,
             'QCD_HT10to15_bFlt_mc20UL17': 249664,

             'TTJets_up20UL18OA': 1814827,
             'WJets_HT800_up20UL18': 475703,
	     'ZJets_HT800_up20UL18': 813970,

             'ZJetsTo2B-HT800-4j-g0': 530324,
             'ZJetsTo2B-HT6to8-4j-g0': 409564,
             'ZJetsTo2B-HT4to6-4j-g0': 595497,
}

HLTdict = {'2018': {'1L': ['IsoMu24','Mu50','Ele27_WPTight_Gsf']
                    , '2L': ['DoubleEle33_CaloIdL_MW','DiEle27_WPTightCaloOnly_L1DoubleEG','Ele23_Ele12_CaloIdL_TrackIdL_IsoVL','DoubleL2Mu50','Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8','Mu37_TkMu27']
                    , 'MET': ['DiJet110_35_Mjj650_PFMET110','MET105_IsoTrk50','PFMET110_PFMHT110_IDTight_CaloBTagDeepCSV_3p1','PFMET250_HBHECleaned','TripleJet110_35_35_Mjj650_PFMET110']
                    , 'Jet': ['AK8PFJet400_TrimMass30','AK8PFJet500','AK8PFJetFwd400','PFJet500']
                    , 'HT': ['PFHT1050']
                    , 'BTag': ['PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94','PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepCSV_4p5',
                               'QuadPFJet103_88_75_15_DoublePFBTagDeepCSV_1p3_7p7_VBF1','DoublePFJets116MaxDeta1p6_DoubleCaloBTagDeepCSV_p71']
                    , 'Photon': ["Photon200", "Photon60_R9Id90_CaloIdL_IsoL_DisplacedIdL_PFHT350MinPFJet15", 'Photon110EB_TightID_TightIso']
                }
       }

def passHLT(nano, key):

    passvec = None
    if key == 'Any':
        for key in HLTdict['2018'].keys():
            for trigger in HLTdict['2018'][key]:
                if passvec is None:
                    passvec = nano.HLT[trigger]
                else:
                    passvec = passvec | nano.HLT[trigger]
    elif type(key) == str:
        for trigger in HLTdict['2018'][key]:
            if passvec is None:
                passvec = nano.HLT[trigger]
            else:
                passvec = passvec | nano.HLT[trigger]
    elif type(key) == list:
        for trigger in key:
            if passvec is None:
                passvec = nano.HLT[trigger]
            else:
                passvec = passvec | nano.HLT[trigger]

    if passvec is not None:
        return passvec

anaHLT = {'ZZTo2L2B-NLO-1j-g0': ['Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8', 'Ele23_Ele12_CaloIdL_TrackIdL_IsoVL'],
          #'ZZTo2L2B-NLO-1j-g0': ['Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8', 'DoubleEle25_CaloIdL_MW', 'Ele23_Ele12_CaloIdL_TrackIdL_IsoVL', 'Ele28_WPTight_Gsf', 'IsoMu24'],
          #'ZJetsTo2B-HT800-4j-g0': ['AK8PFJet330_TrimMass30_PFAK8BoostedDoubleB_np4', 'PFHT1050'],
          'ZJetsTo2B-HT800-4j-g0': ['PFJet500', 'PFHT1050'],
          'WZToLNu2B-NLO-1j-g0': ['Ele30_WPTight_Gsf', 'IsoMu24'],
          #'WZToLNu2B-NLO-1j-g0': ['Ele30_WPTight_Gsf', 'L2Mu23NoVtx_2Cha', 'IsoMu24'],
          'ZZTo2Nu2B-NLO-1j-g0': ['PFMET120_PFMHT120_IDTight_PFHT60'],
          'ZGammaTo2B-NLO-1j-g0': ['Photon60_R9Id90_CaloIdL_IsoL_DisplacedIdL_PFHT350MinPFJet15', 'Photon110EB_TightID_TightIso']}


all2018HLT = ['BTagMu_AK4Jet300_Mu5_noalgo'
              ,'BTagMu_AK4Jet300_Mu5'
              ,'BTagMu_AK8Jet170_DoubleMu5_noalgo'
              ,'BTagMu_AK8Jet170_DoubleMu5'
              ,'BTagMu_AK8Jet300_Mu5_noalgo'
              ,'BTagMu_AK8Jet300_Mu5'
              ,'Dimuon0_Jpsi3p5_Muon2'
              ,'Dimuon18_PsiPrime_noCorrL1'
              ,'Dimuon18_PsiPrime'
              ,'Dimuon25_Jpsi_noCorrL1'
              ,'Dimuon25_Jpsi'
              ,'DoubleMu2_Jpsi_DoubleTkMu0_Phi'
              ,'DoubleMu2_Jpsi_DoubleTrk1_Phi1p05'
              ,'DoubleMu4_3_Bs'
              ,'DoubleMu4_JpsiTrkTrk_Displaced'
              ,'DoubleMu4_JpsiTrk_Displaced'
              ,'DoubleMu4_PsiPrimeTrk_Displaced'
              ,'Mu30_TkMu0_Psi'
              ,'IsoTrackHB'
              ,'IsoTrackHE'
              ,'HT430_DisplacedDijet40_DisplacedTrack'
              ,'HT430_DisplacedDijet60_DisplacedTrack'
              ,'HT500_DisplacedDijet40_DisplacedTrack'
              ,'HT650_DisplacedDijet60_Inclusive'
              ,'DoubleL2Mu23NoVtx_2Cha_CosmicSeed_NoL2Matched'
              ,'DoubleL2Mu23NoVtx_2Cha_CosmicSeed'
              ,'DoubleL2Mu23NoVtx_2Cha_NoL2Matched'
              ,'DoubleL2Mu23NoVtx_2Cha'
              ,'DoubleL2Mu25NoVtx_2Cha_CosmicSeed_Eta2p4'
              ,'DoubleL2Mu25NoVtx_2Cha_CosmicSeed_NoL2Matched'
              ,'DoubleL2Mu25NoVtx_2Cha_CosmicSeed'
              ,'DoubleL2Mu25NoVtx_2Cha_Eta2p4'
              ,'DoubleL2Mu25NoVtx_2Cha_NoL2Matched'
              ,'DoubleL2Mu25NoVtx_2Cha'
              ,'DoubleL2Mu30NoVtx_2Cha_CosmicSeed_Eta2p4'
              ,'DoubleL2Mu30NoVtx_2Cha_Eta2p4'
              ,'DoubleL2Mu50'
              ,'DoubleMu33NoFiltersNoVtxDisplaced'
              ,'DoubleMu3_DCA_PFMET50_PFMHT60'
              ,'DoubleMu3_DZ_PFMET50_PFMHT60'
              ,'DoubleMu3_DZ_PFMET70_PFMHT70'
              ,'DoubleMu3_DZ_PFMET90_PFMHT90'
              ,'DoubleMu40NoFiltersNoVtxDisplaced'
              ,'DoubleMu43NoFiltersNoVtx'
              ,'DoubleMu48NoFiltersNoVtx'
              ,'DoubleMu4_Mass3p8_DZ_PFHT350'
              ,'Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8'
              ,'Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8'
              ,'Mu18_Mu9_SameSign_DZ'
              ,'Mu18_Mu9_SameSign'
              ,'Mu19_TrkIsoVVL_Mu9_TrkIsoVVL_DZ_Mass3p8'
              ,'Mu19_TrkIsoVVL_Mu9_TrkIsoVVL_DZ_Mass8'
              ,'Mu20_Mu10_SameSign_DZ'
              ,'Mu20_Mu10_SameSign'
              ,'Mu23_Mu12_SameSign_DZ'
              ,'Mu23_Mu12_SameSign'
              ,'Mu37_TkMu27'
              ,'TripleMu_10_5_5_DZ'
              ,'TripleMu_12_10_5'
              ,'TripleMu_5_3_3_Mass3p8_DCA'
              ,'TripleMu_5_3_3_Mass3p8_DZ'
              ,'TrkMu12_DoubleTrkMu5NoFiltersNoVtx'
              ,'TrkMu16_DoubleTrkMu6NoFiltersNoVtx'
              ,'TrkMu17_DoubleTrkMu8NoFiltersNoVtx'
              ,'DoubleMu3_TkMu_DsTau3Mu'
              ,'DoubleMu4_LowMassNonResonantTrk_Displaced'
              ,'Tau3Mu_Mu7_Mu1_TkMu1_IsoTau15_Charge1'
              ,'Tau3Mu_Mu7_Mu1_TkMu1_IsoTau15'
              ,'DiEle27_WPTightCaloOnly_L1DoubleEG'
              ,'Diphoton30PV_18PV_R9Id_AND_IsoCaloId_AND_HE_R9Id_PixelVeto_Mass55'
              ,'Diphoton30_18_R9IdL_AND_HE_AND_IsoCaloId_NoPixelVeto_Mass55'
              ,'Diphoton30_18_R9IdL_AND_HE_AND_IsoCaloId_NoPixelVeto'
              ,'Diphoton30_22_R9Id_OR_IsoCaloId_AND_HE_R9Id_Mass90'
              ,'Diphoton30_22_R9Id_OR_IsoCaloId_AND_HE_R9Id_Mass95'
              ,'DoubleEle25_CaloIdL_MW'
              ,'DoubleEle27_CaloIdL_MW'
              ,'DoubleEle33_CaloIdL_MW'
              ,'DoubleEle8_CaloIdM_TrackIdM_Mass8_DZ_PFHT350'
              ,'DoubleEle8_CaloIdM_TrackIdM_Mass8_PFHT350'
              ,'DoublePhoton70'
              ,'DoublePhoton85'
              ,'ECALHT800'
              ,'Ele115_CaloIdVT_GsfTrkIdT'
              ,'Ele135_CaloIdVT_GsfTrkIdT'
              ,'Ele145_CaloIdVT_GsfTrkIdT'
              ,'Ele200_CaloIdVT_GsfTrkIdT'
              ,'Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ'
              ,'Ele23_Ele12_CaloIdL_TrackIdL_IsoVL'
              ,'Ele24_eta2p1_WPTight_Gsf_LooseChargedIsoPFTauHPS30_eta2p1_CrossL1'
              ,'Ele24_eta2p1_WPTight_Gsf_LooseChargedIsoPFTauHPS30_eta2p1_TightID_CrossL1'
              ,'Ele24_eta2p1_WPTight_Gsf_MediumChargedIsoPFTauHPS30_eta2p1_CrossL1'
              ,'Ele24_eta2p1_WPTight_Gsf_MediumChargedIsoPFTauHPS30_eta2p1_TightID_CrossL1'
              ,'Ele24_eta2p1_WPTight_Gsf_TightChargedIsoPFTauHPS30_eta2p1_CrossL1'
              ,'Ele24_eta2p1_WPTight_Gsf_TightChargedIsoPFTauHPS30_eta2p1_TightID_CrossL1'
              ,'Ele250_CaloIdVT_GsfTrkIdT'
              ,'Ele27_Ele37_CaloIdL_MW'
              ,'Ele28_HighEta_SC20_Mass55'
              ,'Ele28_WPTight_Gsf'
              ,'Ele28_eta2p1_WPTight_Gsf_HT150'
              ,'Ele300_CaloIdVT_GsfTrkIdT'
              ,'Ele30_WPTight_Gsf'
              ,'Ele30_eta2p1_WPTight_Gsf_CentralPFJet35_EleCleaned'
              ,'Ele32_WPTight_Gsf_L1DoubleEG'
              ,'Ele32_WPTight_Gsf'
              ,'Ele35_WPTight_Gsf_L1EGMT'
              ,'Ele35_WPTight_Gsf'
              ,'Ele38_WPTight_Gsf'
              ,'Ele40_WPTight_Gsf'
              ,'Ele50_CaloIdVT_GsfTrkIdT_PFJet165'
              ,'Photon110EB_TightID_TightIso'
              ,'Photon120EB_TightID_TightIso'
              ,'Photon200'
              ,'Photon300_NoHE'
              ,'Photon50_R9Id90_HE10_IsoM_EBOnly_PFJetsMJJ300DEta3_PFMET50'
              ,'Photon60_R9Id90_CaloIdL_IsoL_DisplacedIdL_PFHT350MinPFJet15'
              ,'Photon75_R9Id90_HE10_IsoM_EBOnly_CaloMJJ300_PFJetsMJJ400DEta3'
              ,'Photon75_R9Id90_HE10_IsoM_EBOnly_CaloMJJ400_PFJetsMJJ600DEta3'
              ,'Photon75_R9Id90_HE10_IsoM_EBOnly_PFJetsMJJ300DEta3'
              ,'Photon75_R9Id90_HE10_IsoM_EBOnly_PFJetsMJJ600DEta3'
              ,'TriplePhoton_20_20_20_CaloIdLV2_R9IdVL'
              ,'TriplePhoton_20_20_20_CaloIdLV2'
              ,'TriplePhoton_30_30_10_CaloIdLV2_R9IdVL'
              ,'TriplePhoton_30_30_10_CaloIdLV2'
              ,'TriplePhoton_35_35_5_CaloIdLV2_R9IdVL'
              ,'AK8PFHT800_TrimMass50'
              ,'AK8PFHT850_TrimMass50'
              ,'AK8PFHT900_TrimMass50'
              ,'AK8PFJet330_TrimMass30_PFAK8BTagDeepCSV_p17'
              ,'AK8PFJet330_TrimMass30_PFAK8BTagDeepCSV_p1'
              ,'AK8PFJet330_TrimMass30_PFAK8BoostedDoubleB_np2'
              ,'AK8PFJet330_TrimMass30_PFAK8BoostedDoubleB_np4'
              ,'AK8PFJet400_TrimMass30'
              ,'AK8PFJet420_TrimMass30'
              ,'AK8PFJet500'
              ,'AK8PFJet550'
              ,'AK8PFJetFwd400'
              ,'AK8PFJetFwd450'
              ,'AK8PFJetFwd500'
              ,'CaloJet500_NoJetID'
              ,'CaloJet550_NoJetID'
              ,'DiPFJetAve300_HFJEC'
              ,'DoublePFJets116MaxDeta1p6_DoubleCaloBTagDeepCSV_p71'
              ,'DoublePFJets128MaxDeta1p6_DoubleCaloBTagDeepCSV_p71'
              ,'Mu12_DoublePFJets40MaxDeta1p6_DoubleCaloBTagDeepCSV_p71'
              ,'Mu12_DoublePFJets54MaxDeta1p6_DoubleCaloBTagDeepCSV_p71'
              ,'Mu12_DoublePFJets62MaxDeta1p6_DoubleCaloBTagDeepCSV_p71'
              ,'PFHT1050'
              ,'PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepCSV_4p5'
              ,'PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2'
              ,'PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepCSV_4p5'
              ,'PFHT400_FivePFJet_120_120_60_30_30_DoublePFBTagDeepCSV_4p5'
              ,'PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94'
              ,'PFHT430_SixPFJet40_PFBTagDeepCSV_1p5'
              ,'PFHT450_SixPFJet36_PFBTagDeepCSV_1p59'
              ,'PFHT500_PFMET100_PFMHT100_IDTight'
              ,'PFHT500_PFMET110_PFMHT110_IDTight'
              ,'PFHT700_PFMET85_PFMHT85_IDTight'
              ,'PFHT700_PFMET95_PFMHT95_IDTight'
              ,'PFHT800_PFMET75_PFMHT75_IDTight'
              ,'PFHT800_PFMET85_PFMHT85_IDTight'
              ,'PFJet500'
              ,'PFJet550'
              ,'PFJetFwd400'
              ,'PFJetFwd450'
              ,'PFJetFwd500'
              ,'QuadPFJet103_88_75_15_DoublePFBTagDeepCSV_1p3_7p7BF1'
              ,'QuadPFJet103_88_75_15_PFBTagDeepCSV_1p3BF2'
              ,'QuadPFJet105_88_76_15_DoublePFBTagDeepCSV_1p3_7p7BF1'
              ,'QuadPFJet105_88_76_15_PFBTagDeepCSV_1p3BF2'
              ,'QuadPFJet105_90_76_15_DoublePFBTagDeepCSV_1p3_7p7_VBF1'
              ,'QuadPFJet111_90_80_15_DoublePFBTagDeepCSV_1p3_7p7BF1'
              ,'QuadPFJet111_90_80_15_PFBTagDeepCSV_1p3BF2'
              ,'Rsq0p35'
              ,'Rsq0p40'
              ,'RsqMR300_Rsq0p09_MR200_4jet'
              ,'RsqMR300_Rsq0p09_MR200'
              ,'RsqMR320_Rsq0p09_MR200_4jet'
              ,'RsqMR320_Rsq0p09_MR200'
              ,'CaloMET350_HBHECleaned'
              ,'DiJet110_35_Mjj650_PFMET110'
              ,'DiJet110_35_Mjj650_PFMET120'
              ,'DiJet110_35_Mjj650_PFMET130'
              ,'MET105_IsoTrk50'
              ,'MET120_IsoTrk50'
              ,'MonoCentralPFJet80_PFMETNoMu120_PFMHTNoMu120_IDTight'
              ,'MonoCentralPFJet80_PFMETNoMu130_PFMHTNoMu130_IDTight'
              ,'MonoCentralPFJet80_PFMETNoMu140_PFMHTNoMu140_IDTight'
              ,'PFMET110_PFMHT110_IDTight_CaloBTagDeepCSV_3p1'
              ,'PFMET120_PFMHT120_IDTight_CaloBTagDeepCSV_3p1'
              ,'PFMET120_PFMHT120_IDTight_PFHT60'
              ,'PFMET120_PFMHT120_IDTight'
              ,'PFMET130_PFMHT130_IDTight_CaloBTagDeepCSV_3p1'
              ,'PFMET130_PFMHT130_IDTight'
              ,'PFMET140_PFMHT140_IDTight_CaloBTagDeepCSV_3p1'
              ,'PFMET140_PFMHT140_IDTight'
              ,'PFMET200_HBHE_BeamHaloCleaned'
              ,'PFMET250_HBHECleaned'
              ,'PFMET300_HBHECleaned'
              ,'PFMETNoMu120_PFMHTNoMu120_IDTight_PFHT60'
              ,'PFMETNoMu120_PFMHTNoMu120_IDTight'
              ,'PFMETNoMu130_PFMHTNoMu130_IDTight'
              ,'PFMETNoMu140_PFMHTNoMu140_IDTight'
              ,'PFMETTypeOne140_PFMHT140_IDTight'
              ,'PFMETTypeOne200_HBHE_BeamHaloCleaned'
              ,'TripleJet110_35_35_Mjj650_PFMET110'
              ,'TripleJet110_35_35_Mjj650_PFMET120'
              ,'TripleJet110_35_35_Mjj650_PFMET130'
              ,'Dimuon12_Upsilon_y1p4'
              ,'Dimuon14_Phi_Barrel_Seagulls'
              ,'Dimuon24_Phi_noCorrL1'
              ,'Dimuon24_Upsilon_noCorrL1'
              ,'DoubleMu3_DoubleEle7p5_CaloIdL_TrackIdL_Upsilon'
              ,'DoubleMu5_Upsilon_DoubleEle3_CaloIdL_TrackIdL'
              ,'Mu25_TkMu0_Phi'
              ,'Mu30_TkMu0_Onia'
              ,'Mu30_TkMu0_Upsilon'
              ,'Trimuon5_3p5_2_Upsilon_Muon'
              ,'TrimuonOpen_5_3p5_2_Upsilon_Muon'
              ,'DiMu4_Ele9_CaloIdL_TrackIdL_DZ_Mass3p8'
              ,'DiMu9_Ele9_CaloIdL_TrackIdL_DZ'
              ,'DoubleMu20_7_Mass0to30_Photon23'
              ,'Mu12_DoublePhoton20'
              ,'Mu12_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ'
              ,'Mu17_Photon30_IsoCaloId'
              ,'Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ'
              ,'Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL'
              ,'Mu27_Ele37_CaloIdL_MW'
              ,'Mu37_Ele27_CaloIdL_MW'
              ,'Mu38NoFiltersNoVtxDisplaced_Photon38_CaloIdL'
              ,'Mu43NoFiltersNoVtxDisplaced_Photon43_CaloIdL'
              ,'Mu43NoFiltersNoVtx_Photon43_CaloIdL'
              ,'Mu48NoFiltersNoVtx_Photon48_CaloIdL'
              ,'Mu8_DiEle12_CaloIdL_TrackIdL_DZ'
              ,'Mu8_DiEle12_CaloIdL_TrackIdL'
              ,'Mu8_Ele8_CaloIdM_TrackIdM_Mass8_PFHT350_DZ'
              ,'Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ_CaloDiJet30_CaloBtagDeepCSV_1p5'
              ,'Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ_CaloDiJet30'
              ,'Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ_PFDiJet30_PFBtagDeepCSV_1p5'
              ,'Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ_PFDiJet30'
              ,'Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ'
              ,'L2Mu40_NoVertex_3Sta_NoBPTX3BX'
              ,'L2Mu45_NoVertex_3Sta_NoBPTX3BX'
              ,'UncorrectedJetE60_NoBPTX3BX'
              ,'UncorrectedJetE70_NoBPTX3BX'
              ,'DoubleEle24_eta2p1_WPTight_Gsf'
              ,'DoubleIsoMu20_eta2p1'
              ,'IsoMu24_eta2p1_MediumChargedIsoPFTau35_Trk1_TightID_eta2p1_Reg_CrossL1'
              ,'IsoMu24_eta2p1_MediumChargedIsoPFTau35_Trk1_eta2p1_Reg_CrossL1'
              ,'IsoMu24_eta2p1_MediumChargedIsoPFTau50_Trk30_eta2p1_1pr'
              ,'IsoMu24_eta2p1_MediumChargedIsoPFTauHPS30_Trk1_eta2p1_Reg_CrossL1'
              ,'IsoMu24_eta2p1_MediumChargedIsoPFTauHPS35_Trk1_TightID_eta2p1_Reg_CrossL1'
              ,'IsoMu24_eta2p1_MediumChargedIsoPFTauHPS35_Trk1_eta2p1_Reg_CrossL1'
              ,'IsoMu24_eta2p1_TightChargedIsoPFTau35_Trk1_TightID_eta2p1_Reg_CrossL1'
              ,'IsoMu24_eta2p1_TightChargedIsoPFTau35_Trk1_eta2p1_Reg_CrossL1'
              ,'IsoMu24_eta2p1_TightChargedIsoPFTauHPS35_Trk1_TightID_eta2p1_Reg_CrossL1'
              ,'IsoMu24_eta2p1_TightChargedIsoPFTauHPS35_Trk1_eta2p1_Reg_CrossL1'
              ,'IsoMu27_LooseChargedIsoPFTau20_Trk1_eta2p1_SingleL1'
              ,'IsoMu27_LooseChargedIsoPFTauHPS20_Trk1_eta2p1_SingleL1'
              ,'IsoMu27_MET90'
              ,'IsoMu27_MediumChargedIsoPFTau20_Trk1_eta2p1_SingleL1'
              ,'IsoMu27_MediumChargedIsoPFTauHPS20_Trk1_eta2p1_SingleL1'
              ,'IsoMu27_TightChargedIsoPFTau20_Trk1_eta2p1_SingleL1'
              ,'IsoMu27_TightChargedIsoPFTauHPS20_Trk1_eta2p1_SingleL1'
              ,'L2Mu23NoVtx_2Cha_CosmicSeed'
              ,'L2Mu23NoVtx_2Cha'
              ,'IsoMu20_eta2p1_LooseChargedIsoPFTau27_eta2p1_CrossL1'
              ,'IsoMu20_eta2p1_LooseChargedIsoPFTau27_eta2p1_TightID_CrossL1'
              ,'IsoMu20_eta2p1_LooseChargedIsoPFTauHPS27_eta2p1_CrossL1'
              ,'IsoMu20_eta2p1_LooseChargedIsoPFTauHPS27_eta2p1_TightID_CrossL1'
              ,'IsoMu20_eta2p1_MediumChargedIsoPFTau27_eta2p1_CrossL1'
              ,'IsoMu20_eta2p1_MediumChargedIsoPFTau27_eta2p1_TightID_CrossL1'
              ,'IsoMu20_eta2p1_MediumChargedIsoPFTauHPS27_eta2p1_CrossL1'
              ,'IsoMu20_eta2p1_MediumChargedIsoPFTauHPS27_eta2p1_TightID_CrossL1'
              ,'IsoMu20_eta2p1_TightChargedIsoPFTau27_eta2p1_CrossL1'
              ,'IsoMu20_eta2p1_TightChargedIsoPFTau27_eta2p1_TightID_CrossL1'
              ,'IsoMu20_eta2p1_TightChargedIsoPFTauHPS27_eta2p1_CrossL1'
              ,'IsoMu20_eta2p1_TightChargedIsoPFTauHPS27_eta2p1_TightID_CrossL1'
              ,'IsoMu24_TwoProngs35'
              ,'IsoMu24_eta2p1'
              ,'IsoMu24'
              ,'IsoMu27'
              ,'IsoMu30'
              ,'Mu50'
              ,'Mu55'
              ,'OldMu100'
              ,'TkMu100'
              ,'DoubleMediumChargedIsoPFTau40_Trk1_TightID_eta2p1_Reg'
              ,'DoubleMediumChargedIsoPFTauHPS35_Trk1_TightID_eta2p1_Reg'
              ,'DoubleMediumChargedIsoPFTauHPS35_Trk1_eta2p1_Reg'
              ,'DoubleMediumChargedIsoPFTauHPS40_Trk1_TightID_eta2p1_Reg'
              ,'DoubleMediumChargedIsoPFTauHPS40_Trk1_eta2p1_Reg'
              ,'DoubleTightChargedIsoPFTau35_Trk1_TightID_eta2p1_Reg'
              ,'DoubleTightChargedIsoPFTau40_Trk1_eta2p1_Reg'
              ,'DoubleTightChargedIsoPFTauHPS35_Trk1_TightID_eta2p1_Reg'
              ,'DoubleTightChargedIsoPFTauHPS35_Trk1_eta2p1_Reg'
              ,'DoubleTightChargedIsoPFTauHPS40_Trk1_TightID_eta2p1_Reg'
              ,'DoubleTightChargedIsoPFTauHPS40_Trk1_eta2p1_Reg'
              ,'MediumChargedIsoPFTau180HighPtRelaxedIso_Trk50_eta2p1_1pr'
              ,'MediumChargedIsoPFTau180HighPtRelaxedIso_Trk50_eta2p1'
              ,'MediumChargedIsoPFTau200HighPtRelaxedIso_Trk50_eta2p1'
              ,'MediumChargedIsoPFTau220HighPtRelaxedIso_Trk50_eta2p1'
              ,'MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET100'
              ,'MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET110'
              ,'MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET120'
              ,'MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET130'
              ,'MediumChargedIsoPFTau50_Trk30_eta2p1_1pr_MET140'
              ,'Photon35_TwoProngs35'
              ,'VBF_DoubleLooseChargedIsoPFTauHPS20_Trk1_eta2p1'
              ,'VBF_DoubleMediumChargedIsoPFTau20_Trk1_eta2p1'
              ,'VBF_DoubleMediumChargedIsoPFTauHPS20_Trk1_eta2p1'
              ,'VBF_DoubleTightChargedIsoPFTau20_Trk1_eta2p1'
              ,'VBF_DoubleTightChargedIsoPFTauHPS20_Trk1_eta2p1']


def xglob_onedir(path):

    t3 = 'root://cmsdata.phys.cmu.edu/'

    cmd = 'xrdfs {} ls {}'.format(t3, path)
    os.system(cmd +' > tmpf')
    out = open('tmpf', 'r').read().split('\n')

    out.remove('')
    return out
