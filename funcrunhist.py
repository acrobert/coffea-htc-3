import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
import matplotlib.pyplot as plt
import mplhep
from argparse import ArgumentParser
from classRunCoffea import RunCoffea
import os, psutil
import tracemalloc
import gc
from copy import deepcopy
import matplotlib.ticker as ticker
from time import sleep
from htcutils import *
import pickle as pkl
import sys
import math

#plt.rcParams['text.usetex'] = True
#site = 'cmsdata.phys.cmu.edu'
site = 'cmseos.fnal.gov'

#keys = [['TTJets_up20UL18'], ['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['QCD_HT10to15_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['WZToLNu2B-NLO-1j-g0', 'ZZTo2L2B-NLO-1j-g0']]
#keys = [['TTJets_up20UL18'], ['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['QCD_HT10to15_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['WZToLNu2B-NLO-1j-g0', 'ZZTo2Nu2B-NLO-1j-g0']]

#types = ['VJets', 'VZ to 4B']
#keys = [['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['ZZTo2L2B-NLO-1j-g0', 'WZToLNu2B-NLO-1j-g0']]

class histsrunner:

    def __init__(self, fillhist, selection, btagthresh, addtag = None, style = None, multi=False, rundata=False):
        
        if not rundata:
            self.types = ['TTJets', 'VJets', 'QCD', 'ZJets to 4B']
            self.keys = [['TTJets_up20UL18'], ['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['QCD_HT10to15_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['ZJetsTo2B-HT6to8-4j-g0', 'ZJetsTo2B-HT800-4j-g0']]
            #self.keys = [['TTJets_up20UL18'], ['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['QCD_HT10to15_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['ZJetsTo2B-HT800-4j-g0']]
            #self.types = ['TTJets', 'ZJets to 4B']
            #self.keys = [['TTJets_up20UL18'], ['ZJetsTo2B-HT6to8-4j-g0', 'ZJetsTo2B-HT800-4j-g0']]
        else:
            self.types = ['Data', 'ZJets to 4B']
            self.keys = [['data_JetHT_up20UL18_A'], ['ZJetsTo2B-HT6to8-4j-g0', 'ZJetsTo2B-HT800-4j-g0']]
         
        #self.types = ['ZJets to 4B']
        #self.keys = [['ZJetsTo2B-HT6to8-4j-g0', 'ZJetsTo2B-HT800-4j-g0']]

        self.data = ['data_JetHT_up20UL18_A']

        self.hltkey = self.keys[-1][-1]

        self.nfiles = 0
        self.filesperhist = 20

        self.tmr = timer()

        self.fillhist = fillhist
        self.selection = selection
        self.btagthresh = btagthresh
        
        self.tag = f'{selection}_btag{btagthresh}_{fillhist}' + (f'_{addtag}' if (addtag is not None and addtag != '') else '')
        
        if style is not None:
            self.style = style

        self.multi = multi
        self.rundata = rundata
        if self.rundata:
            assert self.multi

        if self.multi:
            self.nhists = len(selection.split(','))

    def spawnCondor(self, outname, infiles, xmin, xmax, nbins, submit=False):
        logname = outname
        execsh = 'makeHist.sh' if not self.multi else 'makeHist_multi.sh'
        execpy = 'exechtc.py' if not self.multi else 'exechtc_multi.py'
        subcondor_template = """
Executable            = {}
Arguments             = {} {} {} {} {} {} {} {} {}
Should_transfer_files = YES
transfer_output_files = {}
x509userproxy         = /uscms/home/acrobert/x509up_u57074
Use_x509userproxy     = true
Output                = /uscms/home/acrobert/nobackup/zto4b/htc/logs/{}.out
Error                 = /uscms/home/acrobert/nobackup/zto4b/htc/logs/{}.err
Log                   = /uscms/home/acrobert/nobackup/zto4b/htc/logs/{}.log
Queue
""".format(execsh, outname, self.hltkey, self.selection, self.fillhist, self.btagthresh, infiles, ','.join([str(x) for x in xmin]), ','.join([str(x) for x in xmax]), nbins, execpy, logname, logname, logname)

        with open('subcondorHist','a') as sub:
            sub.write(subcondor_template)

        if submit:
            os.system('condor_submit subcondorHist')

    def addtocondor(self, key, xmin, xmax, nbins):
        
        print(f'Running key {key}; elapsed time {round(self.tmr.time()/60., 3)}')
        filepairs = RunCoffea.initnanos(key, date='Apr2024')
                
        nthreads = len(filepairs) // self.filesperhist + 1
        self.nfiles += nthreads
        for j in range(nthreads):
            s = '='.join([str(a)+'+'+str(b) for a, b in filepairs[j*self.filesperhist : (j+1)*self.filesperhist]])
            self.spawnCondor(f'{key}_{self.tag}_hist{j}', s, xmin, xmax, nbins)
    


    def runhists(self, xmin, xmax, nbins):
        for fl in ['datadicts','utils','htcutils','exechtc', 'exechtc_multi']:
            os.system(f'xrdcp -f {fl}.py root://{site}//store/user/acrobert/condor/.')
    
        for k in range(len(self.types)):
            for i in range(len(self.keys[k])):    
                key = self.keys[k][i]
                self.addtocondor(key, xmin, xmax, nbins)
    
        if self.rundata:
            for key in self.data:
                self.addtocondor(key, xmin, xmax, nbins)

        self.bins = [np.linspace(xmin[i], xmax[i], nbins+1) for i in range(len(xmin))]
                
        print('Spawning {} condor jobs...'.format(self.nfiles))
        os.system('condor_submit subcondorHist')
        print('Submitted!')
    
        os.system('rm subcondorHist')
        self.checkhists()

    def checkhists(self):
        while True:
    
            print(round(self.tmr.time()/60., 3))
            print(checkcq(self.tag))
            if checkcq(self.tag) == {}:
                break
            sleep(30)

    def getpkls(self, key):

        os.system(f'xrdfs root://{site}/ ls /store/user/acrobert/condor/pkl/ > lstmp.txt 2>&1')
        lsout = open('lstmp.txt','r')
        lslines = lsout.readlines()

        thisnall = 0
        thisnevts = [0 for j in range(self.nhists)]
        thiscountslist = None

        for line in lslines:
            if key in line and self.tag in line:

                rfname = f'root://{site}/'+line.rstrip()
                locname = line.rstrip().split('/')[-1]
                os.system(f'xrdcp {rfname} pkl/.')

                with open('pkl/'+locname, "rb") as output_file:
                    outdict = pkl.load(output_file)
                    for ih in range(self.nhists):
                        c, bins = outdict['hist'][ih].to_numpy()
                        if thiscountslist is None:
                            self.nbins = len(c)
                            thiscountslist = [ np.zeros((self.nbins)) for k in range(self.nhists) ]
                        thiscountslist[ih] += c
                        thisnevts[ih] += outdict['nevts'][ih]
                    thisnall += outdict['nall']

                os.system('rm pkl/'+locname)

        os.system('rm lstmp.txt')

        return thisnall, thisnevts, thiscountslist

    def buildcounts(self):

        nall = [[0 for i in self.keys[k]] for k in range(len(self.types))]
        nevts = [[[0 for j in range(self.nhists)] for i in self.keys[k]] for k in range(len(self.types))]

        countslist = None
        #countslist = [[[ np.zeros((self.nbins)) for k in range(self.nhists) ] for i in self.keys[j] ] for j in range(len(self.types)) ]                                                                           
        for it in range(len(self.types)):
            for ik in range(len(self.keys[it])):
                thisnall, thisnevts, thiscountslist = self.getpkls(self.keys[it][ik])
                nall[it][ik] += thisnall
                if countslist is None:
                    self.nbins = len(thiscountslist[0])
                    countslist = [[[ np.zeros((self.nbins)) for k in range(self.nhists) ] for i in self.keys[j] ] for j in range(len(self.types)) ]

                for ih in range(self.nhists):
                    nevts[it][ik][ih] += thisnevts[ih]
                    countslist[it][ik][ih] += thiscountslist[ih]

        print(nall, nevts)

        return nall, nevts, countslist

    def rescalecounts(self, thesekeys, typenall, typenevts, typecountslist):

        retcounts = [[] for ih in range(self.nhists)]
        reterrs = [[] for ih in range(self.nhists)]
        retnevts = [[] for ih in range(self.nhists)]

        for ik in range(len(thesekeys)):
            thiskey = thesekeys[ik]
            if 'data' in thiskey:
                rescale = dsetlumi['Run2'] / dsetlumi[thiskey]
            else:
                rescale = signalevents[thiskey] * filtereffs[thiskey] / typenall[ik]
            for ih in range(self.nhists):
                count = typecountslist[ik][ih]
                retcounts[ih].append(count * rescale)
                reterrs[ih].append(np.sqrt(count) * rescale )
                retnevts[ih].append(typenevts[ik][ih] * rescale )

        return retcounts, reterrs, retnevts

    
    def readhists(self, xlab, ylab, title, logscale, normalized, run2rescale, stack, combinetypes, showSB, mean_spread, separate):
        #countslist = [[ np.zeros((nbins)) for i in self.keys[j] ] for j in range(len(self.types)) ]
        countslist = []

        if separate and showSB:
            raise ValueError

        countslist = []

        if separate and showSB:
            raise ValueError

        nall, nevts, countslist = self.buildcounts()

        finalcounts = [[] for ih in range(self.nhists)]
        finalerrs = [[] for ih in range(self.nhists)]
        finalkeys = []
        finalnevts = [[] for ih in range(self.nhists)]

        for it in range(len(self.types)):

            thiscounts, thiserrs, thisnevts = self.rescalecounts(self.keys[it], nall[it], nevts[it], countslist[it])

            if not combinetypes:
                finalkeys += self.keys[it]
                for ih in range(self.nhists):
                    finalcounts[ih] += thiscounts[ih]
                    finalerrs[ih] += thiserrs[ih]
                    finalnevts[ih] += thisnevts[it]
            else:
                finalkeys.append(self.types[it])
                for ih in range(self.nhists):
                    counts = np.array([thiscounts[ih][ik] for ik in range(len(self.keys[it]))])
                    errs = np.array([thiserrs[ih][ik] for ik in range(len(self.keys[it]))])
                    typecount = np.sum(counts, axis=0)
                    typeerrs = np.sqrt(np.sum(errs**2, axis=0))

                    finalcounts[ih].append(typecount)
                    finalerrs[ih].append(typeerrs)

                    finalnevts[ih].append(sum([thisnevts[ih][ik] for ik in range(len(self.keys[it]))]))

                    
        for i in range(self.nhists):
            tag = self.selection.split(',')[i] + '_' + self.fillhist.split(',')[i]
            self.plothist(finalcounts[i], finalerrs[i], finalkeys, finalnevts[i], self.bins[i], xlab[i], ylab, title[i], logscale, normalized, run2rescale, stack, combinetypes, showSB, mean_spread, separate, tag) 
            
            
        rmyn = input('Type "yes" to delete all hists in xrd dir, else enter: ')
        if rmyn == 'yes':

            os.system(f'xrdfs root://{site}/ ls /store/user/acrobert/condor/pkl/ > lstmp.txt 2>&1')
            lsout = open('lstmp.txt','r')
            lslines = lsout.readlines()

            for i in range(len(self.types)):
                for j in range(len(self.keys[i])):
                    for line in lslines:
                        if self.keys[i][j] in line and self.tag in line:
                            os.system(f'xrdfs root://{site}/ rm '+line.rstrip())
                            
    def add_mean_spread(self, finalcounts, bins, ax, separate, i):

        binwidth = bins[1] - bins[0]
        mean = np.sum((bins[:-1] + binwidth/2) * (finalcounts)) / np.sum(finalcounts)

        densityvals = finalcounts
        if not separate:
            densityvals = densityvals / np.sum(finalcounts * (bins[1] - bins[0]))
        maxval = np.max(densityvals)
        print(densityvals, maxval)    
            
        stdev = np.sqrt(np.sum(finalcounts * (bins[:-1] + binwidth/2 - mean)**2 ) / (np.sum(finalcounts)-1) )
        fwhm = 2*np.sqrt(2*(math.log(2)))*stdev

        x1 = [mean, mean]
        y1 = [0., maxval]
        x2 = [mean - fwhm/2, mean + fwhm/2]
        y2 = [0.1*maxval + 0.03*maxval*i, 0.1*maxval + 0.03*maxval*i]
    
        ax.plot(x2, y2, linestyle='--', color='C'+str(i))
        ax.plot(x1, y1, linestyle='--', color='C'+str(i))
                
                            
                            

    def plothist(self, finalcounts, finalerrs, finalkeys, finalnevts, bins, xlab, ylab, title, logscale, normalized, run2rescale, stack, combinetypes, showSB, mean_spread, separate, tag):
        
        size = (16, 12)
        
        if showSB:
            fig = plt.figure(figsize=size)
            gs = fig.add_gridspec(2, 1, height_ratios=(4, 1))

            ax2 = fig.add_subplot(gs[1,0])
            ax = fig.add_subplot(gs[0,0], sharex=ax2)

            fig.subplots_adjust(hspace=0.1)

        elif separate:
            nplots = len(finalcounts)
            fig = plt.figure(figsize=size)
            gs = fig.add_gridspec(2, int(nplots/2), wspace=0.3, hspace=0.3)

            ax = []
            for i in range(nplots):
                ax.append(fig.add_subplot(gs[i % 2, i // 2]))
            
        else:
            fig, ax = plt.subplots(figsize=size)

        for i in range(len(finalcounts)):

            if not separate:
                mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=finalcounts[i]), yerr=finalerrs[i], label=finalkeys[i].replace('_','-'), ax=ax, density=normalized, color='C'+str(i))
                #mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=finalcounts[i]), yerr=finalerrs[i], label=finalkeys[i].replace('_','-')+' (N='+str(finalnevts[i])+')', ax=ax1, density=normalized)            
            else:
                mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=finalcounts[i]), yerr=finalerrs[i], ax=ax[i], color='C'+str(i))
                ax[i].set_title(finalkeys[i].replace('_','-')+' (N='+str(round(finalnevts[i], 2))+')')
                if i // 2 == 0:
                    ax[i].set_ylabel(ylab)
                if i % 2 == 1:
                    ax[i].set_xlabel(xlab)
                                
        if run2rescale and stack:
            
            if separate:
                raise ValueError

            mplhep.histplot([finalcounts[-1*i-1] for i in range(len(finalkeys))], bins=bins, histtype='fill', stack=True, label=[finalkeys[-1*i-1].replace('_','-') for i in range(len(finalkeys))], ax=ax)

            tcounts = np.array(finalcounts)
            sumcounts = np.sum(tcounts, axis=0)

            mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=sumcounts), yerr=np.sqrt(sumcounts), label='Sum', ax=ax, histtype='errorbar', color='k')

        if not separate:
            ax.set_ylabel(ylab)
            ax.set_title(title)
            ax.legend()
            if logscale:
                ax.set_yscale('log')
                
        if showSB:
            ax.tick_params(labelbottom=False)
            ax2.set_xlabel(xlab)
            ax2.set_ylabel(r'$S/\sqrt{B}$')

            sig = np.array(finalcounts[-1])
            sigma_s = np.array(finalerrs[-1])
            bkg = np.sum(np.array(finalcounts[:-1]), axis=0)
            sigma_b = np.sqrt(np.sum(np.array(finalerrs[:1])**2, axis=0))
            
            ratios = np.nan_to_num(sig / np.sqrt(bkg), nan=0., posinf=0.)
            raterrs = np.nan_to_num(ratios * np.sqrt( sigma_s**2/sig**2 + 1/4 * sigma_b**2/bkg**2 ), nan=0., posinf=0.)
            mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=ratios), yerr=np.sqrt(raterrs), ax=ax2, histtype='errorbar', color='k', markersize=2.)

        elif not separate:
            ax.set_xlabel(xlab)


        if mean_spread:
            for i in range(len(finalcounts)):
                if separate:
                    self.add_mean_spread(finalcounts[i], bins, ax[i], separate, i)
                else:
                    self.add_mean_spread(finalcounts[i], bins, ax, separate, i)
                    
        fname = f'plots/combinedhist_{tag}.pdf'
        if self.style is not None:
            fname = f'plots/combinedhist_{tag}_{self.style}.png'
        print(fname)
        plt.savefig(fname)
        plt.close(fig)

