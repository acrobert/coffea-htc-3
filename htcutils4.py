import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
from argparse import ArgumentParser
import os, psutil
import tracemalloc
import gc
from copy import deepcopy

tmr = timer()
pucut = 4

btagthreshs = {'loose': 0.0490, 'medium': 0.2783, 'tight': 0.7100}

def postsel_b_bb_bin(tagnano, btagthresh):

    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    tagnano = tagnano[(ak.num(bbjets[bbjets.pt > 30.]) >= 1) & (ak.num(bjets[bjets.pt > 30.]) >= 1)]

    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]
    
    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    zsys = bjet4V[:,0] + bbjet4V[:,0]

    return tagnano, zsys

def postsel_3b_bin(tagnano, btagthresh):

    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    tagnano = tagnano[(ak.num(bjets) >= 3)]

    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    zsys = bjet4V[:, 0] + bjet4V[:, 1] + bjet4V[:, 2]

    return tagnano, zsys

def lepsel(tagnano, nleps):

    muons = tagnano.Muon
    muons = muons[(muons.pt > 15.) & (abs(muons.eta) < 2.4) & (muons.looseId)]
    muons = muons[(muons.pfRelIso04_all < 0.25) & (abs(muons.dz) < 0.2) & (abs(muons.dxy) < 0.05)]
    muons = muons.mask[ak.num(muons) == nleps]
    muons4V = ak.zip({"pt": muons.pt, "eta": muons.eta, "phi": muons.phi, "mass": muons.mass},
                    with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    eles = tagnano.Electron
    eles = eles[(eles.pt > 17) & (abs(eles.eta) < 2.4) & (eles.mvaFall17V2noIso_WP90)]
    eles = eles[(eles.pfRelIso03_all < 0.15) & (abs(eles.dz) < 0.2) & (abs(eles.dxy) < 0.05)]
    eles = eles.mask[ak.num(eles) == nleps]
    eles4V = ak.zip({"pt": eles.pt, "eta": eles.eta, "phi": eles.phi, "mass": eles.mass},
                    with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    if ak.sum(ak.num(eles)) > 0 and ak.sum(ak.num(muons)) > 0:
        leps4Vtemp = ak.concatenate(ak.Array([muons4V, eles4V]), axis=1)
    elif ak.sum(ak.num(eles)) > 0:
        leps4Vtemp = eles4V
    elif ak.sum(ak.num(muons)) > 0:
        leps4Vtemp = muons4V
    else:
        leps4Vtemp = eles4V
    leps4V = ak.zip({"pt": leps4Vtemp.pt, "eta": leps4Vtemp.eta, "phi": leps4Vtemp.phi, "mass": leps4Vtemp.mass},
                    with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    tagnano = tagnano[ak.fill_none(ak.num(leps4V), 0) == nleps]
    leps4V = leps4V[ak.fill_none(ak.num(leps4V), 0) == nleps]
    
    return tagnano, leps4V

def presel_hbb_0L(tagnano, hltkey):
    
    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]

    nleps = 0
    tagnano, leps4V = lepsel(tagnano, nleps)

    return tagnano

def presel_hbb_1L(tagnano, hltkey):

    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]

    nleps = 1
    tagnano, leps4V = lepsel(tagnano, nleps)

    return tagnano, leps4V

def presel_hbb_2L(tagnano, hltkey):

    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]

    nleps = 2
    tagnano, leps4V = lepsel(tagnano, nleps)

    return tagnano

def selection_hbb_2L_3b(tagnano, hltkey, btagthresh):

    tagnano = presel_hbb_2L(tagnano, hltkey)
    tagnano, zsys = postsel_3b_bin(tagnano, btagthresh)

    lepsys = leps4V[:, 0] + leps4V[:, 1]
    tagnano = tagnano[(leps4V[:, 0].pt > 25.) & (lepsys.mass > 75.) & (lepsys.mass < 105.) & (lepsys.pt > 75.)]

    return tagnano

def selection_hbb_2L_b_bb(tagnano, hltkey, btagthresh):

    tagnano = presel_hbb_2L(tagnano, hltkey)
    tagnano, zsys = postsel_b_bb_bin(tagnano, btagthresh)

    lepsys = leps4V[:, 0] + leps4V[:, 1]
    tagnano = tagnano[(leps4V[:, 0].pt > 25.) & (lepsys.mass > 75.) & (lepsys.mass < 105.) & (lepsys.pt > 75.)]

    return tagnano

def selection_hbb_1L_3b(tagnano, hltkey, btagthresh):
    
    tagnano, leps4V = presel_hbb_1L(tagnano, hltkey)
    tagnano, zsys = postsel_3b_bin(tagnano, btagthresh)

    mT = np.sqrt(2*leps4V[:, 0].pt*tagnano.MET.pt*(1 - np.cos(leps4V[:, 0].delta_phi(tagnano.MET))))
    delsquare = (80.377**2 - mT**2) / (2*leps4V[:, 0].pt*tagnano.MET.pt)

    etanu0 = leps4V[:, 0].eta + np.arccosh(1 + delsquare) 
    etanu1 = leps4V[:, 0].eta - np.arccosh(1 + delsquare)

    etanu = ak.where(np.isreal(etanu0), etanu0, etanu1)
    
    nu4V = ak.zip({"pt": tagnano.MET.pt, "eta": etanu, "phi": tagnano.MET.phi, "mass": ak.full_like(tagnano.MET.pt, 0.)},
                  with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    wsys = leps4V[:, 0] + nu4V
    passes_this_cut = (wsys.pt > 170.) & (wsys.delta_phi(tagnano.MET) < 2.) & (leps4V[:, 0].pt > 25.)
    tagnano = tagnano[passes_this_cut]
    wsys = wsys[passes_this_cut] 
    zsys = zsys[passes_this_cut]

    tagnano = tagnano[zsys.delta_phi(wsys) > 2.5]

    return tagnano

def selection_hbb_1L_b_bb(tagnano, hltkey, btagthresh):

    os.system('cat htcutils.py')

    tagnano, leps4V = presel_hbb_1L(tagnano, hltkey)
    tagnano, zsys = postsel_b_bb_bin(tagnano, btagthresh)

    print('pt', leps4V[:, 0].pt)
    mT = np.sqrt(2*leps4V[:, 0].pt*tagnano.MET.pt*(1 - np.cos(leps4V[:, 0].delta_phi(tagnano.MET))))
    delsquare = (80.377**2 - mT**2) / (2*leps4V[:, 0].pt*tagnano.MET.pt)

    etanu0 = leps4V[:, 0].eta + np.arccosh(1 + delsquare)
    etanu1 = leps4V[:, 0].eta - np.arccosh(1 + delsquare)

    etanu = ak.where(np.isreal(etanu0), etanu0, etanu1)

    nu4V = ak.zip({"pt": tagnano.MET.pt, "eta": etanu, "phi": tagnano.MET.phi, "mass": ak.full_like(tagnano.MET.pt, 0.)},
                  with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    wsys = leps4V[:, 0] + nu4V
    passes_this_cut = (wsys.pt > 170.) & (wsys.delta_phi(tagnano.MET) < 2.) & (leps4V[:, 0].pt > 25.)
    tagnano = tagnano[passes_this_cut]
    wsys = wsys[passes_this_cut]
    zsys = zsys[passes_this_cut]

    tagnano = tagnano[zsys.delta_phi(wsys) > 2.5]

    return tagnano


def selection_hbb_0L_3b(tagnano, hltkey, btagthresh):

    tagnano = presel_hbb_0L(tagnano, hltkey)
    tagnano, zsys = postsel_3b_bin(tagnano, btagthresh)

    seljets = tagnano.Jet[(tagnano.Jet.pt >= 20) & (abs(tagnano.Jet.eta) < 2.5) & (tagnano.Jet.jetId == 6) & (tagnano.Jet.puId >= pucut)]
    seljets4V = ak.zip({"pt": seljets.pt, "eta": seljets.eta, "phi": seljets.phi, "mass": seljets.mass},
                       with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    
    pfMET = tagnano.MET
    tkMET = tagnano.TkMET
    jetsum = ak.sum(seljets4V, axis=1)
    MHT = jetsum.pt
    tagnano = tagnano[(abs(zsys.delta_phi(pfMET)) > 2.) & (pfMET.pt > 190.) & (abs(pfMET.delta_phi(tkMET)) < 0.5) & (ak.min(ak.Array([MHT, pfMET.pt]), axis=0) > 150.)]
    
    return tagnano

def selection_hbb_0L_b_bb(tagnano, hltkey, btagthresh):

    tagnano = presel_hbb_0L(tagnano, hltkey)
    tagnano, zsys = postsel_b_bb_bin(tagnano, btagthresh)

    seljets = tagnano.Jet[(tagnano.Jet.pt >= 20) & (abs(tagnano.Jet.eta) < 2.5) & (tagnano.Jet.jetId == 6) & (tagnano.Jet.puId >= pucut)]
    seljets4V = ak.zip({"pt": seljets.pt, "eta": seljets.eta, "phi": seljets.phi, "mass": seljets.mass},
                       with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    pfMET = tagnano.MET
    tkMET = tagnano.TkMET
    jetsum = ak.sum(seljets4V, axis=1)
    MHT = jetsum.pt
    tagnano = tagnano[(abs(zsys.delta_phi(pfMET)) > 2.) & (pfMET.pt > 190.) & (abs(pfMET.delta_phi(tkMET)) < 0.5) & (ak.min(ak.Array([MHT, pfMET.pt]), axis=0) > 150.)]

    return tagnano

def selection_zjhlt_b_bb_dR1p5(tagnano, hltkey, btagthresh):

    # basic selection with ZJets HLT                                                                                                                                                                              
    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]
    selbjets = tagnano.Jet[(tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4) & (tagnano.Jet.btagDeepFlavB > btagthresh) & (tagnano.Jet.puId >= 4)]
    tagnano = tagnano[ak.num(selbjets) >= 2]

    # bjet - bbjet bin selection                                                                                                                                                                                  
    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    tagnano = tagnano[(ak.num(bbjets[bbjets.pt > 30.]) >= 1) & (ak.num(bjets[bjets.pt > 30.]) >= 1)]

    # cut on the dR between the two jets                                                                                                                                                                          
    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    passes = (ak.num(bjet4V) > 0) & (ak.num(bbjet4V) > 0)
    bbtobdR = bbjet4V[passes][:, 0].delta_r(bjet4V[passes])
    selbjet = bjet4V[passes][bbtobdR == ak.min(bbtobdR, axis=1)]

    tagnano = tagnano[passes][ak.min(bbtobdR, axis=1) <= 1.5]

    return tagnano


def fillhist_b_bb(tagnano, hist, btagthresh):

    # dijet mass                                                                                                                                                                                                  
    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    passes = (ak.num(bjet4V) > 0) & (ak.num(bbjet4V) > 0)
    bbtobdR = bbjet4V[passes][:, 0].delta_r(bjet4V[passes])
    selbjet = bjet4V[passes][bbtobdR == ak.min(bbtobdR, axis=1)]

    dijetmass = (bbjet4V[passes][:, 0] + selbjet[:, 0]).mass
    hist.fill(dijetmass)
    return len(tagnano)

def fillhist_3b(tagnano, hist, btagthresh):

    # trijet mass                     
    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    trijetmass = (bjet4V[:, 0] + bjet4V[:, 1] + bjet4V[:, 2]).mass
    if ak.sum(trijetmass) > 0:
        hist.fill(trijetmass)
    return len(tagnano)

def loadfile(loc, path='', metadata={}):

    if path=='':
        try:
            nano = NanoEventsFactory.from_root(
                loc,
                schemaclass=NanoAODSchema.v6,
                metadata=metadata,
            ).events()
        except uproot.exceptions.KeyInFileError:
            return None
        except OSError:
            return None
    else:
        try:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                nano = NanoEventsFactory.from_root(
                    loc,
                    treepath=path,
                    schemaclass=NanoAODSchema.v6,
            metadata=metadata,
                ).events()
        except uproot.exceptions.KeyInFileError:
            return None
        except OSError:
            return None

    return nano


def loadpair(nanofile, friendfile, mode='1Fr'):

    tagnano = loadfile(nanofile, metadata={"dataset file": nanofile})
    friends = [loadfile(file, path='fevt/EvtTree', metadata={"dataset file": file}) for file in friendfile]

    if tagnano is None:
        print(f'Skipping null file: {nanofile.split("/")[-1]}')
        return None
    if friends[0] is None:
        locpath = friendfile[0].replace('root://cmsdata.phys.cmu.edu/', '')
        os.system(f'xrdfs root://cmsdata.phys.cmu.edu/ rm {locpath}')
        print(f'Skipping null file: {friendfile[0].split("/")[-1]}')
        return None
        #return tagnano                                                                                                                                                                                        

    if mode != '1Fr':
        if friends[1] is None:
            print(f'Skipping null file: {friendfile[1].split("/")[-1]}')
            return None

    if mode != '1Fr':
        if len(tagnano) != len(friends[0]) or len(tagnano) != len(friends[1]):
            print(f'Skipping unmatched files: {len(friends[0])} {len(friends[1])} {len(tagnano)}')
            return None
    else:
        if len(tagnano) != len(friends[0]):
            print(f'Skipping unmatched files: {len(friends[0])} {len(tagnano)}')
            return None

    assert len(tagnano) == len(friends[0])
    #assert len(tagnano) == len(friends[1])                                                                                                                                                                    

    if mode != '1Fr':
        keys = ['Zs', 'map']
        for i in range(len(friends)):
            #print(i, friends[i].fields)                                                                                                                                                                       
            if keys[i] not in friends[i].fields:
                print(f'Skipping missing field: {keys[i]}')
                return None
            tagnano[keys[i]] = friends[i][keys[i]]
        #print(f'Running file pair {nanofile}; elapsed time {round(tmr.time()/60., 3)}')                                                                                                                       
    else:
        keys = ['Zs', 'map', 'AllTruthBs','NonZTruthBs']
        for i in range(len(keys)):
            #print(friends[0].fields)                                                                                                                                                                          
            if keys[i] not in friends[0].fields:
                print(f'Skipping missing field: {keys[i]}')
                return None
            try:
                tagnano[keys[i]] = friends[0][keys[i]]
            except:
                print('Skipping missing field:', keys[i])

    tagnano['friendjet_noptcut'] = friends[0]['friendjet']
    ptthresh = friends[0].friendjet.pt > 15.00001
    friends[0]['friendjet'] = friends[0]['friendjet'][ptthresh]
    tagnano['friendjet'] = friends[0]['friendjet']

    diffs = ak.num(tagnano.Jet) != ak.num(friends[0]['friendjet'])
    diffs = ak.num(tagnano.Jet) != ak.num(tagnano.friendjet.pfDeepFlavourJetTags_probbb)
    if ak.sum(diffs) != 0:
        print(ak.num(tagnano.Jet)[diffs], ak.num(tagnano.friendjet.pfDeepFlavourJetTags_probbb[diffs]))

        ptthresh = friends[0].friendjet.pt > 15.001
        friends[0]['friendjet'] = friends[0]['friendjet'][ptthresh]
        tagnano['friendjet'] = friends[0]['friendjet']

    return tagnano

def checkcq(tag):
    statuses = {}
    os.system('condor_q > cqtmp.txt 2>&1')
    try:
        with open('cqtmp.txt','r') as cqout:
            cqlines = cqout.readlines()
            for line in cqlines:
                if 'makeHist.sh' in line and tag in line:
                    linelist = line.split(' ')
                    lineinfo = list(filter(lambda a: a != '', linelist[:-1]))

                    key = lineinfo[9].split('_')[0]
                    if key not in statuses.keys():
                        statuses[key] = [0, 0, 0]

                    if lineinfo[5] == 'I':
                        statuses[key][0] += 1
                    elif lineinfo[5] == 'R':
                        statuses[key][1] += 1
                    else:
                        statuses[key][2] += 1
        os.system('rm cqtmp.txt')
    except IOError:
        pass

    return statuses

selections = {'zjhlt_b_bb_dR1p5': selection_zjhlt_b_bb_dR1p5, 'hbb_0L_3b': selection_hbb_0L_3b, 'hbb_1L_3b': selection_hbb_1L_3b, 'hbb_2L_3b': selection_hbb_2L_3b,
              'hbb_0L_b_bb': selection_hbb_0L_b_bb, 'hbb_1L_b_bb': selection_hbb_1L_b_bb, 'hbb_2L_b_bb': selection_hbb_2L_b_bb}
fillhists = {'b_bb': fillhist_b_bb, '3b': fillhist_3b}

