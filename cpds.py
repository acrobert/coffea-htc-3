# /store/user/acrobert/Zpq/Sep2023/ZZTo2L4B-1j-g3/ZZTo2L4B-1j-g3_parquet_1000evts_unmatchedsaj_1.pq
import os
#types = ['matchednonjet', 'matchedsaj', 'matchedSV', 'unmatchednonjet', 'unmatchedsaj','unmatchedSV']
types = ['matchedsaj', 'unmatchedsaj']

for t in types:
    for i in range(50):
        os.system(f'xrdcp root://cmsdata.phys.cmu.edu//store/user/acrobert/Zpq/Sep2023/ZZTo2L2B-NLO-1j-g0/ZZTo2L2B-NLO-1j-g0_parquet_10000evts_{t}_{i}.pq ../gpus/parquet/.')
