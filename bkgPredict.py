import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
import matplotlib.pyplot as plt
import mplhep
from argparse import ArgumentParser
from classRunCoffea import RunCoffea
import os, psutil
import tracemalloc
import gc
from copy import deepcopy
import matplotlib.ticker as ticker

#plt.rcParams['text.usetex'] = True
tmr = timer()

#keys = ['ZJetsTo2B-HT800-4j-g0', 'TTJets_up20UL18', 'ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18', 'QCD_HT2000_up20UL18']
#keys = ['TTJets_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18', 'ZJetsTo2B-HT800-4j-g0']
#types = ['TTJets', 'VJets', 'QCD', 'ZJets to 4B']
#keys = [['TTJets_up20UL18'], ['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['QCD_HT10to15_up20UL18', 'QCD_HT15to20_up20UL18', 'QCD_HT2000_up20UL18'], ['ZJetsTo2B-HT4to6-4j-g0', 'ZJetsTo2B-HT6to8-4j-g0', 'ZJetsTo2B-HT800-4j-g0']]

#types = ['VJets', 'ZJets to 4B']
#keys = [['ZJets_HT800_up20UL18', 'WJets_HT800_up20UL18'], ['ZJetsTo2B-HT800-4j-g0']]

#types = ['QCD', 'Data', 'ZJets']
#keys = [['QCD_HT10to15_up20UL18'], ['data_JetHT_up20UL18_A'], ['ZJets_HT800_up20UL18']]
types = ['Data']
keys = [['data_JetHT_up20UL18_A']]


xmin = 0
xmax = 300
nbins = 30
xlab = 'Dijet Mass'
ylab = 'Events'
logscale = False
title = r'BJet + BBJet mass Bkg Prediction (not rescaled)'
fname = 'plots/bkgpredicthist_dijetmass_b-bb-bin-dR1p5_ABCD_{}.pdf'
normalized = False
run2rescale = False
stack = False
combinetypes = True


maxevents = 10000
maxfiles = 400

thishist = [[ [Hist(hist.axis.Regular(nbins, xmin, xmax, name='bin')) for k in range(4)] for i in range(len(keys[j])) ] for j in range(len(types)) ]
loose_btagthresh = 0.2783
btagthresh = 0.2783
#btagthresh = 0.7100
pucut = 4

def create_jets(tagnano):

    jets = tagnano.Jet[(tagnano.Jet.puId >= pucut) & (abs(tagnano.Jet.eta) < 2.4) & (tagnano.Jet.pt >= 30.)]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = tagnano.Jet[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = tagnano.Jet[btagged & ~bbtagged]
    nonjets = tagnano.Jet[~btagged & ~bbtagged]
    
    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
                    with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
                     with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    
    return bbjets, bjets, nonjets

def cut_a(tagnano):

    bbjets, bjets, nonjets = create_jets(tagnano)
    return (ak.num(bbjets) >= 1) & (ak.num(bjets) >= 1) 

def cut_b(tagnano, jets_1, jets_2):

    mat_12 = ak.cartesian([jets_1, jets_2], axis=1, nested=True)
    dR_12 = mat_12['0'].delta_r(mat_12['1'])
    mindR_12 = ak.min(dR_12, axis = 2)
    return ak.flatten(mindR_12 < 1.5, axis=None)    

def cut_mass(tagnano, jets_1, jets_2):

    mat_12 = ak.cartesian([jets_1, jets_2], axis=1, nested=True)
    dR_12 = mat_12['0'].delta_r(mat_12['1'])
    seljet = jets_2[ak.flatten(dR_12 == ak.min(dR_12, axis = 2), axis=1)]


    mass = (jets_1 + seljet).mass
    #print('masscut', mass)
    return (mass > 70.) & (mass < 110.)


def selection(tagnano, hideSR=True):

    # basic selection with ZJets HLT
    #print(len(tagnano), tagnano.Jet.pt)
    tagnano = tagnano[passHLT(tagnano, anaHLT['ZJetsTo2B-HT800-4j-g0'])]
    selbjets = tagnano.Jet[(tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4) & (tagnano.Jet.btagDeepFlavB > btagthresh) & (tagnano.Jet.puId >= 4)]
    tagnano = tagnano[ak.num(selbjets) >= 2]

    bbjets, bjets, nonjets = create_jets(tagnano)

    tagnano = tagnano[(ak.num(bjets) >= 2) | ((ak.num(bjets) >= 1) & (ak.num(bbjets) >= 1))]
    
    A = cut_a(tagnano)

    bbjets, bjets, nonjets = create_jets(tagnano)    
    jets_1 = ak.where(A, ak.firsts(bbjets), ak.firsts(bjets))
    jets_2 = ak.where(A, bjets, bjets[:, 1:])
    B = cut_b(tagnano, jets_1, jets_2)

    #print(len(tagnano), ak.sum(A & B), ak.sum(A & ~B), ak.sum(~A & B), ak.sum(~A & ~B))
    #print(A.to_numpy().shape, B.to_numpy().shape)
    if not hideSR:
        return tagnano[A & B], tagnano[A & ~B], tagnano[~A & B], tagnano[~A & ~B]
    else:
        C = cut_mass(tagnano, jets_1, jets_2)
        C = ak.flatten(C, axis=None)
        return tagnano[A & B & ~C], tagnano[A & ~B], tagnano[~A & B], tagnano[~A & ~B]


def fillhist(tagnanos, hist):

    # dijet mass
    for i in range(4):
        tagnano = tagnanos[i]
        #print(len(tagnano), tagnano.Jet.pt)
        bbjets, bjets, nonjets = create_jets(tagnano)

        if (i == 0 or i == 1):

            bbtobdR = bbjets[:, 0].delta_r(bjets)
            selbjet = bjets[bbtobdR == ak.min(bbtobdR, axis=1)]

            dijetmass = (bbjets[:, 0] + selbjet[:, 0]).mass
            #print(f'fill{i}', ak.flatten(dijetmass, axis=None))
            hist[i].fill(ak.flatten(dijetmass, axis=None))
            
        else:
            btobdR = bjets[:, 0].delta_r(bjets[:, 0:])
            btobdR =  ak.where(btobdR > 0., btobdR, 100.)
            selbjet = bjets[btobdR == ak.min(btobdR, axis=1)]

            #print(bjet4V[:, 0].delta_r(selbjet))

            dijetmass = (bjets[:, 0] + selbjet[:, 0]).mass
            #print(f'fill{i}b', ak.flatten(dijetmass, axis=None))
            hist[i].fill(dijetmass)

    return len(tagnanos[0])


nall = [[0 for i in keys[k]] for k in range(len(types))]
nevts = [[[0 for k in range(4)] for i in keys[k]] for k in range(len(types))]

for k in range(len(types)):
    for i in range(len(keys[k])):

        key = keys[k][i]
        
        print(f'Running key {key}; elapsed time {round(tmr.time()/60., 3)}')
        #if key == 'TTJets_up20UL18':
        #    filepairs = RunCoffea.initnanos(key, mode='1Fr')
        #else:
        filepairs = RunCoffea.initnanos(key, date='Apr2024')
        
        nfiles = 0
        nevents = 0
        for j in range(len(filepairs)):
        
            nanofile, friendfile = filepairs[j]
            if key == 'TTJets_up20UL18':
                tagnano = RunCoffea.loadpair(nanofile, friendfile, mode='1Fr')
            else:
                tagnano = RunCoffea.loadpair(nanofile, friendfile) 
        
            if tagnano is None:
                continue
            if 'Zs' not in tagnano.fields:
                continue
           
            print(f'Running file pair {j} filled {nevents}; elapsed time {round(tmr.time()/60., 3)}')
            nfiles += 1
        
            print(len(tagnano))

            nall[k][i] += len(tagnano)
        
            if 'data' in key or 'Data' in key:
                hideSR = True
            else:
                hideSR = False
            tagnano_sig, tagnano_B, tagnano_C, tagnano_D = selection(tagnano, hideSR = hideSR)


            nevents += fillhist([tagnano_sig, tagnano_B, tagnano_C, tagnano_D], thishist[k][i])
        
            nevts[k][i][0] += len(tagnano_sig)
            nevts[k][i][1] += len(tagnano_B)
            nevts[k][i][2] += len(tagnano_C)
            nevts[k][i][3] += len(tagnano_D)
        
            del tagnano, tagnano_sig, tagnano_B, tagnano_C, tagnano_D
            print('Current memory:', psutil.Process(os.getpid()).memory_info().rss / 1024 ** 2)
            gc.collect()
        
            if nevents >= maxevents:
                break
            if nfiles >= maxfiles:
                break
    

#fig, (ax1, ax2) = plt.subplots(nrows=2, height_ratios=(3,1), figsize=(8, 5))
#fig, ax = plt.subplots(figsize=(8, 5))

for j in range(len(types)):

    fig = plt.figure(figsize=(8, 5))
    gs = fig.add_gridspec(2, 1, height_ratios=(4, 1))
    
    ax2 = fig.add_subplot(gs[1,0])
    ax1 = fig.add_subplot(gs[0,0], sharex=ax2)
    
    fig.subplots_adjust(hspace=0.1)
    
    finalcounts = []
    finalerrs = []
    finalkeys = []
    finalnevts = []
    
    for k in range(4):
        counts, bins = thishist[j][0][k].to_numpy()     
        print(counts)
        finalcounts.append(counts)
        finalerrs.append(np.sqrt(counts))
    
    finalnevts = nevts[j][0]
    
    htypes = ['SR', 'B', 'C', 'D']
    
    for i in range(len(finalcounts)):
        mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=finalcounts[i]), yerr=finalerrs[i], label=htypes[i]+' (N='+str(finalnevts[i])+')', ax=ax1, density=normalized)
    
    estsig = finalcounts[1] * finalcounts[2] / finalcounts[3]
    estsig = np.nan_to_num(estsig)
    estsig[estsig > 100000000.] = 0.
    esterr = estsig * np.sqrt( (finalerrs[1]/finalcounts[1])**2 + (finalerrs[2]/finalcounts[2])**2 + (finalerrs[3]/finalcounts[3])**2 )
    esterr = np.nan_to_num(esterr)
    print(estsig)
    mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=estsig), yerr=esterr, label='SR Estimate (ABCD)', ax=ax1, density=normalized)
    
    ax1.tick_params(labelbottom=False)
    ax2.set_xlabel(xlab)
    ax2.set_ylabel(r'Predicted/Actual')
    
    sig = estsig
    sigma_s = esterr
    bkg = finalcounts[0]
    sigma_b = finalerrs[0]
    
    ratios = np.nan_to_num(sig / bkg, nan=0., posinf=0.)
    raterrs = np.nan_to_num(ratios * np.sqrt( sigma_s**2/sig**2 + sigma_b**2/bkg**2 ), nan=0., posinf=0.)
    mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=ratios), yerr=np.sqrt(raterrs), ax=ax2, histtype='errorbar', color='k', markersize=2.)
    
    mplhep.cms.label(loc=1, ax=ax1)
    ax1.set_ylabel(ylab)
    #ax2.set_ylabel(r'$S/\sqrt{B}$')
    #ax1.tick_params(labelbottom=False)
    #ax2.set_xlabel(xlab)
    ax1.set_title(title)
    ax1.legend()
    if logscale:
        ax1.set_yscale('log')
    
    x1 = [xmin, xmax]
    y1 = [1., 1.]
    
    ax2.plot(x1, y1, linestyle='--', color='k')
    ax2.set_yscale('log')
    
    #sig = np.array(finalcounts[-1])
    #sigma_s = np.array(finalerrs[-1])
    #bkg = np.sum(np.array(finalcounts[:-1]), axis=0)
    #sigma_b = np.sqrt(np.sum(np.array(finalerrs[:1])**2, axis=0))
    #
    #ratios = np.nan_to_num(sig / np.sqrt(bkg), nan=0., posinf=0.)
    #raterrs = np.nan_to_num(ratios * np.sqrt( sigma_s**2/sig**2 + 1/4 * sigma_b**2/bkg**2 ), nan=0., posinf=0.)
    #mplhep.histplot(np.histogram(bins[0:-1], bins=bins, weights=ratios), yerr=np.sqrt(raterrs), ax=ax2, histtype='errorbar', color='k', markersize=2.)
    
    print(fname.format(types[j]))
    plt.savefig(fname.format(types[j]))
    plt.close(fig)
        
        


