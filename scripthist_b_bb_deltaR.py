import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
import matplotlib.pyplot as plt
import mplhep
from argparse import ArgumentParser
from classRunCoffea import RunCoffea
import os, psutil
import tracemalloc
import gc
from copy import deepcopy
import matplotlib.ticker as ticker
from time import sleep
from htcutils import *
import pickle as pkl
import sys

plt.rcParams['text.usetex'] = True
tmr = timer()

run = sys.argv[1] == 'run' 

if run:
    print(' >> running hists')

fillhist = 'dijetdr'
selection = 'zjhlt_b_bb'
btagthresh = 'medium'

style = 'same' #signal, same, None

addtag = ''
#if addtag == '' and style != '':
#    addtag = style


xmin = 0.
xmax = 4.
nbins = 20
xlab = r'Dijet $\Delta R$'
logscale = False
#title = r'Number of Single-B-Tagged Jets'
title = ''

if style == 'multi':
    normalized = False
    run2rescale = False
    stack = False
    combinetypes = True
    showSB = False
    mean_spread = False
    separate = True
    show_nevts = False
    xlab = title
    
elif style == 'signal':
    normalized = False
    run2rescale = True
    stack = False
    combinetypes = True
    showSB = True
    mean_spread = False
    separate = False
    show_nevts = True
elif style == 'same':
    normalized = True
    run2rescale = False
    stack = False
    combinetypes = True
    showSB = False
    mean_spread = False
    show_nevts = False
    separate = False
elif style is None:
    normalized = False
    run2rescale = False
    stack = False
    combinetypes = True
    showSB = False
    mean_spread = True
    separate = True
    show_nevts = True
else:
    raise ValueError

if normalized:
    ylab = 'Normalized Events'
elif run2rescale:
    ylab = 'Run 2 Events'
else:
    ylab = 'Events'


from funcrunhist import histsrunner

runner = histsrunner(fillhist, selection, btagthresh, addtag=addtag, style=style)

if run:
    runner.runhists(xmin, xmax, nbins)

print(' >> reading hists')
runner.checkhists()
runner.readhists(xlab, ylab, title, logscale, normalized, run2rescale, stack, combinetypes, showSB, mean_spread, separate)

