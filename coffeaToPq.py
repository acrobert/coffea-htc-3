import sys, os
import pickle as pkl
import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
#from utils import *
import gc
import ROOT
import pyarrow.parquet as pq
import pyarrow as pa
import time
import glob
import uproot
import warnings
import numpy as np

start = time.time()

def xglob_onedir(path):

    t3 = 'root://cmsdata.phys.cmu.edu/'

    cmd = 'xrdfs {} ls {}'.format(t3, path)
    os.system(cmd +' > tmpf')
    out = open('tmpf', 'r').read().split('\n')

    out.remove('')
    return out


def initnanos(key):

    friendtag = ['ZBQ-EPid', 'MapEcalPFCs']
    localfiles = glob.glob('nanos/*'+key+'*')
    thesefiles = xglob_onedir('/store/user/acrobert/Znano/Jan2023/'+key+'/')
    taggednano = [f for f in thesefiles if key in f and 'nano_ParT' in f]
    friendtrees = [[f for f in thesefiles if key in f and tag in f] for tag in friendtag]

    matchedpairs = []

    for i in range(len(friendtrees[0])):
        friendTname = [f'friend_{key}_{tag}_file{i}.root' for tag in friendtag]
        tagnanoname = f'nano_ParT_{key}_file{i}.root'
        friendTfile = [f'/store/user/acrobert/Znano/Jan2023/{key}/{name}' for name in friendTname]
        tagnanofile = f'/store/user/acrobert/Znano/Jan2023/{key}/{tagnanoname}'
        friendTpath = [f'root://cmsdata.phys.cmu.edu/{file}' for file in friendTfile]
        tagnanopath = f'root://cmsdata.phys.cmu.edu/{tagnanofile}'

        allfriends = all([file in thesefiles for file in friendTfile])
        if tagnanofile in thesefiles and allfriends:
            
            friendfile = [f'root://cmsdata.phys.cmu.edu/{file}' for file in friendTfile]
            nanofile = f'root://cmsdata.phys.cmu.edu/{tagnanofile}'
            matchedpairs.append((nanofile, friendfile))

    print(f'Key: {key} found {len(matchedpairs)} pairs; missing {(len(friendtrees[0]) - len(matchedpairs))}')
    return matchedpairs

def loadfile(loc, path='', metadata={}):

    try:
        if path=='':
            nano = NanoEventsFactory.from_root(
                loc,
                schemaclass=NanoAODSchema.v6,
                metadata=metadata,
            ).events()
        else:
            try:
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    nano = NanoEventsFactory.from_root(
                        loc,
                        treepath=path,
                        schemaclass=NanoAODSchema.v6,
                        metadata=metadata,
                    ).events()
            except uproot.exceptions.KeyInFileError:
                return None
    except OSError:
        if path=='':
            nano = NanoEventsFactory.from_root(
                loc,
                schemaclass=NanoAODSchema.v6,
                metadata=metadata,
            ).events()
        else:
            try:
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    nano = NanoEventsFactory.from_root(
                        loc,
                        treepath=path,
                        schemaclass=NanoAODSchema.v6,
                        metadata=metadata,
                    ).events()
            except uproot.exceptions.KeyInFileError:
                return None


    return nano

def loadpair(nanofile, friendfile):

    tagnano = loadfile(nanofile, metadata={"dataset file": nanofile})
    friends = [loadfile(file, path='fevt/EvtTree', metadata={"dataset file": file}) for file in friendfile]

    if friends[0] is None:
        return tagnano

    if friends[1] is None:
        print(f'Skipping null file: {friendfile[1].split("/")[-1]}')
        return None

    if len(tagnano) != len(friends[0]) or len(tagnano) != len(friends[1]):
        print(f'Skipping unmatched files: {len(friends[0])} {len(friends[1])} {len(tagnano)}')
        return None

    assert len(tagnano) == len(friends[0])
    assert len(tagnano) == len(friends[1])

    keys = ['Zs', 'map']
    for i in range(len(friends)):
        if keys[i] not in friends[i].fields:
            print(f'Skipping missing field: {keys[i]}')
            return None
        tagnano[keys[i]] = friends[i][keys[i]]
    
    return tagnano

def object_combiner(obj_list, redundant_rad = 0.4):

    for obj in obj_list:
        if 'mass' not in obj.fields:
            obj['mass'] = ak.zeros_like(obj.pt, dtype=float)

    list4V = [ak.zip({"pt": obj.pt, "eta": obj.eta, "phi": obj.phi, "mass": obj.mass}, with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior) for obj in obj_list]
    listidx = [ak.local_index(i4V, axis=1) for i4V in list4V]

    finlist4V = [list4V[0]]
    finlistidx = [listidx[0]]
    finlisttype = [ak.zeros_like(list4V[0].pt)]

    for i in range(1, len(list4V)):
        before = finlist4V[0] if i == 1 else ak.concatenate(finlist4V, axis=1)
        objmat = ak.cartesian([list4V[i], before], axis=1, nested=True)
        matdR = objmat['0'].delta_r(objmat['1'])
        redundant_obj = ak.where((ak.num(before) > 0), (ak.min(matdR, axis=2) > 0.4), ak.full_like(list4V[i].pt, True, dtype=bool))

        finlist4V.append(list4V[i][redundant_obj])
        finlistidx.append(listidx[i][redundant_obj])
        finlisttype.append(ak.full_like(finlist4V[i].pt, i))

    obj_4V = ak.concatenate(finlist4V, axis=1)
    obj_idx = ak.concatenate(finlistidx, axis=1)
    obj_type = ak.concatenate(finlisttype, axis=1)

    return obj_4V, obj_idx, obj_type

def match_with_4V(obj_4V, Bs4V, jetrad=0.4, radbuffer=0.2):

    has_obj = ak.num(obj_4V) >= 1
    bobjmat = ak.cartesian([Bs4V, obj_4V], axis=1, nested=True)

    bobjdR = bobjmat['0'].delta_r(bobjmat['1'])
    evtbl = (bobjdR < jetrad+radbuffer) & (bobjdR == ak.min(bobjdR, axis=-1))
    bs_per_obj = ak.sum(evtbl, axis=1)

    return evtbl, bs_per_obj


key = sys.argv[1]
fileind = int(sys.argv[2])
nevtsperfile = int(sys.argv[3])
objtype = sys.argv[4]

outname = f'{key}_parquet_{nevtsperfile}evts_{objtype}_{fileind}.pq'

filepairs = initnanos(key)

ind = 0
npassed = 0
startind = fileind*nevtsperfile
localstart = -1
data = {}
pfcfields = ['PFCands_pT', 'PFCands_phi', 'PFCands_eta', 'PFCands_d0', 'PFCands_z0', 'PFCands_Q', 'PFCands_E', 'PFCands_m', 'PFCands_nPixHits', 'PFCands_nHits', 'PFCands_status', 'PFCands_rawCaloFrac', 'PFCands_rawHcalFrac', 'PFCands_caloFrac', 'PFCands_hcalFrac', 'PFCands_time', 'PFCands_pdgId']#, 'PFCands_mva']
obj_written = 0
started = False

#for i in range(len(filepairs)):
while True:

    if ind >= len(filepairs):
        print(f' >> Finishing early at file {ind - 1}; written: {obj_written}; time: {round((time.time() - start)/60., 2)}')
        break
     
    nanofile, friendfile = filepairs[ind]
    tagnano = loadpair(nanofile, friendfile)
    
    if tagnano is None:
        continue
    if 'Zs' not in tagnano.fields or 'map' not in tagnano.fields:
        continue
        
    #print(f'Running file pair {i}; elapsed time {round(tmr.time()/60., 3)}') 
            
    jets = tagnano.Jet
    jets = jets[(jets.pt >= 30.) & (abs(jets.eta) <= 2.5)]# & (jets.puId > 4)]                                                                                                                                 
    bjets = jets[jets.btagDeepFlavB > 0.6]
    nonjets = jets[jets.btagDeepFlavB < 0.6]
    
    Zevts = tagnano.Zs[:,0]
    
    bpdg = abs(Zevts.final_Qs_pdgId) == 5
    Qs_indiv4V = ak.zip({"pt": Zevts.final_Qs_pT[bpdg], "eta": Zevts.final_Qs_eta[bpdg], "phi": Zevts.final_Qs_phi[bpdg], "energy": Zevts.final_Qs_E[bpdg]},
                        with_name="PtEtaPhiELorentzVector", behavior=vector.behavior)
    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
                    with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    obj_4V, obj_idx, obj_type = object_combiner([bjets, nonjets, tagnano.SoftActivityJet, tagnano.SV])
    b_to_obj_matches, bs_per_obj = match_with_4V(obj_4V, Qs_indiv4V)
    
    b_is_matched = ak.any(b_to_obj_matches, axis=2)
    obj_is_matched = ak.any(b_to_obj_matches, axis=1)
    
    if objtype == 'matchednonjet':
        objects = obj_4V[obj_is_matched & (obj_type == 1)]
        theseidx = obj_idx[obj_is_matched & (obj_type == 1)]
        thesetypes = obj_type[obj_is_matched & (obj_type == 1)]
        origobj = nonjets[theseidx]
    elif objtype == 'matchedsaj':
        objects = obj_4V[obj_is_matched & (obj_type == 2)]
        theseidx = obj_idx[obj_is_matched & (obj_type == 2)]
        thesetypes = obj_type[obj_is_matched & (obj_type == 2)]
        origobj = tagnano.SoftActivityJet[theseidx]
    elif objtype == 'matchedSV':
        objects = obj_4V[obj_is_matched & (obj_type == 3)]
        theseidx = obj_idx[obj_is_matched & (obj_type == 3)]
        thesetypes = obj_type[obj_is_matched & (obj_type == 3)]
        origobj = tagnano.SV[theseidx]
    elif objtype == 'unmatchednonjet':
        objects = obj_4V[~obj_is_matched & (obj_type == 1)]
        theseidx = obj_idx[~obj_is_matched & (obj_type == 1)]
        thesetypes = obj_type[~obj_is_matched & (obj_type == 1)]
        origobj = nonjets[theseidx]
    elif objtype == 'unmatchedsaj':
        objects = obj_4V[~obj_is_matched & (obj_type == 2)]
        theseidx = obj_idx[~obj_is_matched & (obj_type == 2)]
        thesetypes = obj_type[~obj_is_matched & (obj_type == 2)]
        origobj = tagnano.SoftActivityJet[theseidx]
    elif objtype == 'unmatchedSV':
        objects = obj_4V[~obj_is_matched & (obj_type == 3)]
        theseidx = obj_idx[~obj_is_matched & (obj_type == 3)]
        thesetypes = obj_type[~obj_is_matched & (obj_type == 3)]
        origobj = tagnano.SV[theseidx]

    assert ak.sum(ak.num(origobj)) == ak.sum(ak.num(objects))
    nobjects = ak.sum(ak.num(objects))
    if npassed+nobjects > startind and not started:
        localstart = startind - npassed
        print(f' >> Writing: starting at file {ind}; local index: {localstart}; time: {round((time.time() - start)/60., 2)}')
    elif started and obj_written < nevtsperfile:
        print(f' .. Writing: continuing at file {ind}')
    elif npassed+nobjects <= startind and not started:    
        print(f' .. Read file {ind} with {nobjects} objects; continuing')
        npassed += nobjects
        ind += 1
        
        del tagnano
        gc.collect()

        continue

    started = True
    print(tagnano.fields, tagnano.map.fields)
    pfcs = ak.zip({"pt": tagnano.map.PFCands_pT, "eta": tagnano.map.PFCands_eta, "phi": tagnano.map.PFCands_phi, "mass": tagnano.map.PFCands_m}, 
                        with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    obj_before = 0
    for i in range(len(tagnano)):
        if obj_written >= nevtsperfile:
            #print(i,j,obj_written,'breaking')
            break
        for j in range(len(objects[i])):
            if obj_before < localstart:
                obj_before += 1
                continue
            if obj_written >= nevtsperfile:
                #print(i,j,obj_written,'breaking')
                break
            
            if obj_written % 1000 == 0 and obj_written > 0:
                print(" .. Processing entry {},{}; written: {}; time: {}".format(i, j, obj_written, round((time.time() - start)/60., 2)))

            wherenear = ak.where(pfcs[i].delta_r(objects[i][j]) < 0.6)[0]
            thiswindow = np.zeros((1, len(wherenear), len(pfcfields)))
            
            for k in range(len(pfcfields)):
                thiswindow[0,:,k] = tagnano.map[pfcfields[k]][i][wherenear].to_numpy()
            #print(i,j,thiswindow.shape,obj_written+1)

            data = {}
            data['G'] = thiswindow
            data['type'] = objtype
            data['pt'] = objects[i][j].pt
            data['eta'] = objects[i][j].eta
            data['phi'] = objects[i][j].phi

            extras = []
            if 'nonjet' in objtype:
                data['mass'] = origobj[i][j].mass
                for key1 in ['area', 'btagCMVA', 'btagCSVV2', 'btagDeepB', 'btagDeepC', 'btagDeepCvB', 'btagDeepCvL', 'btagDeepFlavB', 'btagDeepFlavC', 'btagDeepFlavCvB', 'btagDeepFlavCvL', 'btagDeepFlavQG', 'chEmEF', 'chFPV0EF', 'chFPV1EF', 'chFPV2EF', 'chFPV3EF', 'chHEF', 'hfsigmaEtaEta', 'hfsigmaPhiPhi', 'muEF', 'muonSubtrFactor', 'neEmEF', 'neHEF', 'puIdDisc', 'qgl', 'rawFactor', 'bRegCorr', 'bRegRes', 'cRegCorr', 'cRegRes', 'electronIdx1', 'electronIdx2', 'hfadjacentEtaStripsSize', 'hfcentralEtaStripSize', 'jetId', 'muonIdx1', 'muonIdx2', 'nElectrons', 'nMuons', 'puId', 'nConstituents', 'genJetIdx', 'hadronFlavour', 'partonFlavour', 'cleanmask', 'electronIdx1G', 'electronIdx2G', 'genJetIdxG', 'muonIdx1G', 'muonIdx2G', 'muonIdxG', 'electronIdxG']:
                    #print(key, type(origobj[i][j][key]), origobj[i][j][key])
                    if type(origobj[i][j][key1]) == int or type(origobj[i][j][key1]) == float:
                        extras.append(origobj[i][j][key1])
            elif 'saj' in objtype:
                pass
            elif 'SV' in objtype:
                data['mass'] = origobj[i][j].mass
                for key2 in ['dlen', 'dlenSig', 'dxy', 'dxySig', 'pAngle', 'chi2', 'ndof', 'x', 'y', 'z', 'ntracks']:
                    extras.append(origobj[i][j][key2])
                    
            data['y'] = 0 if 'un' in objtype else 1
            data['extras'] = extras
             
            #pqdata = []
            #for d in list(data.values()):
            #    print(d)
            #    pqdata.append(pa.array([d]) if np.isscalar(d) or type(d) == list else pa.array([d.tolist()]))
            pqdata = [pa.array([d]) if np.isscalar(d) or type(d) == list else pa.array([d.tolist()]) for d in list(data.values())]
            table = pa.Table.from_arrays(pqdata, list(data.keys()))

            if obj_written == 0:
                print(f' >> Writing: parquet file {outname}')
                writer = pq.ParquetWriter(outname, table.schema, compression='snappy')
            
            writer.write_table(table)
                
            obj_written += 1
            obj_before += 1

    del tagnano
    gc.collect()
            
    if obj_written >= nevtsperfile:
        print(f' >> Finishing at file {ind}, local index {obj_before}')
        started = False
        break
    else:
        npassed += nobjects
        ind += 1
        localstart = 0

writer.close()

print(" >> ======================================")
print(" >> nobj = {}; file {}".format(obj_written, outname))
print(" >> Real time: {} minutes".format(round((time.time() - start)/60., 2)))
print(" >> ======================================")

pqds = pq.ParquetFile(outname)
assert obj_written == pqds.num_row_groups

print(pqds.metadata)
print(pqds.schema)

if obj_written != nevtsperfile:
    newout = f'{key}_parquet_{obj_written}evts_{objtype}_{fileind}.pq'
    os.system(f'mv {outname} {newout}')
    os.system(f'xrdcp {newout} root://cmsdata.phys.cmu.edu//store/user/acrobert/Zpq/Sep2023/{key}/.')
else:
    os.system(f'xrdcp {outname} root://cmsdata.phys.cmu.edu//store/user/acrobert/Zpq/Sep2023/{key}/.')

#for i in range(nevtsperfile):
#    data = pqds.read_row_group(i, columns=None).to_pydict()
#    dd = dict(data)
#    print(i, list(dd.keys()))

