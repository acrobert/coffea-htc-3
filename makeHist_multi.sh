#!/bin/sh                                                                                                                                                                                                 

ls
        
echo "Starting job on " `date` #Date/time of start of job                                                                                                                                                 
echo "Running on: `uname -a`" #Condor job is running on this node                                                                                                                                         
echo "System software: `cat /etc/redhat-release`" #Operating System on that node                                                                                                                                 
outname=$1
hltkey=$2
selection=$3
fillhist=$4
btagthresh=$5
files=$6
xmin=$7
xmax=$8
nbins=$9

#site=cmsdata.phys.cmu.edu
site=cmseos.fnal.gov

#export SCRAM_ARCH=slc7_amd64_gcc700
#source /cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc8-opt/setup.sh
export SCRAM_ARCH=el9_amd64_gcc13
source /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc13-opt/setup.sh 

xrdcp -f root://${site}//store/user/acrobert/condor/datadicts.py .
xrdcp -f root://${site}//store/user/acrobert/condor/utils.py .
xrdcp -f root://${site}//store/user/acrobert/condor/htcutils.py .
xrdcp -f root://${site}//store/user/acrobert/condor/exechtc_multi.py .

ls

python3 exechtc_multi.py $outname $hltkey $selection $fillhist $btagthresh $files $xmin $xmax $nbins

ls

xrdcp -f ${outname}.pkl root://${site}//store/user/acrobert/condor/pkl/.
