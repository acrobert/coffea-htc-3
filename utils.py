import os
import pickle
import numpy as np
import ROOT as r
#import pyarrow as pa
#import pyarrow.parquet as pq
import math
import time
#import torch
#import torch_geometric as geo
import random
import yaml
import awkward as ak
from coffea.nanoevents.methods import vector
from datadicts import *
import matplotlib.pyplot as plt
import mplhep

names = ['idx','E','pt','eta','phi','pho_id','pi0_p4','m','Xtz','ieta','iphi','p4','pho_p4','pho_vars','X','dR','y','bdt','ancestry','genId','pteta','wgt','Xk','Xk_full','Xtzk','X6','X4','X5','pu']

date = 'July2022'

#datasets = {
#      'ZJets_HT_800toInf': '/ZJetsToQQ_HT-800toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
#    , 'ZGammaToJJ': '/ZGammaToJJGamma_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
#    , 'WZToLNuQQ': '/WZTo1L1Nu2Q_4f_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
#    , 'ZZTo2Q2L': '/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'ZZTo2Q2L_mc2017UL': '/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'WZToLNuQQ_mc2017UL': '/WZTo1L1Nu2Q_4f_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'ZGammaToJJ_mc2017UL': '/ZGammaToJJGamma_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM' 
#    , 'ZJets_HT_800toInf_mc2017UL': '/ZJetsToQQ_HT-800toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM' 
#    , 'ZZTo4B01j_mc2017UL': '/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v1/MINIAODSIM'
#    , 'ZJets_HT_600to800_mc2017UL': '/ZJetsToQQ_HT-600to800_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'WZ_mc2017UL': '/WZ_TuneCP5_13TeV-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v1/MINIAODSIM'
#    , 'TTJets_mc2017UL': '/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'WJetsToLNu_mc2017UL': '/WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'sWJetsToLNu_mc2017UL': '/WJetsToLNu_012JetsNLO_34JetsLO_EWNLOcorr_13TeV-sherpa/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v4/MINIAODSIM'
#    , 'QCD_BGen_HT_700to1000_mc2017UL': '/QCD_HT700to1000_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'QCD_BGen_HT_1000to1500_mc2017UL': '/QCD_HT1000to1500_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'QCD_BGen_HT_1500to2000_mc2017UL': '/QCD_HT1500to2000_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'ZJets_HT_800toInf_F17mc': '/ZJetsToQQ_HT-800toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIFall17MiniAODv2-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/MINIAODSIM'
#    , 'ZJets_HT_800toInf_up2018UL': '/ZJetsToQQ_HT-800toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
#    , 'ZHToAATo4B_mc2017UL_m12': '/SUSY_ZH_ZToAll_HToAATo4B_M-12_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'ZHToAATo4B_mc2017UL_m15': '/SUSY_ZH_ZToAll_HToAATo4B_M-15_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'ZHToAATo4B_mc2017UL_m20': '/SUSY_ZH_ZToAll_HToAATo4B_M-20_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'ZHToAATo4B_mc2017UL_m25': '/SUSY_ZH_ZToAll_HToAATo4B_M-25_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'ZHToAATo4B_mc2017UL_m30': '/SUSY_ZH_ZToAll_HToAATo4B_M-30_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'ZHToAATo4B_mc2017UL_m35': '/SUSY_ZH_ZToAll_HToAATo4B_M-35_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#    , 'ZHToAATo4B_mc2017UL_m40': '/SUSY_ZH_ZToAll_HToAATo4B_M-40_TuneCP5_13TeV_madgraph_pythia8/RunIISummer20UL17MiniAODv2-106X_mc2017_realistic_v9-v2/MINIAODSIM'
#
#    , 'WZToLNu4B-1j-g1': '/privateMCProductionLHEGEN/acrobert-eventMiniAODv2_WZToLNu4B-1j-g1_NFiles-499_2023080915081691587279-dd00e8e5190104a7aafdc4fba9805483/USER'
#
#}
#
#
#t3dirs = {'WZToLNuQQ_mc2017UL': 'WZ', 'ZJets_HT_800toInf_mc2017UL': 'ZJets', 'ZGammaToJJ_mc2017UL': 'ZGamma', 'ZZTo2Q2L_mc2017UL': 'ZZ', 'ZZTo4B01j_mc2017UL': 'ZZ', 'ZJets_HT_600to800_mc2017UL': 'ZJets', 'WZ_mc2017UL': 'WZ', 'TTJets_mc2017UL':'TTJets', 'WJetsToLNu_mc2017UL': 'WJets', 'sWJetsToLNu_mc2017UL': 'WJets', 'QCD_BGen_HT_700to1000_mc2017UL': 'QCD', 'QCD_BGen_HT_1000to1500_mc2017UL': 'QCD', 'QCD_BGen_HT_1500to2000_mc2017UL': 'QCD', 'ZJets_HT_800toInf_F17mc': 'ZJets', 'ZJets_HT_800toInf_up2018UL': 'ZJets', 'ZHToAATo4B_mc2017UL_m12': 'ZHToAA', 'ZHToAATo4B_mc2017UL_m15': 'ZHToAA', 'ZHToAATo4B_mc2017UL_m20': 'ZHToAA', 'ZHToAATo4B_mc2017UL_m25': 'ZHToAA', 'ZHToAATo4B_mc2017UL_m30': 'ZHToAA', 'ZHToAATo4B_mc2017UL_m35': 'ZHToAA', 'ZHToAATo4B_mc2017UL_m40': 'ZHToAA', 'WZToLNu4B-1j-g1':'WZToLNu4B-1j-g1'}
#

datasets = datasets
t3dirs = t3dirs

class ParquetDataset:
    def __init__(self, filename,cols=None):
        import pyarrow.parquet as pq
        self.parquet = pq.ParquetFile(filename)
        self.cols = cols # if None, read all columns
        #self.cols = ['X_jets.list.item.list.item.list.item','y'] 
    def __getitem__(self, index):
        data = self.parquet.read_row_group(index, columns=self.cols).to_pydict()

        iters = self.cols if self.cols != None else names
        for name in iters:
            if name == 'idx':
                data['idx'] = np.int64(data['idx'][0])
            else:
                try:
                    data[name] = np.float64(data[name][0]) # everything else is doubles
                except KeyError:
                    pass
                
        return dict(data)
    def __len__(self):
        return self.parquet.num_row_groups

def round_to_pixel(pos):
    dst = 0.01726
    return dst * round(pos/dst)

class ParquetDatasetGNN:
    
    def __init__(self, filename, layer=None, knn=3):
        
        import pyarrow.parquet as pq
        self.filename = filename
        self.parquet = pq.ParquetFile(filename)
        self.layer = layer
        self.cols = ['RHgraph_nodes_img','PFTgraph_nodes_img','pt','y','bdt','wgt','eta','pu']
        self.knn = knn
        
    def __getitem__(self, index):

        data = self.parquet.read_row_group(index, columns=self.cols).to_pydict()

        data['pt'] =  torch.reshape(torch.Tensor(data['pt']).float() , (1,))
        data['y'] =   torch.reshape(torch.Tensor(data['y']).float()  , (1,))
        data['bdt'] = torch.reshape(torch.Tensor(data['bdt']).float(), (1,))
        data['wgt'] = torch.reshape(torch.Tensor(data['wgt']).float(), (1,))
        data['eta'] = torch.reshape(torch.Tensor(data['eta']).float(), (1,))
        data['pu'] =  torch.reshape(torch.Tensor(data['pu']).float()   , (1,))

        # make graph
        node_list = []
        #node_pos = []
        for i in range(len(data['RHgraph_nodes_img'][0][0])):
            if data['RHgraph_nodes_img'][0][0][i][0] != 0 :
                xphi = round_to_pixel(data['RHgraph_nodes_img'][0][0][i][1])
                xeta = round_to_pixel(data['RHgraph_nodes_img'][0][0][i][2])
                node_list.append([1., data['RHgraph_nodes_img'][0][0][i][0], xphi, xeta, 0., 0., 0.])
                #node_pos.append( [xphi, xeta] ) 
        for i in range(len(data['PFTgraph_nodes_img'][0][0])):
            node_list.append([2.] + list(data['PFTgraph_nodes_img'][0][0][i][0:3]) + list(data['PFTgraph_nodes_img'][0][0][i][3:6]))
            #node_pos.append( list(data['PFTgraph_nodes_img'][0][0][i][1:3]) )

        edge_list = [[], []]
        edge_wgt = []
        if self.knn > 0:
            for i in range(len(node_list)):
                closest = []
                for j in range(len(node_list)):
                    if i != j:
                        #print(node_list[i], node_list[j])
                        closest.append( [ j, find_dist(node_list[i], node_list[j]) ] )

                #closest.sort(key = keyfun) 
                random.shuffle(closest)
                
                for j in range(min(self.knn, len(node_list) - 1)):
                    try:
                        if closest[j][1] > 0.:
                            edge_list[0].append(i)
                            edge_list[1].append(closest[j][0])
                            edge_wgt.append([ closest[j][1] ])
                    except:
                        print(j, len(closest), len(node_list))
                        raise

            data['G'] = geo.data.Data(x=torch.tensor(node_list), edge_index=torch.tensor(edge_list), edge_attr=torch.tensor(edge_wgt)) #pos=node_pos)
        else:
            data['G'] = geo.data.Data(x=torch.tensor(node_list))
        del data['RHgraph_nodes_img']
        del data['PFTgraph_nodes_img']
        
        return dict(data)

    def __len__(self):
        return self.parquet.num_row_groups
        
def keyfun(element):
    return element[1]

def find_dist(node_1, node_2):
    return np.sqrt( (node_1[3] - node_2[3])**2 + (node_1[2] - node_2[2])**2 ) 

class ParquetDatasetCNN:

    def __init__(self, filename,layer=None):

        import pyarrow.parquet as pq
        self.parquet = pq.ParquetFile(filename)
        self.layer = layer
        self.cols = ['pt','y','bdt','wgt','eta','pu','X_RH_energyT', 'X_RH_energyZ', 'X_PFT_pT', 'X_PFT_d0', 'X_PFT_z0']
        self.norms = [40., 20., 15., 6., 20.]

    def __getitem__(self, index):
        data = self.parquet.read_row_group(index, columns=self.cols).to_pydict()

        data['pt'] = np.float32(data['pt'])        
        data['y'] = np.int64(data['y'][0])
        data['bdt'] = np.float32(data['bdt'])
        data['wgt'] = np.float64(data['wgt'])
        data['eta'] = np.float64(data['eta'])
        data['pu'] = np.float64(data['pu'])
        
        keys = ['X_RH_energyT', 'X_RH_energyZ', 'X_PFT_pT', 'X_PFT_d0', 'X_PFT_z0']
        X = [0,0,0,0,0]
        if self.layer == None:
            for i in range(len(X)):
                X[i] = np.float32(data[keys[i]][0][0])/self.norms[i] # to get pixel intensities roughly within [0, 1]
                del data[keys[i]]
            data['X'] = np.array(X)
        else:
            data['X'] = np.float32(data[keys[self.layer]][0])/self.norms[self.layer] # to get pixel intensities roughly within [0, 1]

        data['X'][data['X'] < 1.e-3] = 0.
        
        return dict(data)
    
    def __len__(self):
        return self.parquet.num_row_groups

class ParquetToNumpy:
    def __init__(self, type, nfiles, fname, cols=None, nevts = -1):
        
        start = time.time()

        colstr = ''
        if cols != None:
            for col in cols:
                colstr += col

        objloc = "obj/"+type+"_"+fname.replace('/','')+"_"+str(nevts)+"_"+colstr+"_dict.pkl"
        print(objloc)
        
        try:
            self.datadict = pickle.load( open( objloc, "rb" ) )
            if self.datadict != None:
                return
        except IOError:
            pass
        except EOFError:
            pass

        self.datadict = {}

        for i in range(nfiles):
            if nfiles > 1:
                parquet = ParquetDataset(fname+str(i))
            else:
                parquet = ParquetDataset(fname)
                
            if nevts == -1:
                nevts = len(parquet)
            print('>> Initiating file {} - location {}, length {}'.format(i, fname+str(i), len(parquet)))

            if cols == None:
                iters = names
            else:
                iters = cols
                
            for j in range(nevts):
                if j % 5000 == 0:
                    print(">> Running %d - nevts: %d time elapsed: %f s"%(i, j, time.time()-start))
                 
                row = parquet[j]
   
                for name in iters:
                    if name not in list(self.datadict.keys()):
                        self.datadict[name] = []
                                
                    self.datadict[name].append(row[name])
                    
                if i == 0 and j == 1:
                    print('>> Datadict at i == 0, j == 1:', self.datadict)

        for name in iters:
            try:
                self.datadict[name] = np.stack(self.datadict[name], axis=0)
            except KeyError:
                pass

        pickle.dump( self.datadict, open( objloc, "wb" ) )

    def __getitem__(self, name):
        return self.datadict[name]

    def getDict(self):
        return self.datadict

class timer:
    def __init__(self):
        self.start = time.time()

    def time(self):
        return time.time() - self.start

class obj_weights:

    def __init__(self):
        with open('/uscms/home/acrobert/nobackup/gnn_classifier/pteta_ratios.yaml','r') as file:
            self.wgt_dct = yaml.safe_load(file)
        self.ptbins = self.wgt_dct['ptbins']
        self.etabins = self.wgt_dct['etabins']
        self.nptbins = len(self.ptbins) - 1
        self.netabins = len(self.etabins) - 1

    def get_weights_torch(self,y,pt,eta,wgt):
        
        ly = len(y)
        wv = torch.zeros([ly,1],dtype=torch.float32).cuda()
        for i in range(ly):
            yi = int(y[i].item())
            if yi == 1:
                wv[i][0] = wgt[i][0]
            elif yi == 0:
                pti = pt[i].item()
                eti = eta[i].item()
                
                ipt = None
                for j in range(self.nptbins):
                    if pti >= self.ptbins[j] and pti < self.ptbins[j+1]:
                        ipt = j
                        break

                iet = None
                for j in range(self.netabins):
                    if eti >= self.etabins[j] and eti < self.etabins[j+1]:
                        iet = j
                        break
                    
                if ipt == None or iet == None:
                    print(pti, eti, ipt, iet)
                wv[i][0] = wgt[i][0] * self.wgt_dct[ipt][iet]

        return wv

    def get_weights_np(self,y,pt,eta,wgt):

        if y == 1:
            return wgt
        else:

            ipt = None
            for i in range(self.nptbins):
                if pt >= self.ptbins[i] and pt < self.ptbins[i+1]:
                    ipt = i
                    break
                    
            iet = None
            for i in range(self.netabins):
                if eta >= self.etabins[i] and eta < self.etabins[i+1]:
                    iet = i
                    break
                    
            wgt_adj = self.wgt_dct[ipt][iet]
            return wgt * wgt_adj
    
def imgplot(data,name,ind,indivs=False,ratio=False,ds2=None,dim=160):
    ld = len(data)
    hist = r.TH2F('data '+name,name+' (n='+str(ld/1000)+'k); Relative i#phi; Relative i#eta; E [GeV]',dim,0.,float(dim),dim,0.,float(dim))

    for i in range(dim):
        for j in range(dim):
            if not ratio:
                bin_val = float(sum([ data[k][ind][i][j] for k in range(ld) ])) / ld
            else:
                bin_p = float(sum([ data[k][ind][i][j] for k in range(ld) ])) / ld
                bin_f = float(sum([ ds2[k][ind][i][j] for k in range(ld) ])) / ld
                if bin_f == 0.:
                    bin_val = 0.
                else:
                    bin_val = bin_p/bin_f

            hist.Fill(j,i,bin_val)

    hist.SetStats(0)
    r.gStyle.SetNumberContours(100)

    c = r.TCanvas('layer '+name)
    c.SetRightMargin(0.15)
    c.SetLogz()
    hist.Draw('colz')
    c.Write()

    del c

    if indivs:

        for i in range(5):
            indnm = name+' '+str(i)
            hist_indiv = r.TH2F('data_'+indnm,indnm+'; Relative i#phi; Relative i#eta; E [GeV]',dim,0.,float(dim),dim,0.,float(dim))
            for j in range(dim):
                for k in range(dim):
                    if not ratio:
                        hist_indiv.Fill(k,j,float( data[i][ind][j][k] ))
                    else:
                        if float( ds2[i][ind][j][k] ) == 0.:
                            hist_indiv.Fill(k,j,0.)
                        else:
                            hist_indiv.Fill(k,j,float( data[i][ind][j][k] )/float( ds2[i][ind][j][k] ))

            hist_indiv.SetStats(0)
            cx = r.TCanvas("indiv_layer_"+indnm)
            cx.SetRightMargin(0.15)
            cx.SetLogz()
            hist_indiv.Draw('colz')
            cx.Write()
            
            del cx
            del hist_indiv

    return hist

class imgplot_dyn:
    def __init__(self,ld,name,resc,zax,dim=160):
        r.gStyle.SetNumberContours(100)
        self.n = str(int(round(ld/1000)))+'k' if ld >= 1000 else str(int(round(ld)))
        self.hist = r.TH2F('data '+name,name+' (n='+self.n+'); Relative i#phi; Relative i#eta; '+zax,dim,0.,float(dim),dim,0.,float(dim))
        self.hist.SetStats(0)
        self.resc = resc
        self.dim = dim
        self.name = name
        #self.c = r.TCanvas('layer '+name)
        #self.c.SetRightMargin(0.15)
        #self.c.SetLogz()

    def fill(self,X):
        for i in range(self.dim):
            for j in range(self.dim):
                if X[i][j] != 0:
                    self.hist.Fill(j,i,X[i][j]/float(self.resc))

    def ret(self):
        #self.hist.Draw('colz')
        #self.c.Write()
        return self.hist, self.name

def drawlayerhist(hist,name,dim=160):
    c = r.TCanvas('layer '+name)
    c.SetRightMargin(0.15)
    c.SetLogz()
    hist.Draw('colz')

    mini = 1000000.
    maxi = 0.
    for i in range(dim):
        for j in range(dim):
            val = hist.GetBinContent(i,j)
            if val != 0:
                if val > maxi:
                    maxi = val
                if val < mini:
                    mini = val

    hist.SetMinimum(mini)
    hist.SetMaximum(maxi)
    c.Write()
    
    del c

def combine_parquet_files(input_folder, target_path, npqs_per_file=-1):
    import pyarrow as pa
    import pyarrow.parquet as pq
    
    outlist = []

    if npqs_per_file == -1:
        npqs_per_f
    made_writer = False

    ldir = os.listdir(input_folder)
    ld = len(ldir)

    noutfiles = int(math.ceil(float(ld) / float(npqs_per_file))) 
    print(noutfiles)

    for i in range(noutfiles):
        for j in range(npqs_per_file):
            idx = i*npqs_per_file + j
            if idx >= ld:
                break
            file_name = ldir[idx]
            print(file_name)
            
            pqds = ParquetDataset(os.path.join(input_folder, file_name))
            for k in range(len(pqds)): 
                dct = pqds[k]            
                pqdata = [pa.array([d]) if np.isscalar(d) or type(d) == list else pa.array([d.tolist()]) for d in dct.values()]
                table = pa.Table.from_arrays(pqdata, dct.keys())
    
                if not made_writer:
                    
                    writer = pq.ParquetWriter(target_path+'.'+str(i), table.schema, compression='snappy')
                    outlist.append(target_path+'.'+str(i))
                    made_writer = True
        
                writer.write_table(table)
                                
            del pqds
            
        writer.close()
        if idx >= ld:
            break
        made_writer = False
        del writer

    return outlist



def combine_parquet_files_evts(input_folder, target_path, nevts_per_file=-1, n_out_files=-1):
    import pyarrow as pa
    import pyarrow.parquet as pq
    
    outlist = []

    if nevts_per_file == -1:
        nevts_per_file = 10000000000

    made_writer = False

    ldir = os.listdir(input_folder)
    ld = len(ldir)

    for i in range(n_out_files):
        for j in range(nevts_per_file):

            pqds = ParquetDataset(os.path.join(input_folder, file_name))
            for k in range(len(pqds)):
                dct = pqds[k]

                pqdata = [pa.array([d]) if np.isscalar(d) or type(d) == list else pa.array([d.tolist()]) for d in dct.values()]
                table = pa.Table.from_arrays(pqdata, dct.keys())

                if not made_writer:

                    writer = pq.ParquetWriter(target_path+'.'+str(i), table.schema, compression='snappy')
                    outlist.append(target_path+'.'+str(i))
                    made_writer = True

                writer.write_table(table)

            del pqds

        writer.close()
        if idx >= ld:
            break
        made_writer = False

    return outlist

def check_nan(tensor, idxs):
    try:
        size = list(tensor.size())
    except:
        size = list(tensor.shape)

    lst = []

    if len(size) > 1:
        for i in range(size[0]):
            check = check_nan(tensor[i], idxs+[i])
            if check != []:
                lst += check
    else:
        for i in range(size[0]):
            if tensor[i] != tensor[i]:
                lst += [(idxs+[i],tensor[i].item())]

    return lst

def checknan(tensor):
    return check_nan(tensor, [])

def check_inf(tensor, idxs):
    try:
        size = list(tensor.size())
    except:
        size = list(tensor.shape)

    lst = []

    if len(size) > 1:
        for i in range(size[0]):
            check = check_inf(tensor[i], idxs+[i])
            if check != []:
                lst += check
    else:
        for i in range(size[0]):
            if math.isinf(tensor[i]):
                lst += [(idxs+[i],tensor[i].item())]

    return lst

def checkinf(tensor):
    return check_inf(tensor, [])

def xglob_for_ftype(date, run, dset, runname, ftype):

    datasets = { 2: {'dp': 'DiPhotonJets_MGG-80toInf_13TeV_amcatnloFXFX_pythia8',
                     'gj1': 'GJet_Pt-20to40_DoubleEMEnriched_MGG-80toInf_TuneCP5_13TeV_Pythia8',
                     'gj2': 'GJet_Pt-40toInf_DoubleEMEnriched_MGG-80toInf_TuneCP5_13TeV_Pythia8',
                     'gj3': 'GJet_Pt-20toInf_DoubleEMEnriched_MGG-40to80_TuneCP5_13TeV_Pythia8'},
                 3: {'dp1': 'DoublePhoton_FlatPt-5To500_13p6TeV',
                     'dp2': 'DoublePhoton_FlatPt-500To1000_13p6TeV',
                     'dp3': 'DoublePhoton_FlatPt-1000To1500_13p6TeV',
                     'dp4': 'DoublePhoton_FlatPt-1500To3000_13p6TeV',
                     'dp5': 'DoublePhoton_FlatPt-3000To4000_13p6TeV',
                     'dp6': 'DoublePhoton_FlatPt-4000To5000_13p6TeV',
                     'gj1': 'GJet_Pt-10to40_DoubleEMEnriched_TuneCP5_13p6TeV_pythia8',
                     'gj2': 'GJet_Pt-40toInf_DoubleEMEnriched_TuneCP5_13p6TeV_pythia8' } }

    for i in range(1, 18):
        datasets[2]['dt'+str(i)] = 'DoubleEG'

    t3 = 'root://cmsdata.phys.cmu.edu'

    cmd = 'xrdfs {} ls /store/user/acrobert/egiddata/{}/{}/{}'.format(t3, date, datasets[run][dset], runname)
    os.system(cmd +' > tmpf')
    out = open('tmpf', 'r').read().split('\n')
    #out = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')                                                                                                                

    while not check_for_ftype(out, ftype):

        new_out = []
        for fld in out:
            if fld != '':
                #cmd = ['xrdfs', t3, 'ls', fld]                                                                                                                                                          
                #new_out += subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')                                                                                               
                cmd = 'xrdfs {} ls {}'.format(t3, fld)
                os.system(cmd + ' > tmpf')
                new_out += open('tmpf', 'r').read().split('\n')

        out = new_out

    ret = []
    for f in out:
        if '.root' in f:
            ret.append(f)

    return ret

def xglob_onedir(path):

    #nanopath = '/store/user/acrobert/Znano/{}'.format(date)
    t3 = 'root://cmsdata.phys.cmu.edu/'

    cmd = 'xrdfs {} ls {}'.format(t3, path)
    os.system(cmd +' > tmpf')
    out = open('tmpf', 'r').read().split('\n')
    #out = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')                                                                                                                
    
    out.remove('')
    return out

def check_for_ftype(lst, ftype):

    bl = False
    for l in lst:
        if ftype in l:
            bl = True

    return bl

def calc_xs(dataset):

    os.system('curl https://raw.githubusercontent.com/cms-sw/genproductions/master/Utilities/calculateXSectionAndFilterEfficiency/genXsec_cfg.py -o ana.py > /dev/null 2>&1')
    os.system('dasgoclient -query="file dataset={}" > dastmp.txt 2>&1'.format(dataset))

    with open('dastmp.txt','r') as dasout:
        daslines = dasout.readlines()
        print('N files: {}'.format(len(daslines)-1))
        xsarr = []
        errarr = []
        
        for i in range(min(10, len(daslines)-1)):
            thisfile = daslines[i].strip('\n')
            print('file: {}'.format(thisfile))
            os.system('cmsRun ana.py inputFiles="file:root://cms-xrd-global.cern.ch/{}" maxEvents=-1 > xstmp.txt 2>&1'.format(thisfile))

            xs = -1.

            with open('xstmp.txt','r') as xsout:
                xslines = xsout.readlines()
                
                for j in range(len(xslines)):
                    if 'After filter: final cross section' in xslines[j]:
                        eles = xslines[j].strip('\n').split(' ')
                        xsarr.append(float(eles[6]))
                        errarr.append(float(eles[8]))

    xsarr = np.array(xsarr)
    print('dataset xs:', xsarr.mean(), xsarr.std())
    os.system('rm dastmp.txt && rm xstmp.txt')

def match_with_jets(jets, Bs4V, jetradii=[0.4, 0.8, 1.5], radbuffer=0.2, exclusive=False):

    nj = len(jets)
    has_jet = [(ak.num(jets[i]) >= 1) for i in range(nj)]
    bjetmat = [ak.cartesian([Bs4V, jets[i]], axis=1, nested=True) for i in range(nj)]
    jetdR = [bjetmat[i]['0'].delta_r(bjetmat[i]['1']) for i in range(nj)]
    evtbl = [(jetdR[i] < jetradii[i]+radbuffer) & (jetdR[i] == ak.min(jetdR[i], axis=-1)) for i in range(nj)]
    bs_per_jet = [ak.sum(evtbl[i], axis=1) for i in range(nj)]

    #print(evtbl[0].type, bs_per_jet[0].type)                                                                                                                                                                      
    return evtbl, bs_per_jet

def match_with_SVjets(jets, SV, Bs4V, jetradii=[0.4, 0.8, 1.5], radbuffer=0.2, exclusive=False):

    nB = len(Bs4V[0])
    nj = len(jets)

    has_jet = [(ak.num(jets[i]) >= 1) for i  in range(nj)]
    has_SV = ak.num(SV) >= 1

    jetdR = [ak.Array([Bs4V.mask[has_jet[i]][:,j].delta_r(jets[i].mask[has_jet[i]]) for j in range(nB)]) for i in range(nj)]
    evtbl = [(jetdR[i] < jetradii[i]+radbuffer) & (jetdR[i] == ak.min(jetdR[i], axis=-1)) for i in range(nj)]
    bs_per_jet = [ak.sum(evtbl[i], axis=0) for i in range(nj)]

    SVdR = ak.Array([Bs4V.mask[has_SV][:,j].delta_r(SV.mask[has_SV]) for j in range(nB)])
    evtblSV = (SVdR < jetradii[0]+radbuffer) & (SVdR == ak.min(SVdR, axis=-1))
    bs_per_SV = ak.sum(evtblSV, axis=0)

    return evtbl, bs_per_jet, evtblSV, bs_per_SV

def match_with_any(jets, SV, saj, Bs4V, jetradii=[0.4, 0.8, 1.5], radbuffer=0.2, exclusive=False):

    nB = len(Bs4V[0])
    nj = len(jets)

    has_jet = [(ak.num(jets[i]) >= 1) for i in range(nj)]
    has_SV = ak.num(SV) >= 1
    has_saj = ak.num(saj) > 1

    jetdR = [ak.Array([Bs4V.mask[has_jet[i]][:,j].delta_r(jets[i].mask[has_jet[i]]) for j in range(nB)]) for i in range(nj)]
    evtbl = [(jetdR[i] < jetradii[i]+radbuffer) & (jetdR[i] == ak.min(jetdR[i], axis=-1)) for i in range(nj)]
    bs_per_jet = [ak.sum(evtbl[i], axis=0) for i in range(nj)]

    SVdR = ak.Array([Bs4V.mask[has_SV][:,j].delta_r(SV.mask[has_SV]) for j in range(nB)])
    evtblSV = (SVdR < jetradii[0]+radbuffer) & (SVdR == ak.min(SVdR, axis=-1))
    bs_per_SV = ak.sum(evtblSV, axis=0)

    sajdR = ak.Array([Bs4V.mask[has_saj][:,j].delta_r(saj.mask[has_saj]) for j in range(nB)])
    evtblsaj = (sajdR < jetradii[0]+radbuffer) & (sajdR == ak.min(sajdR, axis=-1))
    bs_per_saj = ak.sum(evtblsaj, axis=0)

    return evtbl, bs_per_jet, evtblSV, bs_per_SV, evtblsaj, bs_per_saj

def object_combiner(obj_list, redundant_rad = 0.4):

    for obj in obj_list:
        if 'mass' not in obj.fields:
            obj['mass'] = ak.zeros_like(obj.pt, dtype=float)

    list4V = [ak.zip({"pt": obj.pt, "eta": obj.eta, "phi": obj.phi, "mass": obj.mass}, with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior) for obj in obj_list]
    listidx = [ak.local_index(i4V, axis=1) for i4V in list4V]

    finlist4V = [list4V[0]]
    finlistidx = [listidx[0]]
    finlisttype = [ak.zeros_like(list4V[0].pt)]

    for i in range(1, len(list4V)):
        before = finlist4V[0] if i == 1 else ak.concatenate(finlist4V, axis=1)
        objmat = ak.cartesian([list4V[i], before], axis=1, nested=True)
        matdR = objmat['0'].delta_r(objmat['1'])
        redundant_obj = ak.where((ak.num(before) > 0), (ak.min(matdR, axis=2) > 0.37), ak.full_like(list4V[i].pt, True, dtype=bool))

        finlist4V.append(list4V[i][redundant_obj])
        finlistidx.append(listidx[i][redundant_obj])
        finlisttype.append(ak.full_like(finlist4V[i].pt, i))

    obj_4V = ak.concatenate(finlist4V, axis=1)
    obj_idx = ak.concatenate(finlistidx, axis=1)
    obj_type = ak.concatenate(finlisttype, axis=1)

    return obj_4V, obj_idx, obj_type

def match_with_4V(obj_4V, Bs4V, jetrad=0.4, radbuffer=0.2):

    has_obj = ak.num(obj_4V) >= 1
    bobjmat = ak.cartesian([Bs4V, obj_4V], axis=1, nested=True)

    bobjdR = bobjmat['0'].delta_r(bobjmat['1'])
    evtbl = (bobjdR < jetrad+radbuffer) & (bobjdR == ak.min(bobjdR, axis=-1))
    bs_per_obj = ak.sum(evtbl, axis=1)

    #print(bobjdR[0], evtbl[0])

    return evtbl, bs_per_obj

def match_4V_with_4V(obj1_4V, obj2_4V, matchrad=0.2):

    objmat = ak.cartesian([obj1_4V, obj2_4V], axis=1, nested=True)

    objdR = objmat['0'].delta_r(objmat['1'])
    print('mins', len(obj1_4V[0]), len(obj2_4V[0]))
    print(ak.min(objdR, axis=0)[0].type)
    print(ak.min(objdR, axis=1)[0].type)
    print(ak.min(objdR, axis=-1)[0].type)
    print((objdR < matchrad)[0].to_numpy().shape)
    print(objdR[0])
    print(ak.min(objdR, axis=1)[0])
    
    
    #print((objdR == ak.min(objdR, axis=1))[0].to_numpy().shape)
    print(ak.num(obj1_4V), ak.num(obj2_4V))
    print((objdR < matchrad)[0].to_numpy().shape)
    print((objdR == ak.min(objdR, axis=-1))[0].to_numpy().shape)
    print(objdR[0].to_numpy().shape, ak.min(objdR, axis=1)[0].to_numpy().shape)
    print(objdR[0], ak.min(objdR, axis=1)[0])
    print((objdR[0].to_numpy().T == ak.min(objdR, axis=1)[0].to_numpy()))
    
    evtmatch = (objdR < matchrad) & (objdR == ak.min(objdR, axis=-1)) & (objdR == ak.min(objdR, axis=1))

    return evtmatch

def object_grouper(obj_4V, rad=0.4):

    #bjmat = ak.cartesian([obj_4V, obj_4V], axis=1, nested=True)
    #Rmat = objmat['0'].delta_r(objmat['1']) # obv we only care about 1/2 of off-diagonals

    #atchable = (dRmat < rad)
    
    pairs = ak.Array([[0,0,0,1,1,2],
                      [1,2,3,2,3,3]])

    dRs = obj_4V[:, pairs[0]].delta_r(obj_4V[:, pairs[1]])
    #print(dRs[0:2])
    npairs = ak.sum(dRs < rad, axis=1)
    #print(npairs[0:8])

    #print(ak.sum(dRs < rad, axis=1))
    #print(ak.min(dRs, axis=1))
    #print(ak.argmin(dRs, axis=1))

    print(pairs)

    print(ak.argmin(dRs, axis=1))
    pairloc = ak.argmin(dRs, axis=1)
    print(pairs[0][pairloc])
    print(pairs[1][pairloc])

    #dphi = thesebs.phi - thesepx.phi
    #deta = thesebs.eta - thesepx.eta
    #
    #dphi_fixed = ak.concatenate((dphi[abs(dphi) <= np.pi], dphi[dphi > np.pi] - 2*np.pi, dphi[dphi < -np.pi] + 2*np.pi), axis=1)
    #deta_fixed = ak.concatenate((deta[abs(dphi) <= np.pi], deta[dphi > np.pi], deta[dphi < -np.pi]), axis=1)
    #pxpt_fixed = ak.concatenate((thesepx[self.fields[k]][abs(dphi) <= np.pi], thesepx[self.fields[k]][dphi > np.pi], thesepx[self.fields[k]][dphi < -np.pi]), axis=1)

    hapair = ak.min(dRs, axis=1) < rad
    print(obj_4V.phi[:,pairs[0][pairloc]])
    print(obj_4V.phi[:,pairs[1][pairloc]])
    obj_pair_1 = (obj_4V[:,pairs[0][pairloc]] + obj_4V[:,pairs[1][pairloc]])
    print(obj_pair_1.phi)
    #print(pairlocobj.eta)
    #firstreplace = ak.where(haspair, , obj4V)

    center = (obj_4V[:, 0] + obj_4V[:, 1] + obj_4V[:, 2] + obj_4V[:, 3])
    cdRs = obj_4V.delta_r(center)
    print(center.phi[0:3])
    print(center.eta[0:3])
    print(cdRs[0:2])

    print(ak.all(cdRs < 0.4, axis=1)[0:10])
    print(ak.where(ak.all(cdRs < 0.4, axis=1)))
    
    hasquad = ak.all(cdRs < 0.4, axis=1)
    groupreplace = ak.where(hasquad, center, obj_4V)
    grouptype = ak.where(hasquad, ak.Array([[4]]), ak.Array([[1,1,1,1]]))

    print(center.fields)
    trioseeds = [obj_4V[:, 0] + obj_4V[:, 1] + obj_4V[:, 2], obj_4V[:, 0] + obj_4V[:, 1] + obj_4V[:, 3], obj_4V[:, 0] + obj_4V[:, 3] + obj_4V[:, 2], obj_4V[:, 3] + obj_4V[:, 1] + obj_4V[:, 2]]
    triodRs = [obj_4V.delta_r(trio) for trio in trioseeds]

    hastrio0 = ak.sum(triodRs[0] < 0.4, axis=1) == 3
    hastrio1 = ak.sum(triodRs[1] < 0.4, axis=1) == 3
    hastrio2 = ak.sum(triodRs[2] < 0.4, axis=1) == 3
    hastrio3 = ak.sum(triodRs[3] < 0.4, axis=1) == 3

    print(ak.sum(hastrio0 & hastrio1))
    print(ak.sum(hastrio0 & hastrio2))
    print(ak.sum(hastrio0 & hastrio3))
    print(ak.sum(hastrio1 & hastrio2))
    print(ak.sum(hastrio1 & hastrio3))
    print(ak.sum(hastrio2 & hastrio3))
    print(ak.sum(hastrio0 | hastrio1 | hastrio2 | hastrio3))

    print(obj_4V.energy[hastrio0 & hastrio1][0:2])
    print(obj_4V.pt[hastrio0 & hastrio1][0:2])
    print(obj_4V.eta[hastrio0 & hastrio1][0:2])
    print(obj_4V.phi[hastrio0 & hastrio1][0:2])
    print(trioseeds[0][hastrio0 & hastrio1].eta[0:2], trioseeds[1][hastrio0 & hastrio1].eta[0:2])
    print(trioseeds[0][hastrio0 & hastrio1].phi[0:2], trioseeds[1][hastrio0 & hastrio1].phi[0:2])
    print(triodRs[0][hastrio0 & hastrio1][0:2], triodRs[1][hastrio0 & hastrio1][0:2])

    #print(groupreplace.phi[459:461])
    print(groupreplace.fields)
    print(grouptype[460: 471])

    drs2 = ak.min(ak.Array([(obj_4V[:,0].phi + obj_4V[:,1].phi)/2, (obj_4V[:,0].phi+2*np.pi + obj_4V[:,1].phi)/2, (obj_4V[:,0].phi + obj_4V[:,1].phi+2*np.pi)/2]), axis=0)
    print(ak.Array([(obj_4V[:,0].phi + obj_4V[:,1].phi)/2, (obj_4V[:,0].phi+2*np.pi + obj_4V[:,1].phi)/2, (obj_4V[:,0].phi + obj_4V[:,1].phi+2*np.pi)/2])[:, 0:2])
    print(obj_4V[:,0].phi[0:4])    
    print(obj_4V[:,1].phi[0:4])
    print(drs2[0:4])

def k_means_clustering_k2(obj_4V):

    init_groups_0 = [[0, 1], [2, 3]]

    grouped_objs = obj_4V[:, init_groups_0]

    for i in range(len(obj_4V)):
       
        thgroup = grouped_objs[i]

        reassigning = True
        #while reassigning:

        #   center_eta_g0 = sum([thisgroup[j].eta for j in range(len(

        center_eta_g0 = ak.sum(grouped_objs[:,0], axis=1).eta
        center_phi_g0 = ak.sum(grouped_objs[:,0], axis=1).phi
        center_eta_g1 = ak.sum(grouped_objs[:,1], axis=1).eta
        center_phi_g1 = ak.sum(grouped_objs[:,1], axis=1).phi

        print(center_eta_g0[0:3], center_phi_g0[0:3])
        print(center_eta_g1[0:3], center_phi_g1[0:3])

        reassigning = False

def create_jets(tagnano, btagthresh):

    jets = tagnano.Jet[(tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4) & (tagnano.Jet.puId >= 4)]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]
    
    #bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
    #                with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    #bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
    #                 with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    return bbjets, bjets, nonjets

def addmeanwidth(i, ax, counts, bins, color, density=False):

    binwidth = bins[1] - bins[0]
    mean = np.sum((bins[:-1] + binwidth/2) * (counts)) / np.sum(counts)

    if density:
        densityvals = counts/np.sum(counts * (binwidth))
        maxval = np.max(densityvals)
    else:
        maxval = np.max(counts)

    stdev = np.sqrt(np.sum(counts * (bins[:-1] + binwidth/2 - mean)**2 ) / (np.sum(counts)-1) )
    fwhm = 2*np.sqrt(2*(math.log(2)))*stdev

    x1 = [mean, mean]
    y1 = [0., maxval]
    x2 = [mean - fwhm/2, mean + fwhm/2]
    y2 = [0.1*maxval + 0.03*maxval*i, 0.1*maxval + 0.03*maxval*i]
    ax.plot(x2, y2, linestyle='--', color=color)
    ax.plot(x1, y1, linestyle='--', color=color)

def onedim_plot(hists, labels, xlab, ylab, title, fname, key, addmean=False, density=False, yerrs=None):
    
     fig, ax = plt.subplots(figsize=(16, 12))
     mplhep.histplot(hists, label=labels, ax=ax, density=density, yerr=yerrs)
     if addmean:
         for i in range(len(hists)):
             c, b = hists[i].to_numpy()
             addmeanwidth(i, ax, c, b, f'C{i}', density=density)
     ax.set_title(title)
     ax.set_xlabel(xlab)
     ax.set_ylabel(ylab)
     ax.legend()
     print(f'plots/hist_{fname}_{key}.png')
     plt.savefig(f'plots/hist_{fname}_{key}.png')
     plt.close(fig)

def match_pfcs(tagnano, jets):

    pfcs_4V = ak.zip({"pt": tagnano.map.PFCands_pT, "eta": tagnano.map.PFCands_eta, "phi": tagnano.map.PFCands_phi, "energy": tagnano.map.PFCands_E},
                            with_name="PtEtaPhiELorentzVector", behavior=vector.behavior)
    
    pfc_to_jet_matches, pfcs_per_jet = match_with_4V(jets, pfcs_4V, radbuffer=0.)
    pfc_in_jet = ak.any(pfc_to_jet_matches, axis=2)

    pfcd0 = tagnano.map.PFCands_d0[pfc_in_jet]
    pfcz0 = tagnano.map.PFCands_z0[pfc_in_jet]
    pfcpt = tagnano.map.PFCands_pT[pfc_in_jet]
    pfcq = tagnano.map.PFCands_Q[pfc_in_jet]

    return pfcpt, pfcd0, pfcz0, pfcq
    
def jetcharge_allpfc(tagnano, jets, calc='ptsum'): # or qsum

    #if not all(ak.num(jets) == 1):
    #    jets = jets[ak.num(jets) == 1]
    #    pfcs = pfcs[ak.num(jets) == 1]
    
    pfcpt, pfcd0, pfcz0, pfcq = match_pfcs(tagnano, jets)

    if calc == 'ptsum':
        return ak.sum(pfcq*pfcpt / ak.sum(pfcpt, axis=1), axis=1)
    elif calc == 'qsum':
        return ak.sum(pfcq, axis=1)
    else:
        return None

def jet_charge(tagnano, jets, calc='ptsum', cut=None): # calc: ptsum or qsum; cut: None, dz10, dz500dxy50

    tagnano = tagnano[ak.num(jets) > 0]
    jets = jets[ak.num(jets) > 0]
    
    pfcpt, pfcd0, pfcz0, pfcq = match_pfcs(tagnano, jets)

    if cut == 'dz10':
        pfcd0 = pfcd0[abs(pfcz0) < 0.001]
        pfcz0 = pfcz0[abs(pfcz0) < 0.001]
        pfcpt = pfcpt[abs(pfcz0) < 0.001]
        pfcq = pfcq[abs(pfcz0) < 0.001]
    elif cut == 'dz500dxy50':
        dxyzcut = (abs(pfcz0) < 0.05) & (abs(pfcd0) > 0.005)

        pfcd0 = pfcd0[dxyzcut]
        pfcz0 = pfcz0[dxyzcut]
        pfcpt = pfcpt[dxyzcut]
        pfcq = pfcq[dxyzcut]

    
    if calc == 'ptsum':
        return ak.sum(pfcq*pfcpt / ak.sum(pfcpt, axis=1), axis=1)
    elif calc == 'qsum':
        return ak.sum(pfcq, axis=1)
    else:
        return None

    
