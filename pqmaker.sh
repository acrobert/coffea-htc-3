#!/bin/sh                                                                                                                                                                                                         

key=$1
ind=$2
nevts=$3
type=$4

export SCRAM_ARCH=slc7_amd64_gcc700
source /cvmfs/sft.cern.ch/lcg/views/LCG_104cuda/x86_64-centos7-gcc11-opt/setup.sh
export QT_QPA_PLATFORM=offscreen 

#xrdcp -s root://cmseos.fnal.gov//store/user/acrobert/condor/x509up_u57074 .
xrdcp -s root://cmseos.fnal.gov//store/user/acrobert/condor/coffeaToPq.py .
#xrdcp -s root://cmseos.fnal.gov//store/user/acrobert/condor/utils.py .
#xrdcp -s root://cmseos.fnal.gov//store/user/acrobert/condor/datadicts.py .
python3 coffeaToPq.py $key $ind $nevts $type

