import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
from datadicts import *
import uproot
import warnings
import fnmatch
from hist import Hist
import hist
from argparse import ArgumentParser
import os, psutil
import tracemalloc
import gc
from copy import deepcopy
import pickle as pkl
import pandas as pd

tmr = timer()
pucut = 4

btagthreshs = {'loose': 0.0490, 'medium': 0.2783, 'tight': 0.7100}

def postsel_2b_bin(tagnano, btagthresh):

    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    tagnano = tagnano[(ak.num(bjets[bjets.pt > 30.]) >= 2)]

    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
                    with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    zsys = bjet4V[:,0] + bjet4V[:,1]

    return tagnano, zsys

def postsel_b_bb_bin(tagnano, btagthresh):

    jets = tagnano.friendjet_noptcut[(tagnano.friendjet_noptcut.puId >= pucut) & (abs(tagnano.friendjet_noptcut.eta) < 2.4)]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    tagnano = tagnano[(ak.num(bbjets[bbjets.pt > 30.]) >= 1) & (ak.num(bjets[bjets.pt > 30.]) >= 1)]

    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]
    
    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    zsys = bjet4V[:,0] + bbjet4V[:,0]

    return tagnano, zsys

def postsel_3b_bin(tagnano, btagthresh):

    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    tagnano = tagnano[(ak.num(bjets) >= 3)]

    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    zsys = bjet4V[:, 0] + bjet4V[:, 1] + bjet4V[:, 2]

    return tagnano, zsys

def makeleps(tagnano, nleps):

    muons = tagnano.Muon
    muons = muons[(muons.pt > 15.) & (abs(muons.eta) < 2.4) & (muons.looseId)]
    muons = muons[(muons.pfRelIso04_all < 0.25) & (abs(muons.dz) < 0.2) & (abs(muons.dxy) < 0.05)]
    muons = muons.mask[ak.num(muons) == nleps]
    muons4V = ak.zip({"pt": muons.pt, "eta": muons.eta, "phi": muons.phi, "mass": muons.mass},
                    with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    eles = tagnano.Electron
    eles = eles[(eles.pt > 17) & (abs(eles.eta) < 2.4) & (eles.mvaFall17V2noIso_WP90)]
    eles = eles[(eles.pfRelIso03_all < 0.15) & (abs(eles.dz) < 0.2) & (abs(eles.dxy) < 0.05)]
    eles = eles.mask[ak.num(eles) == nleps]
    eles4V = ak.zip({"pt": eles.pt, "eta": eles.eta, "phi": eles.phi, "mass": eles.mass},
                    with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    if ak.sum(ak.num(eles)) > 0 and ak.sum(ak.num(muons)) > 0:
        leps4Vtemp = ak.concatenate(ak.Array([muons4V, eles4V]), axis=1)
    elif ak.sum(ak.num(eles)) > 0:
        leps4Vtemp = eles4V
    elif ak.sum(ak.num(muons)) > 0:
        leps4Vtemp = muons4V
    else:
        leps4Vtemp = eles4V
    leps4V = ak.zip({"pt": leps4Vtemp.pt, "eta": leps4Vtemp.eta, "phi": leps4Vtemp.phi, "mass": leps4Vtemp.mass},
                    with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    return leps4V

def lepsel(tagnano, nleps):

    leps4V = makeleps(tagnano, nleps)

    tagnano = tagnano[ak.fill_none(ak.num(leps4V), 0) == nleps]
    leps4V = leps4V[ak.fill_none(ak.num(leps4V), 0) == nleps]
    
    return tagnano, leps4V

def presel_hbb_0L(tagnano, hltkey):
    
    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]

    nleps = 0
    tagnano, leps4V = lepsel(tagnano, nleps)

    return tagnano

def presel_hbb_1L(tagnano, hltkey):

    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]

    nleps = 1
    tagnano, leps4V = lepsel(tagnano, nleps)

    return tagnano

def presel_hbb_2L(tagnano, hltkey):

    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]

    nleps = 2
    tagnano, leps4V = lepsel(tagnano, nleps)

    return tagnano

def selection_hbb_2L_2b(tagnano, hltkey, btagthresh):

    tagnano = presel_hbb_2L(tagnano, hltkey)
    tagnano, zsys = postsel_2b_bin(tagnano, btagthresh)

    leps4V = makeleps(tagnano, 2)

    lepsys = leps4V[:, 0] + leps4V[:, 1]
    tagnano = tagnano[(leps4V[:, 0].pt > 25.) & (lepsys.mass > 75.) & (lepsys.mass < 105.) & (lepsys.pt > 75.)]
    
    return tagnano

def selection_hbb_2L_3b(tagnano, hltkey, btagthresh):

    tagnano = presel_hbb_2L(tagnano, hltkey)
    tagnano, zsys = postsel_3b_bin(tagnano, btagthresh)

    leps4V = makeleps(tagnano, 2)

    lepsys = leps4V[:, 0] + leps4V[:, 1]
    tagnano = tagnano[(leps4V[:, 0].pt > 25.) & (lepsys.mass > 75.) & (lepsys.mass < 105.) & (lepsys.pt > 75.)]

    return tagnano

def selection_hbb_2L_b_bb(tagnano, hltkey, btagthresh):

    tagnano = presel_hbb_2L(tagnano, hltkey)
    tagnano, zsys = postsel_b_bb_bin(tagnano, btagthresh)

    leps4V = makeleps(tagnano, 2)

    lepsys = leps4V[:, 0] + leps4V[:, 1]
    tagnano = tagnano[(leps4V[:, 0].pt > 25.) & (lepsys.mass > 75.) & (lepsys.mass < 105.) & (lepsys.pt > 75.)]

    return tagnano

def selection_hbb_1L_3b(tagnano, hltkey, btagthresh):
    
    tagnano = presel_hbb_1L(tagnano, hltkey)
    tagnano, zsys = postsel_3b_bin(tagnano, btagthresh)

    leps4V = makeleps(tagnano, 1)

    mT = np.sqrt(2*leps4V[:, 0].pt*tagnano.MET.pt*(1 - np.cos(leps4V[:, 0].delta_phi(tagnano.MET))))
    delsquare = (80.377**2 - mT**2) / (2*leps4V[:, 0].pt*tagnano.MET.pt)

    etanu0 = leps4V[:, 0].eta + np.arccosh(1 + delsquare) 
    etanu1 = leps4V[:, 0].eta - np.arccosh(1 + delsquare)

    etanu = ak.where(np.isreal(etanu0), etanu0, etanu1)
    
    nu4V = ak.zip({"pt": tagnano.MET.pt, "eta": etanu, "phi": tagnano.MET.phi, "mass": ak.full_like(tagnano.MET.pt, 0.)},
                  with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    wsys = leps4V[:, 0] + nu4V
    passes_this_cut = (wsys.pt > 170.) & (wsys.delta_phi(tagnano.MET) < 2.) & (leps4V[:, 0].pt > 25.)
    tagnano = tagnano[passes_this_cut]
    wsys = wsys[passes_this_cut] 
    zsys = zsys[passes_this_cut]

    tagnano = tagnano[zsys.delta_phi(wsys) > 2.5]

    return tagnano

def selection_hbb_1L_b_bb(tagnano, hltkey, btagthresh):

    tagnano = presel_hbb_1L(tagnano, hltkey)
    tagnano, zsys = postsel_b_bb_bin(tagnano, btagthresh)

    leps4V = makeleps(tagnano, 1)

    print('pt', leps4V.pt)
    mT = np.sqrt(2*leps4V[:, 0].pt*tagnano.MET.pt*(1 - np.cos(leps4V[:, 0].delta_phi(tagnano.MET))))
    delsquare = (80.377**2 - mT**2) / (2*leps4V[:, 0].pt*tagnano.MET.pt)

    etanu0 = leps4V[:, 0].eta + np.arccosh(1 + delsquare)
    etanu1 = leps4V[:, 0].eta - np.arccosh(1 + delsquare)

    etanu = ak.where(np.isreal(etanu0), etanu0, etanu1)

    nu4V = ak.zip({"pt": tagnano.MET.pt, "eta": etanu, "phi": tagnano.MET.phi, "mass": ak.full_like(tagnano.MET.pt, 0.)},
                  with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    wsys = leps4V[:, 0] + nu4V
    passes_this_cut = (wsys.pt > 170.) & (wsys.delta_phi(tagnano.MET) < 2.) & (leps4V[:, 0].pt > 25.)
    tagnano = tagnano[passes_this_cut]
    wsys = wsys[passes_this_cut]
    zsys = zsys[passes_this_cut]

    tagnano = tagnano[zsys.delta_phi(wsys) > 2.5]

    return tagnano


def selection_hbb_0L_3b(tagnano, hltkey, btagthresh):

    tagnano = presel_hbb_0L(tagnano, hltkey)
    tagnano, zsys = postsel_3b_bin(tagnano, btagthresh)

    seljets = tagnano.Jet[(tagnano.Jet.pt >= 20) & (abs(tagnano.Jet.eta) < 2.5) & (tagnano.Jet.jetId == 6) & (tagnano.Jet.puId >= pucut)]
    seljets4V = ak.zip({"pt": seljets.pt, "eta": seljets.eta, "phi": seljets.phi, "mass": seljets.mass},
                       with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    
    pfMET = tagnano.MET
    tkMET = tagnano.TkMET
    jetsum = ak.sum(seljets4V, axis=1)
    MHT = jetsum.pt
    tagnano = tagnano[(abs(zsys.delta_phi(pfMET)) > 2.) & (pfMET.pt > 190.) & (abs(pfMET.delta_phi(tkMET)) < 0.5) & (ak.min(ak.Array([MHT, pfMET.pt]), axis=0) > 150.)]
    
    return tagnano

def selection_hbb_0L_b_bb(tagnano, hltkey, btagthresh):

    tagnano = presel_hbb_0L(tagnano, hltkey)
    tagnano, zsys = postsel_b_bb_bin(tagnano, btagthresh)

    seljets = tagnano.Jet[(tagnano.Jet.pt >= 20) & (abs(tagnano.Jet.eta) < 2.5) & (tagnano.Jet.jetId == 6) & (tagnano.Jet.puId >= pucut)]
    seljets4V = ak.zip({"pt": seljets.pt, "eta": seljets.eta, "phi": seljets.phi, "mass": seljets.mass},
                       with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    pfMET = tagnano.MET
    tkMET = tagnano.TkMET
    jetsum = ak.sum(seljets4V, axis=1)
    MHT = jetsum.pt
    tagnano = tagnano[(abs(zsys.delta_phi(pfMET)) > 2.) & (pfMET.pt > 190.) & (abs(pfMET.delta_phi(tkMET)) < 0.5) & (ak.min(ak.Array([MHT, pfMET.pt]), axis=0) > 150.)]

    return tagnano

def selection_zjhlt_b_bb_dR1p5(tagnano, hltkey, btagthresh):

    # basic selection with ZJets HLT                                                                                                                                                                              
    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]
    selbjets = tagnano.Jet[(tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4) & (tagnano.Jet.btagDeepFlavB > btagthresh) & (tagnano.Jet.puId >= 4)]
    tagnano = tagnano[ak.num(selbjets) >= 2]

    # bjet - bbjet bin selection                                                                                                                                                                                  
    jets = tagnano.friendjet_noptcut[(tagnano.friendjet_noptcut.puId >= pucut) & (abs(tagnano.friendjet_noptcut.eta) < 2.4)]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    tagnano = tagnano[(ak.num(bbjets[bbjets.pt > 30.]) >= 1) & (ak.num(bjets[bjets.pt > 30.]) >= 1)]

    # cut on the dR between the two jets                                                                                                                                                                          
    jets = tagnano.friendjet[(tagnano.Jet.puId >= pucut) & (tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4)]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = tagnano.Jet[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = tagnano.Jet[btagged & ~bbtagged]
    nonjets = tagnano.Jet[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    passes = (ak.num(bjet4V) > 0) & (ak.num(bbjet4V) > 0)
    bbtobdR = bbjet4V[passes][:, 0].delta_r(bjet4V[passes])
    selbjet = bjet4V[passes][bbtobdR == ak.min(bbtobdR, axis=1)]

    tagnano = tagnano[passes][ak.min(bbtobdR, axis=1) <= 1.5]

    return tagnano

def selection_zjhlt_b_bb_dRmZ(tagnano, hltkey, btagthresh):

    # basic selection with ZJets HLT                                                                                                                                                                              
    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]
    selbjets = tagnano.Jet[(tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4) & (tagnano.Jet.btagDeepFlavB > btagthresh) & (tagnano.Jet.puId >= 4)]
    tagnano = tagnano[ak.num(selbjets) >= 2]

    # bjet - bbjet bin selection                                                                                                                                                                                  
    jets = tagnano.Jet[(tagnano.Jet.puId >= pucut) & (abs(tagnano.Jet.eta) < 2.4)]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    tagnano = tagnano[(ak.num(bbjets[bbjets.pt > 30.]) >= 1) & (ak.num(bjets[bjets.pt > 30.]) >= 1)]

    # cut on the dR between the two jets                                                                                                                                                                          
    jets = tagnano.friendjet[(tagnano.Jet.puId >= pucut) & (tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4)]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = tagnano.Jet[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = tagnano.Jet[btagged & ~bbtagged]
    nonjets = tagnano.Jet[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    passes = (ak.num(bjet4V) > 0) & (ak.num(bbjet4V) > 0)
    bbtobdR = bbjet4V[passes][:, 0].delta_r(bjet4V[passes])
    selbjet = bjet4V[passes][bbtobdR == ak.min(bbtobdR, axis=1)]

    zmass = (bbjet4V[passes][:, 0] + selbjet[:, 0]).mass

    tagnano = tagnano[passes][(ak.min(bbtobdR, axis=1) <= 1.5) & (zmass > 70.) & (zmass < 110.)]

    return tagnano
    

def create_jets(tagnano, btagthresh, nanojets = False):

    #print('nanojets', nanojets)

    #print(tagnano.Jet.pt)

    if nanojets:
        jets = tagnano.Jet[(tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4) & (tagnano.Jet.puId >= 4)]
        bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                    (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                    (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
        bbjets = jets[bbtagged]
        btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
        bjets = jets[btagged & ~bbtagged]
        nonjets = jets[~btagged & ~bbtagged]

        bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
                        with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
        bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
                         with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    else:
        jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
        bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                    (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                    (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
        bbjets = jets[bbtagged]
        btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
        bjets = jets[btagged & ~bbtagged]
        nonjets = jets[~btagged & ~bbtagged]

        bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
                        with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
        bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
                         with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    return bbjets, bjets, nonjets, bbjet4V, bjet4V

def create_bb_bb(tagnano, btagthresh):

    bbjets, bjets, nonjets, bbjet4V, bjet4V = create_jets(tagnano, btagthresh, nanojets=True)

    bbtobbdR = bbjet4V[:, 0].delta_r(bbjet4V[:, 0:])
    bbtobbdR = ak.where(bbtobbdR > 0., bbtobbdR, 100.)
    selbbjet = bbjets[bbtobbdR == ak.min(bbtobbdR, axis=1)]

    return bbjets[:, 0], selbbjet[:, 0]

def create_b_bb(tagnano, btagthresh, nanojets=True):

    bbjets, bjets, nonjets, bbjet4V, bjet4V = create_jets(tagnano, btagthresh, nanojets=nanojets)

    bbtobdR = bbjet4V[:, 0].delta_r(bjet4V)
    selbjet = bjets[bbtobdR == ak.min(bbtobdR, axis=1)]

    return bbjets, selbjet

def create_b_b(tagnano, btagthresh, nanojets=False):

    bbjets, bjets, nonjets, bbjet4V, bjet4V = create_jets(tagnano, btagthresh, nanojets=nanojets)

    btobdR = bjets[:, 0].delta_r(bjets[:, 0:])
    btobdR =  ak.where(btobdR > 0., btobdR, 100.)
    selbjet = bjets[btobdR == ak.min(btobdR, axis=1)]
    
    return bjets, selbjet

def selection_zjets_b_bb_dR1p5_mZ(tagnano, hltkey, btagthresh):

    tagnano = presel_ABCD(tagnano, hltkey, btagthresh) 
    
    tagnano = tagnano[cut_a(tagnano, btagthresh)]
    
    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    tagnano = tagnano[cut_b(tagnano, ak.firsts(bbjets), bjets)]

    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    tagnano = tagnano[cut_mass(tagnano, ak.firsts(bbjets), bjets)]

    return tagnano

def selection_zjhlt_b_bb_dR1p5_bdt1(tagnano, hltkey, btagthresh):

    tagnano = presel_ABCD(tagnano, hltkey, btagthresh) 
    
    tagnano = tagnano[cut_a(tagnano, btagthresh)]
    
    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    tagnano = tagnano[cut_b(tagnano, ak.firsts(bbjets), bjets)]

    bbjets, selbjet = create_b_bb(tagnano, btagthresh)
    tagnano = tagnano[cut_bdt1(tagnano, bbjets)]
    
    return tagnano

def selection_zjets_b_bb_dR1p5_mZ_sigbkg(tagnano, hltkey, btagthresh):

    tagnano = selection_zjets_b_bb_dR1p5_mZ(tagnano, hltkey, btagthresh)
    
    bbjets, selbjet = create_b_bb(tagnano, btagthresh)
    
    Bs4V= ak.zip({"pt": tagnano.AllTruthBs.pt, "eta": tagnano.AllTruthBs.eta, "phi": tagnano.AllTruthBs.phi, "energy": tagnano.AllTruthBs.energy},
                           with_name="PtEtaPhiELorentzVector", behavior=vector.behavior)

    b_to_jet_matches, bs_per_jet = match_with_4V(bbjets, Bs4V, radbuffer=0.)
    bs_per_jet = ak.where(ak.num(bs_per_jet) == 0, ak.Array([[0] for i in range(len(bs_per_jet))]), bs_per_jet)

    bs_per_jet = ak.flatten(bs_per_jet, axis=None)
    print('len', len(tagnano), len(bs_per_jet))
    
    print(bs_per_jet)
    
    tagnano = tagnano[(bs_per_jet == 2) | (bs_per_jet == 3)]

    return tagnano
    
def cut_a(tagnano, btagthresh):

    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    return (ak.num(bbjets) == 1) & (ak.num(bjets) >= 1)

def cut_b(tagnano, jets_1, jets_2):

    mat_12 = ak.cartesian([jets_1, jets_2], axis=1, nested=True)
    dR_12 = mat_12['0'].delta_r(mat_12['1'])
    mindR_12 = ak.min(dR_12, axis = 2)

    #print(jets_1.type, jets_2.type)
    #print(mindR_12.type, ak.flatten(mindR_12 < 1.5, axis=None).type)
    #
    #for i in range(len(tagnano)):
    #    print(i, jets_1.pt[i], mindR_12[i])

    return ak.flatten(mindR_12 < 1.5, axis=None)

def cut_mass(tagnano, jets_1, jets_2):

    mat_12 = ak.cartesian([jets_1, jets_2], axis=1, nested=True)
    dR_12 = mat_12['0'].delta_r(mat_12['1'])
    seljet = jets_2[ak.flatten(dR_12 == ak.min(dR_12, axis = 2), axis=1)]

    mass = (jets_1 + seljet).mass
    return ak.flatten((mass > 70.) & (mass < 110.), axis=None)

def cut_bdt1(tagnano, bbjets):

    thresh = 0.6646737456321716
    
    os.system('xrdcp root://cmseos.fnal.gov//store/user/acrobert/condor/bst_renorm_nbs23_SR12x2_500_6.pkl .')
    bstfile = open('bst_renorm_nbs23_SR12x2_500_6.pkl', "rb")
    bstdict = pkl.load(bstfile)
    bst = bstdict['bst']

    df = makedf(bbjets)
    X = df.to_numpy()
    X = (X - bstdict['X_mean']) / bstdict['X_std']
    score = bst.predict(X)
    score = ak.flatten(score, axis=None)
    
    print(score, len(score), len(tagnano))
    
    return (score > thresh)


def presel_ABCD(tagnano, hltkey, btagthresh):

    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]
    selbjets = tagnano.Jet[(tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4) & (tagnano.Jet.btagDeepFlavB > btagthresh) & (tagnano.Jet.puId >= 4)]
    tagnano = tagnano[ak.num(selbjets) >= 2]

    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    tagnano = tagnano[(ak.num(bjets) >= 2) | ((ak.num(bjets) >= 1) & (ak.num(bbjets) == 1))]

    return tagnano

def presel_ABCD_dibb(tagnano, hltkey, btagthresh):

    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]
    selbjets = tagnano.Jet[(tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4) & (tagnano.Jet.btagDeepFlavB > btagthresh) & (tagnano.Jet.puId >= 4)]
    tagnano = tagnano[ak.num(selbjets) >= 2]

    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    tagnano = tagnano[(ak.num(bbjets) >= 2) | ((ak.num(bjets) >= 1) & (ak.num(bbjets) == 1))]

    return tagnano

def selection_zjhlt_2bjets(tagnano, hltkey, btagthresh):

    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]
    selbjets = tagnano.Jet[(tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4) & (tagnano.Jet.btagDeepFlavB > btagthresh) & (tagnano.Jet.puId >= 4)]
    tagnano = tagnano[ak.num(selbjets) >= 2]

    return tagnano

def selection_zjhlt_b_bb(tagnano, hltkey, btagthresh):

    tagnano = tagnano[passHLT(tagnano, anaHLT[hltkey])]
    selbjets = tagnano.Jet[(tagnano.Jet.pt >= 30) & (abs(tagnano.Jet.eta) < 2.4) & (tagnano.Jet.btagDeepFlavB > btagthresh) & (tagnano.Jet.puId >= 4)]
    tagnano = tagnano[ak.num(selbjets) >= 2]

    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    tagnano = tagnano[((ak.num(bjets) >= 1) & (ak.num(bbjets) == 1))]

    return tagnano

def selection_yAyB(tagnano, hltkey, btagthresh, blind=False):

    tagnano = presel_ABCD(tagnano, hltkey, btagthresh)

    A = cut_a(tagnano, btagthresh)
    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    jets_1 = ak.where(A, ak.firsts(bbjets), ak.firsts(bjets))
    jets_2 = ak.where(A, bjets, bjets[:, 1:])
    B = cut_b(tagnano, jets_1, jets_2)
    if blind:
        C = cut_mass(tagnano, jets_1, jets_2)
        tagnano = tagnano[A & B & ~C]
    else:
        tagnano = tagnano[A & B]
    return tagnano

def selection_yAnB(tagnano, hltkey, btagthresh):

    tagnano = presel_ABCD(tagnano, hltkey, btagthresh)

    A = cut_a(tagnano, btagthresh)
    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    jets_1 = ak.where(A, ak.firsts(bbjets), ak.firsts(bjets))
    jets_2 = ak.where(A, bjets, bjets[:, 1:])
    B = cut_b(tagnano, jets_1, jets_2)
    
    print('AnB', A.type, B.type, A, B, ak.sum(A & ~B))
    tagnano = tagnano[A & ~B]

    return tagnano

def selection_nAyB(tagnano, hltkey, btagthresh):

    tagnano = presel_ABCD(tagnano, hltkey, btagthresh)

    A = cut_a(tagnano, btagthresh)
    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    jets_1 = ak.where(A, ak.firsts(bbjets), ak.firsts(bjets))
    jets_2 = ak.where(A, bjets, bjets[:, 1:])
    B = cut_b(tagnano, jets_1, jets_2)
    
    print('nAB', A.type, B.type, A, B, ak.sum(~A & B))
    tagnano = tagnano[~A & B]

    return tagnano

def selection_nAnB(tagnano, hltkey, btagthresh):

    tagnano = presel_ABCD(tagnano, hltkey, btagthresh)

    A = cut_a(tagnano, btagthresh)
    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    jets_1 = ak.where(A, ak.firsts(bbjets), ak.firsts(bjets))
    jets_2 = ak.where(A, bjets, bjets[:, 1:])
    B = cut_b(tagnano, jets_1, jets_2)
    
    print('nAnB', A.type, B.type, A, B, ak.sum(~A & ~B))
    tagnano = tagnano[~A & ~B]

    return tagnano

def selection_dibb_yAyB(tagnano, hltkey, btagthresh, blind=False):

    tagnano = presel_ABCD_dibb(tagnano, hltkey, btagthresh)

    A = cut_a(tagnano, btagthresh)
    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    jets_1 = ak.firsts(bbjets)
    jets_2 = ak.where(A, bjets, bbjets[:, 1:])
    B = cut_b(tagnano, jets_1, jets_2)
    if blind:
        C = cut_mass(tagnano, jets_1, jets_2)
        tagnano = tagnano[A & B & ~C]
    else:
        tagnano = tagnano[A & B]
    return tagnano

def selection_dibb_yAnB(tagnano, hltkey, btagthresh):

    tagnano = presel_ABCD_dibb(tagnano, hltkey, btagthresh)

    A = cut_a(tagnano, btagthresh)
    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    jets_1 = ak.firsts(bbjets)
    jets_2 = ak.where(A, bjets, bbjets[:, 1:])
    B = cut_b(tagnano, jets_1, jets_2)
    tagnano = tagnano[A & ~B]

    return tagnano

def selection_dibb_nAyB(tagnano, hltkey, btagthresh):

    tagnano = presel_ABCD_dibb(tagnano, hltkey, btagthresh)

    A = cut_a(tagnano, btagthresh)
    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    jets_1 = ak.firsts(bbjets)
    jets_2 = ak.where(A, bjets, bbjets[:, 1:])
    B = cut_b(tagnano, jets_1, jets_2)
    tagnano = tagnano[~A & B]

    return tagnano

def selection_dibb_nAnB(tagnano, hltkey, btagthresh):

    tagnano = presel_ABCD_dibb(tagnano, hltkey, btagthresh)

    A = cut_a(tagnano, btagthresh)
    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    jets_1 = ak.firsts(bbjets)
    jets_2 = ak.where(A, bjets, bbjets[:, 1:])
    B = cut_b(tagnano, jets_1, jets_2)
    tagnano = tagnano[~A & ~B]

    return tagnano

def fillhist_dijetmass_b_bb(tagnano, hist, btagthresh, signal=False):

    bbjets, selbjet = create_b_bb(tagnano, btagthresh)
    
    dijetmass = (bbjets[:, 0] + selbjet[:, 0]).mass
    hist.fill(dijetmass)
    return len(tagnano)

#def fillhist_dijetmass_b_bb(tagnano, hist, btagthresh, signal=False):
#
#    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
#    bbtobdR = bbjets[:, 0].delta_r(bjets)
#    selbjet = bjets[bbtobdR == ak.min(bbtobdR, axis=1)]
#
#    dijetmass = (bbjets[:, 0] + selbjet[:, 0]).mass
#    hist.fill(dijetmass)
#    return len(tagnano)

def fillhist_dijetmass_bb_bb(tagnano, hist, btagthresh, signal=False):

    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    bbtobbdR = bbjets[:, 0].delta_r(bbjets[:, 0:])
    bbtobbdR = ak.where(bbtobbdR > 0., bbtobbdR, 100.)
    selbbjet = bbjets[bbtobbdR == ak.min(bbtobbdR, axis=1)]

    dijetmass = (bbjets[:, 0] + selbbjet[:, 0]).mass
    hist.fill(dijetmass)
    return len(tagnano)

def fillhist_dijetmass_b_b(tagnano, hist, btagthresh, signal=False):

    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)
    btobdR = bjets[:, 0].delta_r(bjets[:, 0:])
    btobdR =  ak.where(btobdR > 0., btobdR, 100.)
    selbjet = bjets[btobdR == ak.min(btobdR, axis=1)]
    dijetmass = (bjets[:, 0] + selbjet[:, 0]).mass

    hist.fill(dijetmass)
    return len(tagnano)

def fillhist_dijetptsum(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh)

    hist.fill(bbjets[:,0].pt + selbjet[:, 0].pt)
    return len(tagnano)

def fillhist_absbbjetcharge(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh)

    hist.fill(abs(bbjets[:,0].charge))
    return len(tagnano)

def fillhist_bbjetcharge(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh)

    hist.fill(bbjets[:,0].charge)
    return len(tagnano)

def fillhist_bbjetarea(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh, nanojets=True)

    hist.fill(bbjets[:,0].area)
    return len(tagnano)

def fillhist_bbjetnconst(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh)

    hist.fill(bbjets[:,0].nConstituents)
    return len(tagnano)

def fillhist_nbjets(tagnano, hist, btagthresh, signal=False):
    
    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)

    hist.fill(ak.num(bjets))
    return len(tagnano)

def fillhist_nbbjets(tagnano, hist, btagthresh, signal=False):
    
    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)

    hist.fill(ak.num(bbjets))
    return len(tagnano)

def fillhist_dijetcharge(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh)

    hist.fill(bbjets[:,0].charge + selbjet[:, 0].charge)
    return len(tagnano)

def fillhist_bbjettau2tau1(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh, nanojets=True)

    print(bbjets.fields)

    hist.fill(bbjets[:,0].NjettinessAK4CHS_tau2 / bbjets[:,0].NjettinessAK4CHS_tau1)
    return len(tagnano)

def fillhist_bbjettau3tau2(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh, nanojets=True)

    hist.fill(bbjets[:,0].NjettinessAK4CHS_tau3 / bbjets[:,0].NjettinessAK4CHS_tau2)
    return len(tagnano)

def fillhist_bbjettau4tau3(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh, nanojets=True)

    hist.fill(bbjets[:,0].NjettinessAK4CHS_tau4 / bbjets[:,0].NjettinessAK4CHS_tau3)
    return len(tagnano)

def fillhist_bbjettau5tau4(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh, nanojets=True)

    hist.fill(bbjets[:,0].NjettinessAK4CHS_tau5 / bbjets[:,0].NjettinessAK4CHS_tau4)
    return len(tagnano)

def fillhist_bbjettau6tau5(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh, nanojets=True)

    hist.fill(bbjets[:,0].NjettinessAK4CHS_tau6 / bbjets[:,0].NjettinessAK4CHS_tau5)
    return len(tagnano)

def fillhist_bbjettau1(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh, nanojets=True)

    hist.fill(bbjets[:,0].NjettinessAK4CHS_tau1)
    return len(tagnano)

def fillhist_bbjettau2(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh, nanojets=True)

    hist.fill(bbjets[:,0].NjettinessAK4CHS_tau2)
    return len(tagnano)

def fillhist_bbjettau3(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh, nanojets=True)

    hist.fill(bbjets[:,0].NjettinessAK4CHS_tau3)
    return len(tagnano)

def fillhist_bbjettau4(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh, nanojets=True)

    hist.fill(bbjets[:,0].NjettinessAK4CHS_tau4)
    return len(tagnano)

def fillhist_bbjettau5(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh, nanojets=True)

    hist.fill(bbjets[:,0].NjettinessAK4CHS_tau5)
    return len(tagnano)

def fillhist_bbjettau6(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet, bbjet4V, bjet4V = create_b_bb(tagnano, btagthresh, nanojets=True)

    hist.fill(bbjets[:,0].NjettinessAK4CHS_tau6)
    return len(tagnano)

def fillhist_dijetdr(tagnano, hist, btagthresh, signal=False):
    
    bbjets, selbjet = create_b_bb(tagnano, btagthresh, nanojets=True)

    hist.fill(bbjets[:,0].delta_r(selbjet[:,0]))
    return len(tagnano)


def fillhist_b_bb(tagnano, hist, btagthresh):

    # dijet mass                                                                                                                                                                                                  
    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    passes = (ak.num(bjet4V) > 0) & (ak.num(bbjet4V) > 0)
    bbtobdR = bbjet4V[passes][:, 0].delta_r(bjet4V[passes])
    selbjet = bjet4V[passes][bbtobdR == ak.min(bbtobdR, axis=1)]

    dijetmass = (bbjet4V[passes][:, 0] + selbjet[:, 0]).mass
    hist.fill(dijetmass)
    return len(tagnano)

def fillhist_3b(tagnano, hist, btagthresh):

    # trijet mass                     
    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    trijetmass = (bjet4V[:, 0] + bjet4V[:, 1] + bjet4V[:, 2]).mass
    if ak.sum(trijetmass) > 0:
        hist.fill(trijetmass)
    return len(tagnano)

def fillhist_2b_signaldelta(tagnano, hist, btagthresh, signal=False):

    print('signal', signal)

    # dijet mass                     
    jets = tagnano.friendjet_noptcut[tagnano.friendjet_noptcut.puId >= pucut]
    bbtagged = ((jets.pfDeepFlavourJetTags_probbb >= btagthresh) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_probb) &
                (jets.pfDeepFlavourJetTags_probbb > jets.pfDeepFlavourJetTags_problepb))
    bbjets = jets[bbtagged]
    btagged = (jets.pfDeepFlavourJetTags_probb + jets.pfDeepFlavourJetTags_problepb) > btagthresh
    bjets = jets[btagged & ~bbtagged]
    nonjets = jets[~btagged & ~bbtagged]

    bjet4V = ak.zip({"pt": bjets.pt, "eta": bjets.eta, "phi": bjets.phi, "mass": bjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)
    bbjet4V = ak.zip({"pt": bbjets.pt, "eta": bbjets.eta, "phi": bbjets.phi, "mass": bbjets.mass},
            with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior)

    dijetmass = (bjet4V[:, 0] + bjet4V[:, 1]).mass
    if ak.sum(dijetmass) > 0:
        if signal:
            print('signal fill', np.repeat(91.188, len(tagnano)))
            hist.fill(np.repeat(91.188, len(tagnano)))
        else:
            hist.fill(dijetmass)
    return len(tagnano)

def jet_charge(tagnano, jets, calc='ptsum', cut=None): # calc: ptsum or qsum; cut: None, dz10, dz500dxy50                                                                                                          
    tagnano = tagnano[ak.num(jets) > 0]
    jets = jets[ak.num(jets) > 0]

    pfcpt, pfcd0, pfcz0, pfcq = match_pfcs(tagnano, jets)

    if cut == 'dz10':
        pfcd0 = pfcd0[abs(pfcz0) < 0.001]
        pfcz0 = pfcz0[abs(pfcz0) < 0.001]
        pfcpt = pfcpt[abs(pfcz0) < 0.001]
        pfcq = pfcq[abs(pfcz0) < 0.001]
    elif cut == 'dz500dxy50':
        dxyzcut = (abs(pfcz0) < 0.05) & (abs(pfcd0) > 0.005)

        pfcd0 = pfcd0[dxyzcut]
        pfcz0 = pfcz0[dxyzcut]
        pfcpt = pfcpt[dxyzcut]
        pfcq = pfcq[dxyzcut]


    if calc == 'ptsum':
        return ak.sum(pfcq*pfcpt / ak.sum(pfcpt, axis=1), axis=1)
    elif calc == 'qsum':
        return ak.sum(pfcq, axis=1)
    else:
        return None

def fillhist_bbjetq_ptsipcut(tagnano, hist, btagthresh, signal=False):

    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)

    thisq = jet_charge(tagnano, bbjets, cut='dz500dxy50')
    
    hist.fill(ak.flatten(thisq, axis=None))
    return len(tagnano)

def fillhist_bbjetq_ptsnocut(tagnano, hist, btagthresh, signal=False):

    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)

    thisq = jet_charge(tagnano, bbjets)
    
    hist.fill(ak.flatten(thisq, axis=None))
    return len(tagnano)

def fillhist_bbjetq_cmssw(tagnano, hist, btagthresh, signal=False):

    bbjets, bjets, nonjets, _, _ = create_jets(tagnano, btagthresh, nanojets=True)

    hist.fill(ak.flatten(bbjets.charge, axis=None))
    return len(tagnano)

def fillhist_dijetq_ptsipcut(tagnano, hist, btagthresh, signal=False):

    bbjets, selbjet = create_b_bb(tagnano, btagthresh)
    
    bbq = jet_charge(tagnano, bbjets, cut='dz500dxy50')
    bq = jet_charge(tagnano, selbjet, cut='dz500dxy50')
    
    hist.fill(ak.flatten(bbq+bq, axis=None))
    return len(tagnano)

def fillhist_dijetq_ptsnocut(tagnano, hist, btagthresh, signal=False):

    bbjets, selbjet = create_b_bb(tagnano, btagthresh)
    
    bbq = jet_charge(tagnano, bbjets)
    bq = jet_charge(tagnano, selbjet)
    
    hist.fill(ak.flatten(bbq+bq, axis=None))
    return len(tagnano)

def fillhist_dijetq_cmssw(tagnano, hist, btagthresh, signal=False):

    bbjets, selbjet = create_b_bb(tagnano, btagthresh)
    
    hist.fill(ak.flatten(bbjets.charge+selbjet.charge, axis=None))
    return len(tagnano)

def fillhist_bbjet_pt(tagnano, hist, btagthresh, signal=False):

    bbjets, selbjet = create_b_bb(tagnano, btagthresh)
    hist.fill(ak.flatten(bbjets.pt, axis=None))
    return len(tagnano)

def fillhist_bbjet_mass(tagnano, hist, btagthresh, signal=False):

    bbjets, selbjet = create_b_bb(tagnano, btagthresh)
    hist.fill(ak.flatten(bbjets.mass, axis=None))
    return len(tagnano)

def fillhist_bbjet_energy(tagnano, hist, btagthresh, signal=False):

    bbjets, selbjet = create_b_bb(tagnano, btagthresh)
    hist.fill(ak.flatten(bbjets.energy, axis=None))
    return len(tagnano)

def fillhist_bbjet_phi(tagnano, hist, btagthresh, signal=False):

    bbjets, selbjet = create_b_bb(tagnano, btagthresh)
    hist.fill(ak.flatten(bbjets.phi, axis=None))
    return len(tagnano)

def fillhist_bbjet_eta(tagnano, hist, btagthresh, signal=False):

    bbjets, selbjet = create_b_bb(tagnano, btagthresh)
    hist.fill(ak.flatten(bbjets.eta, axis=None))
    return len(tagnano)

def makedf(jets):

    thisdf = None
    
    for feature in jets.fields:

        thisfeature = ak.flatten(jets[feature], axis=None)

        if len(thisfeature) != len(jets):
            print('skipping', feature)
            continue

        if thisdf is None:
            thisdf = pd.DataFrame({feature: thisfeature})
        else:
            thisdf.insert(0, feature, thisfeature)

    cols = ['multiplicityNeutral', 'multiplicityCharged',
            'multiplicityNeutralHad', 'multiplicityChargedHad',
            'multiplicityPhotons', 'multiplicityElectrons', 'multiplicityMuons',
            'charge', 'gql', 'energy', 'pfDeepFlavourJetTags_problepb',
            'pfDeepFlavourJetTags_probbb', 'pfDeepFlavourJetTags_probb',
            'softdropMass', 'prunedMass', 'pfDeepCSVJetTags_probudsg',
            'pfDeepCSVJetTags_probc', 'pfDeepCSVJetTags_probbb',
            'pfDeepCSVJetTags_probb', 'NjettinessAK4CHS_tau6',
            'NjettinessAK4CHS_tau5', 'NjettinessAK4CHS_tau4',
            'NjettinessAK4CHS_tau3', 'NjettinessAK4CHS_tau2',
            'NjettinessAK4CHS_tau1', 'cleanmask', 'nConstituents', 'puId', 'nMuons',
            'nElectrons', 'cRegRes', 'cRegCorr', 'bRegRes', 'bRegCorr', 'rawFactor',
            'qgl', 'puIdDisc', 'pt', 'phi', 'neHEF', 'neEmEF', 'muonSubtrFactor',
            'muEF', 'mass', 'eta', 'chHEF', 'chFPV3EF', 'chFPV2EF', 'chFPV1EF',
            'chFPV0EF', 'chEmEF', 'btagDeepFlavQG', 'btagDeepFlavCvL',
            'btagDeepFlavCvB', 'btagDeepFlavC', 'btagDeepFlavB', 'btagDeepCvL',
            'btagDeepCvB', 'btagDeepC', 'btagDeepB', 'btagCSVV2', 'btagCMVA',
            'area']

    thisdf = thisdf[cols]

    return thisdf
                

def fillhist_bbjet_bdtscore(tagnano, hist, btagthresh, signal=False):

    os.system('xrdcp root://cmseos.fnal.gov//store/user/acrobert/condor/bst_renorm_nbs23_SR12x2_500_6.pkl .')
    bstfile = open('bst_renorm_nbs23_SR12x2_500_6.pkl', "rb")
    bstdict = pkl.load(bstfile)
    bst = bstdict['bst']
    
    bbjets, selbjet = create_b_bb(tagnano, btagthresh)

    df = makedf(bbjets)
    X = df.to_numpy()
    X = (X - bstdict['X_mean']) / bstdict['X_std']
    score = bst.predict(X)

    if len(score) > 0:
        hist.fill(score)
        
    return len(tagnano)

    
def loadfile(loc, path='', metadata={}):

    if path=='':
        try:
            nano = NanoEventsFactory.from_root(
                loc,
                schemaclass=NanoAODSchema.v6,
                metadata=metadata,
            ).events()
        except uproot.exceptions.KeyInFileError:
            return None
        except OSError:
            return None
    else:
        try:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                nano = NanoEventsFactory.from_root(
                    loc,
                    treepath=path,
                    schemaclass=NanoAODSchema.v6,
            metadata=metadata,
                ).events()
        except uproot.exceptions.KeyInFileError:
            return None
        except OSError:
            return None

    return nano

def loadpair(nanofile, friendfile, mode='1Fr'):

    tagnano = loadfile(nanofile, metadata={"dataset file": nanofile})
    friends = [loadfile(file, path='fevt/EvtTree', metadata={"dataset file": file}) for file in friendfile]

    #print(nanofile, friendfile)                                                                                                                                                                               

    if tagnano is None:
        print(f'Skipping null file: {nanofile.split("/")[-1]}')
        return None
    if friends[0] is None:
        locpath = friendfile[0].replace('root://cmsdata.phys.cmu.edu/', '')
        os.system(f'xrdfs root://cmsdata.phys.cmu.edu/ rm {locpath}')
        print(f'Skipping null file: {friendfile[0].split("/")[-1]}')
        return None

    if mode != '1Fr':
        if friends[1] is None:
            print(f'Skipping null file: {friendfile[1].split("/")[-1]}')
            return None

    if mode != '1Fr':
        if len(tagnano) != len(friends[0]) or len(tagnano) != len(friends[1]):
            print(f'Skipping unmatched files: {len(friends[0])} {len(friends[1])} {len(tagnano)}')
            return None
    else:
        if len(tagnano) != len(friends[0]):
            print(f'Skipping unmatched files: {len(friends[0])} {len(tagnano)}')
            return None

    assert len(tagnano) == len(friends[0])

    if mode != '1Fr':
        keys = ['Zs', 'map']
        for i in range(len(friends)):
            if keys[i] not in friends[i].fields:
                print(f'Skipping missing field: {keys[i]}')
                return None
            tagnano[keys[i]] = friends[i][keys[i]]
    else:
        keys = ['Zs', 'map', 'AllTruthBs','NonZTruthBs']
        for i in range(len(keys)):
            if keys[i] not in friends[0].fields:
                print(f'Skipping missing field: {keys[i]}')
                return None
            try:
                tagnano[keys[i]] = friends[0][keys[i]]
            except:
                print('Skipping missing field:', keys[i])

                if 'selectedPatJetsAK4PFCHS' not in tagnano.fields:
                    print(f'Skipping missing field: selectedPatJetsAK4PFCHS')
                    return None

    if 'selectedPatJetsAK4PFCHS' not in tagnano.fields:
        print(f'Skipping missing field: selectedPatJetsAK4PFCHS')
        return None

    tagnano['friendjet_noptcut'] = friends[0]['friendjet']


    diffs1 = ak.num(tagnano.Jet) != ak.num(tagnano['selectedPatJetsAK4PFCHS'])

    jetnewjet = ak.cartesian([tagnano.Jet, tagnano.selectedPatJetsAK4PFCHS], axis=1, nested=True)
    jnjdr = jetnewjet['0'].delta_r(jetnewjet['1'])
    mindr = ak.min(jnjdr, axis = 2)
    newjetpt = tagnano.selectedPatJetsAK4PFCHS.pt[ak.argmin(jnjdr, axis = 2)]
    matched = (mindr < 0.01) & (abs( (tagnano.Jet.pt - newjetpt) / tagnano.Jet.pt ) < 0.2)

    for f in tagnano.selectedPatJetsAK4PFCHS.fields:
        if f not in tagnano.Jet.fields:
            newrec = ak.where(matched, tagnano.selectedPatJetsAK4PFCHS[f][ak.argmin(jnjdr, axis = 2)], ak.full_like(tagnano.Jet.pt, None))
            tagnano['Jet'] = ak.with_field(tagnano['Jet'], newrec, f)

    jetnewjet = ak.cartesian([tagnano.Jet, friends[0].friendjet], axis=1, nested=True)
    jnjdr = jetnewjet['0'].delta_r(jetnewjet['1'])
    mindr = ak.min(jnjdr, axis = 2)
    newjetpt = friends[0].friendjet.pt[ak.argmin(jnjdr, axis = 2)]
    matched = (mindr < 0.01) & (abs( (tagnano.Jet.pt - newjetpt) / tagnano.Jet.pt ) < 0.2)

    for f in friends[0].friendjet.fields:
        if f not in tagnano.Jet.fields:
            newrec = ak.where(matched, friends[0].friendjet[f][ak.argmin(jnjdr, axis = 2)], ak.full_like(tagnano.Jet.pt, None))
            tagnano['Jet'] = ak.with_field(tagnano['Jet'], newrec, f)

    ptthresh = friends[0].friendjet.pt > 15.00001
    friends[0]['friendjet'] = friends[0]['friendjet'][ptthresh]
    tagnano['friendjet'] = friends[0]['friendjet']

    diffs = ak.num(tagnano.Jet) != ak.num(friends[0]['friendjet'])
    diffs = ak.num(tagnano.Jet) != ak.num(tagnano.friendjet.pfDeepFlavourJetTags_probbb)
    if ak.sum(diffs) != 0:
        print(ak.num(tagnano.Jet)[diffs], ak.num(tagnano.friendjet.pfDeepFlavourJetTags_probbb[diffs]))

        ptthresh = friends[0].friendjet.pt > 15.001
        friends[0]['friendjet'] = friends[0]['friendjet'][ptthresh]
        tagnano['friendjet'] = friends[0]['friendjet']


    return tagnano


#def loadpair(nanofile, friendfile, mode='1Fr'):
#
#    tagnano = loadfile(nanofile, metadata={"dataset file": nanofile})
#    friends = [loadfile(file, path='fevt/EvtTree', metadata={"dataset file": file}) for file in friendfile]
#
#    if tagnano is None:
#        print(f'Skipping null file: {nanofile.split("/")[-1]}')
#        return None
#    if friends[0] is None:
#        locpath = friendfile[0].replace('root://cmsdata.phys.cmu.edu/', '')
#        os.system(f'xrdfs root://cmsdata.phys.cmu.edu/ rm {locpath}')
#        print(f'Skipping null file: {friendfile[0].split("/")[-1]}')
#        return None
#        #return tagnano                                                                                                                                                                                        
#
#    if mode != '1Fr':
#        if friends[1] is None:
#            print(f'Skipping null file: {friendfile[1].split("/")[-1]}')
#            return None
#
#    if mode != '1Fr':
#        if len(tagnano) != len(friends[0]) or len(tagnano) != len(friends[1]):
#            print(f'Skipping unmatched files: {len(friends[0])} {len(friends[1])} {len(tagnano)}')
#            return None
#    else:
#        if len(tagnano) != len(friends[0]):
#            print(f'Skipping unmatched files: {len(friends[0])} {len(tagnano)}')
#            return None
#
#    assert len(tagnano) == len(friends[0])
#    #assert len(tagnano) == len(friends[1])                                                                                                                                                                    
#
#    if mode != '1Fr':
#        keys = ['Zs', 'map']
#        for i in range(len(friends)):
#            #print(i, friends[i].fields)                                                                                                                                                                       
#            if keys[i] not in friends[i].fields:
#                print(f'Skipping missing field: {keys[i]}')
#                return None
#            tagnano[keys[i]] = friends[i][keys[i]]
#        #print(f'Running file pair {nanofile}; elapsed time {round(tmr.time()/60., 3)}')                                                                                                                       
#    else:
#        keys = ['Zs', 'map', 'AllTruthBs','NonZTruthBs']
#        for i in range(len(keys)):
#            #print(friends[0].fields)                                                                                                                                                                          
#            if keys[i] not in friends[0].fields:
#                print(f'Skipping missing field: {keys[i]}')
#                return None
#            try:
#                tagnano[keys[i]] = friends[0][keys[i]]
#            except:
#                print('Skipping missing field:', keys[i])
#
#    tagnano['friendjet_noptcut'] = friends[0]['friendjet']
#    ptthresh = friends[0].friendjet.pt > 15.00001
#    friends[0]['friendjet'] = friends[0]['friendjet'][ptthresh]
#    tagnano['friendjet'] = friends[0]['friendjet']
#
#    diffs = ak.num(tagnano.Jet) != ak.num(friends[0]['friendjet'])
#    diffs = ak.num(tagnano.Jet) != ak.num(tagnano.friendjet.pfDeepFlavourJetTags_probbb)
#    if ak.sum(diffs) != 0:
#        print(ak.num(tagnano.Jet)[diffs], ak.num(tagnano.friendjet.pfDeepFlavourJetTags_probbb[diffs]))
#
#        ptthresh = friends[0].friendjet.pt > 15.001
#        friends[0]['friendjet'] = friends[0]['friendjet'][ptthresh]
#        tagnano['friendjet'] = friends[0]['friendjet']
#
#    return tagnano

def checkcq(tag):
    statuses = {}
    os.system('condor_q > cqtmp.txt 2>&1')
    try:
        with open('cqtmp.txt','r') as cqout:
            cqlines = cqout.readlines()
            for line in cqlines:
                if ('makeHist.sh' in line or 'makeHist_multi.sh' in line) and tag in line:
                    linelist = line.split(' ')
                    lineinfo = list(filter(lambda a: a != '', linelist[:-1]))

                    key = lineinfo[9].split('_')[0]
                    status = [0, 0]
                    if key not in statuses.keys():
                        statuses[key] = [0, 0]

                    if lineinfo[5] == 'I':
                        statuses[key][0] += 1
                    elif lineinfo[5] == 'R' or lineinfo[5] == '<q':
                        statuses[key][1] += 1
        os.system('rm cqtmp.txt')
    except IOError:
        pass

    rmkeys = []
    for key in statuses.keys():
        if statuses[key] == [0, 0]:
            rmkeys.append(key)
            
    for key in rmkeys:
        del statuses[key]

    return statuses

selections = {'zjhlt_b_bb_dR1p5': selection_zjhlt_b_bb_dR1p5, 'hbb_0L_3b': selection_hbb_0L_3b, 'hbb_1L_3b': selection_hbb_1L_3b, 'hbb_2L_3b': selection_hbb_2L_3b,
              'hbb_0L_b_bb': selection_hbb_0L_b_bb, 'hbb_1L_b_bb': selection_hbb_1L_b_bb, 'hbb_2L_b_bb': selection_hbb_2L_b_bb, 'hbb_2L_2b': selection_hbb_2L_2b,
              'zjhlt_b_bb_dRmZ': selection_zjets_b_bb_dR1p5_mZ, 'yAyB': selection_yAyB, 'nAyB': selection_nAyB, 'yAnB': selection_yAnB, 'nAnB': selection_nAnB, 
              'zjhlt_2bjets': selection_zjhlt_2bjets, 'zjhlt_b_bb': selection_zjhlt_b_bb, 'dibb_yAyB': selection_dibb_yAyB, 'dibb_nAyB': selection_dibb_nAyB, 
              'dibb_yAnB': selection_dibb_yAnB, 'dibb_nAnB': selection_dibb_nAnB, 'zjets_b_bb_dRmZ_sigbkg': selection_zjets_b_bb_dR1p5_mZ_sigbkg, 'zjhlt_b_bb_dRbdt': selection_zjhlt_b_bb_dR1p5_bdt1}
fillhists = {'b_bb': fillhist_b_bb, '3b': fillhist_3b, '2b_signaldelta': fillhist_2b_signaldelta, 'dijetptsum': fillhist_dijetptsum, 'dijetcharge': fillhist_dijetcharge,
             'absbbjetcharge': fillhist_absbbjetcharge, 'bbjetcharge': fillhist_bbjetcharge, 'bbjetarea': fillhist_bbjetarea,'bbjetnconst': fillhist_bbjetnconst,
             'bbjettau2tau1': fillhist_bbjettau2tau1, 'bbjettau3tau2': fillhist_bbjettau3tau2, 'bbjettau4tau3': fillhist_bbjettau4tau3, 'bbjettau5tau4': fillhist_bbjettau5tau4,
             'bbjettau6tau5': fillhist_bbjettau6tau5, 'bbjettau1': fillhist_bbjettau1, 'bbjettau2': fillhist_bbjettau2, 'bbjettau3': fillhist_bbjettau3, 'bbjettau4': fillhist_bbjettau4, 
             'bbjettau5': fillhist_bbjettau5, 'bbjettau6': fillhist_bbjettau6, 'dijetmass_b_bb': fillhist_dijetmass_b_bb, 'dijetmass_b_b': fillhist_dijetmass_b_b, 
             'nbbjets': fillhist_nbbjets, 'nbjets': fillhist_nbjets, 'dijetdr': fillhist_dijetdr, 'dijetmass_bb_bb': fillhist_dijetmass_bb_bb, 'dijetq_cmssw': fillhist_dijetq_cmssw,
             'bbjetq_cmssw': fillhist_bbjetq_cmssw, 'dijetq_ptsnocut': fillhist_dijetq_ptsnocut, 'dijetq_ptsipcut': fillhist_dijetq_ptsipcut, 'bbjet_bdtscore': fillhist_bbjet_bdtscore,
             'bbjet_pt': fillhist_bbjet_pt, 'bbjet_mass': fillhist_bbjet_mass,'bbjet_energy': fillhist_bbjet_energy, 'bbjet_eta': fillhist_bbjet_eta, 'bbjet_phi': fillhist_bbjet_phi}

