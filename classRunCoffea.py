import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, TreeMakerSchema, BaseSchema
from coffea.nanoevents.methods import vector
from coffea.analysis_tools import PackedSelection
from coffea import processor
from coffea.processor import IterativeExecutor
import numpy as np
import glob
from utils import *
import uproot
import warnings
import fnmatch
#import ROOT as r
from hist import Hist
import hist
import matplotlib.pyplot as plt
import mplhep

#plt.rcParams['text.usetex'] = True

tmr = timer()

class RunCoffea:

    def initplotters(key, plots, bkg = False):

        if plots == 'all':
            if not bkg:
                from class4VPlots import Plots4V
                from classDRPlots import PlotsDR
                from classMatchPlots import PlotsMatch
                from classMapPlots import PlotsMap
                from classWindowPlots import PlotsWindow
                plotter4V = Plots4V(key=key)
                plotterDR = PlotsDR(key=key)
                plotterMatch = PlotsMatch(key=key)
                plotterMap = PlotsMap(key=key)
                plotterWindow = PlotsWindow(key=key)

            from classJetPlots import PlotsJet
            from classTrigPlots import PlotsTrig
            plotterJet = PlotsJet(key=key, bkg=bkg)
            plotterTrig = PlotsTrig(key=key, bkg=bkg)

            if not bkg:
                plotters = [plotter4V, plotterDR, plotterMatch, plotterJet, plotterTrig, plotterMap, plotterWindow]
            else:
                plotters = [plotterJet, plotterTrig]

        if plots == '4V':
            from class4VPlots import Plots4V
            plotter4V = Plots4V(key=key)
            plotters = [plotter4V]

        if plots == 'DR':
            from classDRPlots import PlotsDR
            plotterDR = PlotsDR(key=key)
            plotters = [plotterDR]

        if plots == 'Jet':
            from classJetPlots import PlotsJet
            plotterJet = PlotsJet(key=key, bkg=bkg)
            plotters = [plotterJet]

        if plots == 'Match':
            from classMatchPlots import PlotsMatch
            plotterMatch = PlotsMatch(key=key)
            plotters = [plotterMatch]

        if plots == 'Trig':
            from classTrigPlots import PlotsTrig
            plotterTrig = PlotsTrig(key=key, bkg=bkg)
            plotters = [plotterTrig]

        if plots == 'Sel':
            from classSelPlots import PlotsSel
            plotterSel = PlotsSel(key=key, bkg=bkg)
            plotters = [plotterSel]

        if plots == 'Map':
            from classMapPlots import PlotsMap
            plotterMap = PlotsMap(key=key, bkg=bkg)
            plotters = [plotterMap]

        if plots == 'Window':
            from classWindowPlots import PlotsWindow
            plotterWindow = PlotsWindow(key=key, bkg=bkg)
            plotters = [plotterWindow]

        if plots == 'Sig':
            from classSigPlots import PlotsSig
            plotterSig = PlotsSig(key=key, bkg=bkg)
            plotters = [plotterSig]

        if plots == 'Ana':
            from classAnaPlots import PlotsAna
            plotterAna = PlotsAna(key=key, bkg=bkg)
            plotters = [plotterAna]

        if plots == 'Gen':
            from classGenPlots import PlotsGen
            plotterGen = PlotsGen(key=key, bkg=bkg)
            plotters = [plotterGen]

        if plots == 'GenMatch':
            from classGenMatchPlots import PlotsGenMatch
            plotterGenMatch = PlotsGenMatch(key=key, bkg=bkg)
            plotters = [plotterGenMatch]

        if plots == 'Lepton':
            from classLeptonPlots import PlotsLepton
            plotterLepton = PlotsLepton(key=key, bkg=bkg)
            plotters = [plotterLepton]

        if plots == 'Cutflow':
            from classCutflowPlots import PlotsCutflow
            plotterCutflow = PlotsCutflow(key=key, bkg=bkg)
            plotters = [plotterCutflow]

        return plotters

    def initnanos(key, mode='1Fr', local=False, date='Jan2023'):

        if mode == '2Fr':
            friendtag = ['ZBQ-EPid', 'MapEcalPFCs']
        elif mode == '1Fr':
            friendtag = ['EPid-Map']
        localfiles = glob.glob('nanos/*'+key+'*')
        if not local:
            try:
                thesefiles = xglob_onedir(f'/store/user/acrobert/Znano/{date}/{t3dirs[key]}/')
            except KeyError:
                thesefiles = xglob_onedir(f'/store/user/acrobert/Znano/{date}/{key}/')
        else:
            thesefiles = glob.glob('taggednano/*'+key+'*')

        nanostr = 'nano_ParT' if date=='Jan2023' else 'nano_PartJTB'
        addtag = '_skimZJPS1' if 'j-g' not in key else ''
        
        taggednano = [f for f in thesefiles if key in f and nanostr in f]
        friendtrees = [[f for f in thesefiles if key in f and tag in f] for tag in friendtag]

        matchedpairs = []

        missingnano = []
        missingfriend = []

        #for i in range(len(friendtrees[0])):
        lim = 10000 if key == 'TTJets_up20UL18' else 10000
        for i in range(lim):

            #print(i, taggednano[i], friendtrees[0][i])
            friendTname = [f'friend_{key}{addtag}_{tag}_file{i}.root' for tag in friendtag]
            tagnanoname = f'{nanostr}{addtag}_{key}_file{i}.root'
            if not local:
                try:
                    friendTfile = [f'/store/user/acrobert/Znano/{date}/{t3dirs[key]}/{name}' for name in friendTname]
                    tagnanofile = f'/store/user/acrobert/Znano/{date}/{t3dirs[key]}/{tagnanoname}'
                except KeyError:
                    friendTfile = [f'/store/user/acrobert/Znano/{date}/{key}/{name}' for name in friendTname]
                    tagnanofile = f'/store/user/acrobert/Znano/{date}/{key}/{tagnanoname}'
                friendTpath = [f'root://cmsdata.phys.cmu.edu/{file}' for file in friendTfile]
                tagnanopath = f'root://cmsdata.phys.cmu.edu/{tagnanofile}'
            else:
                friendTfile = [f'taggednano/{name}' for name in friendTname]
                tagnanofile = f'taggednano/{tagnanoname}'
            
            allfriends = all([file in thesefiles for file in friendTfile])
            if tagnanofile in thesefiles and allfriends:

                if not local:
                    friendfile = [f'root://cmsdata.phys.cmu.edu/{file}' for file in friendTfile]
                    nanofile = f'root://cmsdata.phys.cmu.edu/{tagnanofile}'
                else:
                    friendfile = [f'{name}' for name in friendTfile]
                    nanofile = f'{tagnanofile}'
                matchedpairs.append((nanofile, friendfile))
            #elif tagnanofile in thesefiles or allfriends:
            #    print(i, tagnanofile in thesefiles, allfriends, [file in thesefiles for file in friendTfile])
            elif tagnanofile in thesefiles:
                missingfriend.append(i)
            elif allfriends:
                missingnano.append(i)

        
        print(f'Key: {key} found {len(matchedpairs)} pairs; missing {(len(friendtrees[0]) - len(matchedpairs))} nanos and {(len(taggednano) - len(matchedpairs))} friends')
        print(f'Missing nano: {" ".join(str(x) for x in missingnano)}')
        print(f'Missing friend: {" ".join(str(x) for x in missingfriend)}')
        return matchedpairs

    def loadfile(loc, path='', metadata={}):
        
        if path=='':
            try:
                nano = NanoEventsFactory.from_root(
                    loc,
                    schemaclass=NanoAODSchema.v6,
                    metadata=metadata,
                ).events()
            except uproot.exceptions.KeyInFileError:
                return None
            except OSError:
                return None
        else:
            try:
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    nano = NanoEventsFactory.from_root(
                        loc,
                        treepath=path,
                        schemaclass=NanoAODSchema.v6,
                        metadata=metadata,
                    ).events()
            except uproot.exceptions.KeyInFileError:
                return None
            except OSError:
                return None

        return nano

    def loadpair(nanofile, friendfile, mode='1Fr'):

        tagnano = RunCoffea.loadfile(nanofile, metadata={"dataset file": nanofile})
        friends = [RunCoffea.loadfile(file, path='fevt/EvtTree', metadata={"dataset file": file}) for file in friendfile]

        #print(nanofile, friendfile)

        if tagnano is None:
            print(f'Skipping null file: {nanofile.split("/")[-1]}')
            return None
        if friends[0] is None:
            locpath = friendfile[0].replace('root://cmsdata.phys.cmu.edu/', '')
            os.system(f'xrdfs root://cmsdata.phys.cmu.edu/ rm {locpath}')
            print(f'Skipping null file: {friendfile[0].split("/")[-1]}')
            return None

        if mode != '1Fr':
            if friends[1] is None:
                print(f'Skipping null file: {friendfile[1].split("/")[-1]}')
                return None
        
        if mode != '1Fr':
            if len(tagnano) != len(friends[0]) or len(tagnano) != len(friends[1]):
                print(f'Skipping unmatched files: {len(friends[0])} {len(friends[1])} {len(tagnano)}')
                return None
        else:
            if len(tagnano) != len(friends[0]):
                print(f'Skipping unmatched files: {len(friends[0])} {len(tagnano)}')
                return None

        assert len(tagnano) == len(friends[0])

        if mode != '1Fr':
            keys = ['Zs', 'map']
            for i in range(len(friends)):
                if keys[i] not in friends[i].fields:
                    print(f'Skipping missing field: {keys[i]}')
                    return None
                tagnano[keys[i]] = friends[i][keys[i]]
        else:
            keys = ['Zs', 'map', 'AllTruthBs','NonZTruthBs']
            for i in range(len(keys)):
                if keys[i] not in friends[0].fields:
                    print(f'Skipping missing field: {keys[i]}')
                    return None
                try:
                    tagnano[keys[i]] = friends[0][keys[i]]
                except:
                    print('Skipping missing field:', keys[i])


        if 'selectedPatJetsAK4PFCHS' not in tagnano.fields:
            print(f'Skipping missing field: selectedPatJetsAK4PFCHS')
            return None
            
        tagnano['friendjet_noptcut'] = friends[0]['friendjet']


        diffs1 = ak.num(tagnano.Jet) != ak.num(tagnano['selectedPatJetsAK4PFCHS'])
    
        jetnewjet = ak.cartesian([tagnano.Jet, tagnano.selectedPatJetsAK4PFCHS], axis=1, nested=True)
        jnjdr = jetnewjet['0'].delta_r(jetnewjet['1'])
        mindr = ak.min(jnjdr, axis = 2)
        newjetpt = tagnano.selectedPatJetsAK4PFCHS.pt[ak.argmin(jnjdr, axis = 2)]
        matched = (mindr < 0.01) & (abs( (tagnano.Jet.pt - newjetpt) / tagnano.Jet.pt ) < 0.2)

        for f in tagnano.selectedPatJetsAK4PFCHS.fields:
            if f not in tagnano.Jet.fields:
                newrec = ak.where(matched, tagnano.selectedPatJetsAK4PFCHS[f][ak.argmin(jnjdr, axis = 2)], ak.full_like(tagnano.Jet.pt, None))
                tagnano['Jet'] = ak.with_field(tagnano['Jet'], newrec, f)

        jetnewjet = ak.cartesian([tagnano.Jet, friends[0].friendjet], axis=1, nested=True)
        jnjdr = jetnewjet['0'].delta_r(jetnewjet['1'])
        mindr = ak.min(jnjdr, axis = 2)
        newjetpt = friends[0].friendjet.pt[ak.argmin(jnjdr, axis = 2)]
        matched = (mindr < 0.01) & (abs( (tagnano.Jet.pt - newjetpt) / tagnano.Jet.pt ) < 0.2)

        for f in friends[0].friendjet.fields:
            if f not in tagnano.Jet.fields:
                newrec = ak.where(matched, friends[0].friendjet[f][ak.argmin(jnjdr, axis = 2)], ak.full_like(tagnano.Jet.pt, None))
                tagnano['Jet'] = ak.with_field(tagnano['Jet'], newrec, f)
            
        ptthresh = friends[0].friendjet.pt > 15.00001
        friends[0]['friendjet'] = friends[0]['friendjet'][ptthresh]
        tagnano['friendjet'] = friends[0]['friendjet']

        diffs = ak.num(tagnano.Jet) != ak.num(friends[0]['friendjet'])
        diffs = ak.num(tagnano.Jet) != ak.num(tagnano.friendjet.pfDeepFlavourJetTags_probbb)
        if ak.sum(diffs) != 0:
            print(ak.num(tagnano.Jet)[diffs], ak.num(tagnano.friendjet.pfDeepFlavourJetTags_probbb[diffs]))

            ptthresh = friends[0].friendjet.pt > 15.001
            friends[0]['friendjet'] = friends[0]['friendjet'][ptthresh]
            tagnano['friendjet'] = friends[0]['friendjet']


        return tagnano

    def runZs(tagnano, plotters, key, totwgt={'all':0., 'Z4B':0., 'Z2Q2B':0., 'Z4Q':0., 'Z2B':0., 'Z2Q':0., }, totevt={'all':0, 'Z4B':0, 'Z2Q2B':0, 'Z4Q':0, 'Z2B':0, 'Z2Q':0}):

        if key == 'ZZTo2Q2L_mc2017UL':
            tagnano = tagnano[ak.num(tagnano.Zs) == 2]
                
        tagnano['Zs'] = tagnano.Zs[ak.argsort(ak.num(tagnano.Zs.final_Qs_pdgId, axis=-1), ascending=False)]
            
        # temp fix! XYZ
        if ak.sum(ak.num(tagnano.Zs) == 0) > 0:
            print('Skipping {} events with zero Zs'.format(ak.sum(ak.num(tagnano.Zs) == 0)))
        tagnano = tagnano[ak.num(tagnano.Zs) > 0]
        hasZ4B = tagnano.Zs.nB[:,0] == 4 #ak.any(tagnano.Zs.nB == 4, axis = -1)
        nZ4B = ak.sum(hasZ4B)

        abspdgs = abs(tagnano[hasZ4B].Zs[:,0].final_Qs_pdgId)
        #print(tagnano[hasZ4B].Zs[:,0].final_Qs_pdgId[abspdgs == 5][0:5])
        bspdg = tagnano[hasZ4B].Zs[:,0].final_Qs_pdgId[abspdgs == 5]
        #print(ak.num(bspdg, axis=-1)[0:5])
        #print(bspdg[ak.num(bspdg, axis=-1) != 4])

        hasZ2Q2B = (tagnano.Zs.nB[:,0] == 2) & (ak.num(tagnano.Zs.final_Qs_pdgId, axis=-1)[:,0] == 4)
        nZ2Q2B = ak.sum(hasZ2Q2B)
         
        hasZ4Q = (tagnano.Zs.nB[:,0] == 0) & (ak.num(tagnano.Zs.final_Qs_pdgId, axis=-1)[:,0] == 4)
        nZ4Q = ak.sum(hasZ4Q)
            
        hasZ2B = (tagnano.Zs.nB[:,0] == 2) & (ak.num(tagnano.Zs.final_Qs_pdgId, axis=-1)[:,0] == 2)
        nZ2B = ak.sum(hasZ2B)
        
        hasZ2Q = (tagnano.Zs.nB[:,0] == 0) & (ak.num(tagnano.Zs.final_Qs_pdgId, axis=-1)[:,0] == 2)
        nZ2Q = ak.sum(hasZ2Q)
            
        totwgt['all'] += ak.sum(tagnano.genWeight)
        totwgt['Z4B'] += ak.sum(tagnano[hasZ4B].genWeight)
        totwgt['Z2Q2B'] += ak.sum(tagnano[hasZ2Q2B].genWeight)
        totwgt['Z4Q'] += ak.sum(tagnano[hasZ4Q].genWeight)
        totwgt['Z2B'] += ak.sum(tagnano[hasZ2B].genWeight)
        totwgt['Z2Q'] += ak.sum(tagnano[hasZ2Q].genWeight)
        totevt['all'] += len(tagnano.genWeight)
        totevt['Z4B'] += len(tagnano[hasZ4B].genWeight)
        totevt['Z2Q2B'] += len(tagnano[hasZ2Q2B].genWeight)
        totevt['Z4Q'] += len(tagnano[hasZ4Q].genWeight)
        totevt['Z2B'] += len(tagnano[hasZ2B].genWeight)
        totevt['Z2Q'] += len(tagnano[hasZ2Q].genWeight)

        for plotter in plotters:
            if nZ4B > 0:
                plotter.process4B(tagnano[hasZ4B])
            if nZ2B > 0:
                plotter.process2B(tagnano[hasZ2B])
            try:
                if nZ2Q2B > 0:
                    plotter.process2Q2B(tagnano[hasZ2Q2B])
                if nZ4Q > 0:
                    plotter.process4Q(tagnano[hasZ4Q])
            except:
                pass

    def runAll(tagnano, plotters, key, totwgt={'all':0.}, totevt={'all':0}):

        totwgt['all'] += ak.sum(tagnano.genWeight)
        totevt['all'] += len(tagnano)

        for plotter in plotters:
            plotter.processAll(tagnano)

    def plot(plotters):
        
        for plotter in plotters:
            plotter.plot()
            
