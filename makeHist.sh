#!/bin/sh                                                                                                                                                                                                 

ls
        
echo "Starting job on " `date` #Date/time of start of job                                                                                                                                                 
echo "Running on: `uname -a`" #Condor job is running on this node                                                                                                                                         
echo "System software: `cat /etc/redhat-release`" #Operating System on that node                                                                                                                                 
outname=$1
hltkey=$2
infiles=$3
fillhist=$4
btagthresh=$5
files=$6
xmin=$7
xmax=$8
nbins=$9

#site=cmsdata.phys.cmu.edu
site=cmseos.fnal.gov

export SCRAM_ARCH=el9_amd64_gcc13
source /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc13-opt/setup.sh

echo "environment created"

xrdcp -f root://${site}//store/user/acrobert/condor/datadicts.py .
xrdcp -f root://${site}//store/user/acrobert/condor/utils.py .
xrdcp -f root://${site}//store/user/acrobert/condor/htcutils.py .
xrdcp -f root://${site}//store/user/acrobert/condor/exechtc.py .

echo "files copied"

python3 exechtc.py $outname $hltkey $infiles $fillhist $btagthresh $files $xmin $xmax $nbins

ls

xrdcp -f ${outname}.pkl root://${site}//store/user/acrobert/condor/pkl/.
